package com.example.demo.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Beosztas.class)
public abstract class Beosztas_ {

	public static volatile SingularAttribute<Beosztas, Long> id;
	public static volatile SingularAttribute<Beosztas, String> beosztasNev;

	public static final String ID = "id";
	public static final String BEOSZTAS_NEV = "beosztasNev";

}

