package com.example.demo.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Telephely.class)
public abstract class Telephely_ extends com.example.demo.domain.AbstractDomain_ {

	public static volatile SingularAttribute<Telephely, Megye> megye;
	public static volatile SingularAttribute<Telephely, String> telefonSzam;
	public static volatile SingularAttribute<Telephely, Integer> iranyitoSzam;
	public static volatile SingularAttribute<Telephely, String> cim;
	public static volatile SingularAttribute<Telephely, LocalDate> mukodesKezdete;
	public static volatile SingularAttribute<Telephely, String> fax;
	public static volatile SingularAttribute<Telephely, String> nev;
	public static volatile SingularAttribute<Telephely, String> email;
	public static volatile SingularAttribute<Telephely, String> telepules;

	public static final String MEGYE = "megye";
	public static final String TELEFON_SZAM = "telefonSzam";
	public static final String IRANYITO_SZAM = "iranyitoSzam";
	public static final String CIM = "cim";
	public static final String MUKODES_KEZDETE = "mukodesKezdete";
	public static final String FAX = "fax";
	public static final String NEV = "nev";
	public static final String EMAIL = "email";
	public static final String TELEPULES = "telepules";

}

