package com.example.demo.domain;

import java.time.LocalDateTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AbstractDomain.class)
public abstract class AbstractDomain_ {

	public static volatile SingularAttribute<AbstractDomain, LocalDateTime> edited;
	public static volatile SingularAttribute<AbstractDomain, LocalDateTime> created;
	public static volatile SingularAttribute<AbstractDomain, Integer> vers;
	public static volatile SingularAttribute<AbstractDomain, Long> id;

	public static final String EDITED = "edited";
	public static final String CREATED = "created";
	public static final String VERS = "vers";
	public static final String ID = "id";

}

