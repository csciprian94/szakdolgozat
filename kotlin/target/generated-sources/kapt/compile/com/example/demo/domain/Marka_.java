package com.example.demo.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Marka.class)
public abstract class Marka_ {

	public static volatile SingularAttribute<Marka, Long> id;
	public static volatile SingularAttribute<Marka, String> nev;

	public static final String ID = "id";
	public static final String NEV = "nev";

}

