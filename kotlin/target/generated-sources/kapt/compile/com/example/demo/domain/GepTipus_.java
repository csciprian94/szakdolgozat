package com.example.demo.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GepTipus.class)
public abstract class GepTipus_ {

	public static volatile SingularAttribute<GepTipus, Long> id;
	public static volatile SingularAttribute<GepTipus, String> nev;

	public static final String ID = "id";
	public static final String NEV = "nev";

}

