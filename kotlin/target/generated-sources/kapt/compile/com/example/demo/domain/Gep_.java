package com.example.demo.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Gep.class)
public abstract class Gep_ extends com.example.demo.domain.AbstractDomain_ {

	public static volatile SingularAttribute<Gep, Marka> marka;
	public static volatile SingularAttribute<Gep, GepTipus> gepTipus;
	public static volatile SingularAttribute<Gep, Integer> suly;
	public static volatile SingularAttribute<Gep, Integer> gyartasEve;
	public static volatile SingularAttribute<Gep, Integer> uzemIdo;
	public static volatile SingularAttribute<Gep, String> nev;
	public static volatile SingularAttribute<Gep, Telephely> telephely;
	public static volatile SingularAttribute<Gep, LocalDate> cegTulajdona;

	public static final String MARKA = "marka";
	public static final String GEP_TIPUS = "gepTipus";
	public static final String SULY = "suly";
	public static final String GYARTAS_EVE = "gyartasEve";
	public static final String UZEM_IDO = "uzemIdo";
	public static final String NEV = "nev";
	public static final String TELEPHELY = "telephely";
	public static final String CEG_TULAJDONA = "cegTulajdona";

}

