package com.example.demo.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Megye.class)
public abstract class Megye_ {

	public static volatile SingularAttribute<Megye, String> megyenev;
	public static volatile SingularAttribute<Megye, Long> id;

	public static final String MEGYENEV = "megyenev";
	public static final String ID = "id";

}

