package com.example.demo.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Dolgozo.class)
public abstract class Dolgozo_ extends com.example.demo.domain.AbstractDomain_ {

	public static volatile SingularAttribute<Dolgozo, Beosztas> beosztas;
	public static volatile SingularAttribute<Dolgozo, String> keresztkNev;
	public static volatile SingularAttribute<Dolgozo, String> telefonSzam;
	public static volatile SingularAttribute<Dolgozo, LocalDate> munkaViszonyKezdete;
	public static volatile SingularAttribute<Dolgozo, Integer> iranyitoSzam;
	public static volatile SingularAttribute<Dolgozo, String> cim;
	public static volatile SingularAttribute<Dolgozo, LocalDate> szulIdo;
	public static volatile SingularAttribute<Dolgozo, String> lakhely;
	public static volatile SingularAttribute<Dolgozo, Long> fizetes;
	public static volatile SingularAttribute<Dolgozo, String> vezetekNev;
	public static volatile SingularAttribute<Dolgozo, Telephely> telephely;

	public static final String BEOSZTAS = "beosztas";
	public static final String KERESZTK_NEV = "keresztkNev";
	public static final String TELEFON_SZAM = "telefonSzam";
	public static final String MUNKA_VISZONY_KEZDETE = "munkaViszonyKezdete";
	public static final String IRANYITO_SZAM = "iranyitoSzam";
	public static final String CIM = "cim";
	public static final String SZUL_IDO = "szulIdo";
	public static final String LAKHELY = "lakhely";
	public static final String FIZETES = "fizetes";
	public static final String VEZETEK_NEV = "vezetekNev";
	public static final String TELEPHELY = "telephely";

}

