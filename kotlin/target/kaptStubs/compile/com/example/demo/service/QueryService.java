package com.example.demo.service;

import java.lang.System;

/**
 * * Base service for constructing and executing complex queries.
 * *
 * * @param <ENTITY> the type of the entity which is queried.
 * </ENTITY> 
 */
@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\"\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u001e\n\u0002\b\u0003\b\'\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004JB\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u000e\b\u0002\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\n2\u0014\u0010\u000b\u001a\u0010\u0012\u0006\b\u0000\u0012\u00028\u0000\u0012\u0004\u0012\u0002H\u00070\fH\u0014J|\u0010\r\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0006\"\u0004\b\u0002\u0010\u000e\"\u0004\b\u0003\u0010\u000f\"\u0004\b\u0004\u0010\u00072\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\u00102$\u0010\u0011\u001a \u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u000e0\u00140\u00122$\u0010\u0015\u001a \u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u000e0\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u0012H\u0014JT\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u0004\b\u0002\u0010\u000e\"\u0004\b\u0003\u0010\u00072\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\u00102\u0014\u0010\u0017\u001a\u0010\u0012\u0006\b\u0000\u0012\u00028\u0000\u0012\u0004\u0012\u0002H\u000e0\f2\u0014\u0010\u0018\u001a\u0010\u0012\u0006\b\u0000\u0012\u0002H\u000e\u0012\u0004\u0012\u0002H\u00070\fH\u0014J\u0084\u0001\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u0004\b\u0002\u0010\u000e\"\u0004\b\u0003\u0010\u000f\"\u000e\b\u0004\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\n2$\u0010\u0011\u001a \u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u000e0\u00140\u00122$\u0010\u0015\u001a \u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u000e0\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u0012H\u0014J|\u0010\u0019\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0006\"\u0004\b\u0002\u0010\u000e\"\u0004\b\u0003\u0010\u000f\"\u0004\b\u0004\u0010\u00072\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\u00102$\u0010\u0011\u001a \u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u000e0\u001a0\u00122$\u0010\u0015\u001a \u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u000e0\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u0012H\u0014J\u0084\u0001\u0010\u0019\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u0004\b\u0002\u0010\u000e\"\u0004\b\u0003\u0010\u000f\"\u000e\b\u0004\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\n2$\u0010\u0011\u001a \u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u000e0\u001a0\u00122$\u0010\u0015\u001a \u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u000e0\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u0012H\u0014JB\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u0004\b\u0002\u0010\u00072\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\u00102\u001e\u0010\u001c\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u0012H\u0014J:\u0010\u001b\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0006\"\u0004\b\u0002\u0010\u00072\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\u00102\u0014\u0010\u000b\u001a\u0010\u0012\u0006\b\u0000\u0012\u00028\u0000\u0012\u0004\u0012\u0002H\u00070\fH\u0014JL\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u000e\b\u0002\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\n2\u001e\u0010\u001c\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u0012H\u0014J6\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\u00000\u00062\u0006\u0010\t\u001a\u00020\u001d2\u001e\u0010\u001c\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001e0\u00160\u0012H\u0014J,\u0010\u001f\u001a\b\u0012\u0004\u0012\u00028\u00000\u00062\u0006\u0010\t\u001a\u00020\u001d2\u0014\u0010\u000b\u001a\u0010\u0012\u0006\b\u0000\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001e0\fH\u0014JB\u0010 \u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u0004\b\u0002\u0010\u00072$\u0010\u001c\u001a \u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070!0\u00160\u00122\u0006\u0010\"\u001a\u00020#H\u0014J<\u0010$\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u0004\b\u0002\u0010\u00072\u001e\u0010\u001c\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u00122\u0006\u0010\"\u001a\u00020#H\u0014J\u001b\u0010%\u001a\b\u0012\u0004\u0012\u00028\u00000\u00062\u0006\u0010&\u001a\u00028\u0001H$\u00a2\u0006\u0002\u0010\'JC\u0010(\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u0004\b\u0002\u0010\u00072\u001e\u0010\u001c\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u00122\b\u0010)\u001a\u0004\u0018\u0001H\u0007H\u0014\u00a2\u0006\u0002\u0010*J#\u0010+\u001a\b\u0012\u0004\u0012\u00028\u00000,2\u0006\u0010&\u001a\u00028\u00012\u0006\u0010-\u001a\u00020.H&\u00a2\u0006\u0002\u0010/JM\u00100\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u000e\b\u0002\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070\b2\u001e\u0010\u001c\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u00122\b\u0010)\u001a\u0004\u0018\u0001H\u0007H\u0014\u00a2\u0006\u0002\u00101JM\u00102\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u000e\b\u0002\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070\b2\u001e\u0010\u001c\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u00122\b\u0010)\u001a\u0004\u0018\u0001H\u0007H\u0014\u00a2\u0006\u0002\u00101JM\u00103\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u000e\b\u0002\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070\b2\u001e\u0010\u001c\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u00122\b\u0010)\u001a\u0004\u0018\u0001H\u0007H\u0014\u00a2\u0006\u0002\u00101JM\u00104\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u000e\b\u0002\u0010\u0007*\b\u0012\u0004\u0012\u0002H\u00070\b2\u001e\u0010\u001c\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u00122\b\u0010)\u001a\u0004\u0018\u0001H\u0007H\u0014\u00a2\u0006\u0002\u00101J8\u00105\u001a\b\u0012\u0004\u0012\u00028\u00000\u00062\u001e\u0010\u001c\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001e0\u00160\u00122\b\u0010)\u001a\u0004\u0018\u00010\u001eH\u0014JA\u00106\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u0004\b\u0002\u0010\u00072\u001e\u0010\u001c\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u00122\u0006\u0010)\u001a\u0002H\u0007H\u0014\u00a2\u0006\u0002\u0010*JD\u00107\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\"\u0004\b\u0002\u0010\u00072\u001e\u0010\u001c\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00070\u00160\u00122\u000e\u00108\u001a\n\u0012\u0004\u0012\u0002H\u0007\u0018\u000109H\u0014J\u0012\u0010:\u001a\u00020\u001e2\b\u0010;\u001a\u0004\u0018\u00010\u001eH\u0014\u00a8\u0006<"}, d2 = {"Lcom/example/demo/service/QueryService;", "ENTITY", "C", "", "()V", "buildRangeSpecification", "Lorg/springframework/data/jpa/domain/Specification;", "X", "", "filter", "Lcom/example/demo/filter/RangeFilter;", "field", "Ljavax/persistence/metamodel/SingularAttribute;", "buildReferringEntitySpecification", "OTHER", "MISC", "Lcom/example/demo/filter/Filter;", "functionToEntity", "Ljava/util/function/Function;", "Ljavax/persistence/criteria/Root;", "Ljavax/persistence/criteria/Join;", "entityToColumn", "Ljavax/persistence/criteria/Expression;", "reference", "valueField", "buildReferringEntitySpecificationSet", "Ljavax/persistence/criteria/SetJoin;", "buildSpecification", "metaclassFunction", "Lcom/example/demo/filter/StringFilter;", "", "buildStringSpecification", "byFieldEmptiness", "", "specified", "", "byFieldSpecified", "createSpecification", "criteria", "(Ljava/lang/Object;)Lorg/springframework/data/jpa/domain/Specification;", "equalsSpecification", "value", "(Ljava/util/function/Function;Ljava/lang/Object;)Lorg/springframework/data/jpa/domain/Specification;", "findByCriteria", "Lorg/springframework/data/domain/Page;", "page", "Lorg/springframework/data/domain/Pageable;", "(Ljava/lang/Object;Lorg/springframework/data/domain/Pageable;)Lorg/springframework/data/domain/Page;", "greaterThan", "(Ljava/util/function/Function;Ljava/lang/Comparable;)Lorg/springframework/data/jpa/domain/Specification;", "greaterThanOrEqualTo", "lessThan", "lessThanOrEqualTo", "likeUpperSpecification", "notEqualsSpecification", "valueIn", "values", "", "wrapLikeQuery", "txt", "demo"})
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public abstract class QueryService<ENTITY extends java.lang.Object, C extends java.lang.Object> {
    
    @org.jetbrains.annotations.NotNull()
    public abstract org.springframework.data.domain.Page<ENTITY> findByCriteria(C criteria, @org.jetbrains.annotations.NotNull()
    org.springframework.data.domain.Pageable page);
    
    @org.jetbrains.annotations.NotNull()
    protected abstract org.springframework.data.jpa.domain.Specification<ENTITY> createSpecification(C criteria);
    
    /**
     * * Helper function to return a specification for filtering on a single field, where equality, and null/non-null
     *     * conditions are supported.
     *     *
     *     * @param filter the individual attribute filter coming from the frontend.
     *     * @param field  the JPA static metamodel representing the field.
     *     * @param <X>    The type of the attribute which is filtered.
     *     * @return a Specification
     *    </X> 
     */
    @org.jetbrains.annotations.Nullable()
    protected <X extends java.lang.Object>org.springframework.data.jpa.domain.Specification<ENTITY> buildSpecification(@org.jetbrains.annotations.NotNull()
    com.example.demo.filter.Filter<X> filter, @org.jetbrains.annotations.NotNull()
    javax.persistence.metamodel.SingularAttribute<? super ENTITY, X> field) {
        return null;
    }
    
    /**
     * * Helper function to return a specification for filtering on a single field, where equality, and null/non-null
     *     * conditions are supported.
     *     *
     *     * @param filter            the individual attribute filter coming from the frontend.
     *     * @param metaclassFunction the function, which navigates from the current entity to a column, for which the filter applies.
     *     * @param <X>               The type of the attribute which is filtered.
     *     * @return a Specification
     *    </X> 
     */
    @org.jetbrains.annotations.NotNull()
    protected <X extends java.lang.Object>org.springframework.data.jpa.domain.Specification<ENTITY> buildSpecification(@org.jetbrains.annotations.NotNull()
    com.example.demo.filter.Filter<X> filter, @org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Expression<X>> metaclassFunction) {
        return null;
    }
    
    /**
     * * Helper function to return a specification for filtering on a [String] field, where equality, containment,
     *     * and null/non-null conditions are supported.
     *     *
     *     * @param filter the individual attribute filter coming from the frontend.
     *     * @param field  the JPA static metamodel representing the field.
     *     * @return a Specification
     */
    @org.jetbrains.annotations.NotNull()
    protected org.springframework.data.jpa.domain.Specification<ENTITY> buildStringSpecification(@org.jetbrains.annotations.NotNull()
    com.example.demo.filter.StringFilter filter, @org.jetbrains.annotations.NotNull()
    javax.persistence.metamodel.SingularAttribute<? super ENTITY, java.lang.String> field) {
        return null;
    }
    
    /**
     * * Helper function to return a specification for filtering on a [String] field, where equality, containment,
     *     * and null/non-null conditions are supported.
     *     *
     *     * @param filter            the individual attribute filter coming from the frontend.
     *     * @param metaclassFunction lambda, which based on a Root&lt;ENTITY&gt; returns Expression - basicaly picks a column
     *     * @return a Specification
     */
    @org.jetbrains.annotations.NotNull()
    protected org.springframework.data.jpa.domain.Specification<ENTITY> buildSpecification(@org.jetbrains.annotations.NotNull()
    com.example.demo.filter.StringFilter filter, @org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Expression<java.lang.String>> metaclassFunction) {
        return null;
    }
    
    /**
     * * Helper function to return a specification for filtering on a single [Comparable], where equality, less
     *     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     *     * supported.
     *     *
     *     * @param filter the individual attribute filter coming from the frontend.
     *     * @param field  the JPA static metamodel representing the field.
     *     * @param <X>    The type of the attribute which is filtered.
     *     * @return a Specification
     *    </X> 
     */
    @org.jetbrains.annotations.NotNull()
    protected <X extends java.lang.Comparable<? super X>>org.springframework.data.jpa.domain.Specification<ENTITY> buildRangeSpecification(@org.jetbrains.annotations.NotNull()
    com.example.demo.filter.RangeFilter<X> filter, @org.jetbrains.annotations.NotNull()
    javax.persistence.metamodel.SingularAttribute<? super ENTITY, X> field) {
        return null;
    }
    
    /**
     * * Helper function to return a specification for filtering on a single [Comparable], where equality, less
     *     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     *     * supported.
     *     *
     *     * @param filter            the individual attribute filter coming from the frontend.
     *     * @param metaclassFunction lambda, which based on a Root&lt;ENTITY&gt; returns Expression - basicaly picks a column
     *     * @param <X>               The type of the attribute which is filtered.
     *     * @return a Specification
     *    </X> 
     */
    @org.jetbrains.annotations.NotNull()
    protected <X extends java.lang.Comparable<? super X>>org.springframework.data.jpa.domain.Specification<ENTITY> buildSpecification(@org.jetbrains.annotations.NotNull()
    com.example.demo.filter.RangeFilter<X> filter, @org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Expression<X>> metaclassFunction) {
        return null;
    }
    
    /**
     * * Helper function to return a specification for filtering on one-to-one or many-to-one reference. Usage:
     *     * <pre>
     *     * Specification&lt;Employee&gt; specByProjectId = buildReferringEntitySpecification(criteria.getProjectId(),
     *     * Employee_.project, Project_.id);
     *     * Specification&lt;Employee&gt; specByProjectName = buildReferringEntitySpecification(criteria.getProjectName(),
     *     * Employee_.project, Project_.name);
     *    </pre> *
     *     *
     *     * @param filter     the filter object which contains a value, which needs to match or a flag if nullness is
     *     * checked.
     *     * @param reference  the attribute of the static metamodel for the referring entity.
     *     * @param valueField the attribute of the static metamodel of the referred entity, where the equality should be
     *     * checked.
     *     * @param <OTHER>    The type of the referenced entity.
     *     * @param <X>        The type of the attribute which is filtered.
     *     * @return a Specification
     *    </X></OTHER> 
     */
    @org.jetbrains.annotations.NotNull()
    protected <OTHER extends java.lang.Object, X extends java.lang.Object>org.springframework.data.jpa.domain.Specification<ENTITY> buildReferringEntitySpecification(@org.jetbrains.annotations.NotNull()
    com.example.demo.filter.Filter<X> filter, @org.jetbrains.annotations.NotNull()
    javax.persistence.metamodel.SingularAttribute<? super ENTITY, OTHER> reference, @org.jetbrains.annotations.NotNull()
    javax.persistence.metamodel.SingularAttribute<? super OTHER, X> valueField) {
        return null;
    }
    
    /**
     * * Helper function to return a specification for filtering on one-to-many or many-to-many reference. Usage:
     *     * <pre>
     *     * Specification&lt;Employee&gt; specByEmployeeId = buildReferringEntitySpecification(
     *     * criteria.getEmployeId(),
     *     * root -&gt; root.get(Project_.company).join(Company_.employees),
     *     * entity -&gt; entity.get(Employee_.id));
     *     * Specification&lt;Employee&gt; specByProjectName = buildReferringEntitySpecification(
     *     * criteria.getProjectName(),
     *     * root -&gt; root.get(Project_.project)
     *     * entity -&gt; entity.get(Project_.name));
     *    </pre> *
     *     *
     *     * @param filter           the filter object which contains a value, which needs to match or a flag if emptiness is
     *     * checked.
     *     * @param functionToEntity the function, which joins he current entity to the entity set, on which the filtering is applied.
     *     * @param entityToColumn   the function, which of the static metamodel of the referred entity, where the equality should be
     *     * checked.
     *     * @param <OTHER>          The type of the referenced entity.
     *     * @param <X>              The type of the attribute which is filtered.
     *     * @return a Specification
     *    </X></OTHER> 
     */
    @org.jetbrains.annotations.Nullable()
    protected <OTHER extends java.lang.Object, MISC extends java.lang.Object, X extends java.lang.Object>org.springframework.data.jpa.domain.Specification<ENTITY> buildReferringEntitySpecificationSet(@org.jetbrains.annotations.NotNull()
    com.example.demo.filter.Filter<X> filter, @org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.SetJoin<MISC, OTHER>> functionToEntity, @org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.SetJoin<MISC, OTHER>, javax.persistence.criteria.Expression<X>> entityToColumn) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    protected <OTHER extends java.lang.Object, MISC extends java.lang.Object, X extends java.lang.Object>org.springframework.data.jpa.domain.Specification<ENTITY> buildReferringEntitySpecification(@org.jetbrains.annotations.NotNull()
    com.example.demo.filter.Filter<X> filter, @org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Join<MISC, OTHER>> functionToEntity, @org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Join<MISC, OTHER>, javax.persistence.criteria.Expression<X>> entityToColumn) {
        return null;
    }
    
    /**
     * * Helper function to return a specification for filtering on one-to-many or many-to-many reference. Where equality, less
     *     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     *     * supported. Usage:
     *     * <pre>`
     *     * Specification<Employee> specByEmployeeId = buildReferringEntitySpecification(
     *     * criteria.getEmployeId(),
     *     * root -> root.get(Project_.company).join(Company_.employees),
     *     * entity -> entity.get(Employee_.id));
     *     * Specification<Employee> specByProjectName = buildReferringEntitySpecification(
     *     * criteria.getProjectName(),
     *     * root -> root.get(Project_.project)
     *     * entity -> entity.get(Project_.name));
     *    ` *
     *    </pre> *
     *     *
     *     * @param filter           the filter object which contains a value, which needs to match or a flag if emptiness is
     *     * checked.
     *     * @param functionToEntity the function, which joins he current entity to the entity set, on which the filtering is applied.
     *     * @param entityToColumn   the function, which of the static metamodel of the referred entity, where the equality should be
     *     * checked.
     *     * @param <OTHER>          The type of the referenced entity.
     *     * @param <MISC>           The type of the entity which is the last before the OTHER in the chain.
     *     * @param <X>              The type of the attribute which is filtered.
     *     * @return a Specification
     *    </X></MISC></OTHER> 
     */
    @org.jetbrains.annotations.NotNull()
    protected <OTHER extends java.lang.Object, MISC extends java.lang.Object, X extends java.lang.Comparable<? super X>>org.springframework.data.jpa.domain.Specification<ENTITY> buildReferringEntitySpecificationSet(@org.jetbrains.annotations.NotNull()
    com.example.demo.filter.RangeFilter<X> filter, @org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.SetJoin<MISC, OTHER>> functionToEntity, @org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.SetJoin<MISC, OTHER>, javax.persistence.criteria.Expression<X>> entityToColumn) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected <OTHER extends java.lang.Object, MISC extends java.lang.Object, X extends java.lang.Comparable<? super X>>org.springframework.data.jpa.domain.Specification<ENTITY> buildReferringEntitySpecification(@org.jetbrains.annotations.NotNull()
    com.example.demo.filter.RangeFilter<X> filter, @org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Join<MISC, OTHER>> functionToEntity, @org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Join<MISC, OTHER>, javax.persistence.criteria.Expression<X>> entityToColumn) {
        return null;
    }
    
    /**
     * * Generic method, which based on a Root&lt;ENTITY&gt; returns an Expression which type is the same as the given 'value' type.
     *     *
     *     * @param metaclassFunction function which returns the column which is used for filtering.
     *     * @param value             the actual value to filter for.
     *     * @param <X>               The type of the attribute which is filtered.
     *     * @return a Specification.
     *    </X> 
     */
    @org.jetbrains.annotations.NotNull()
    protected <X extends java.lang.Object>org.springframework.data.jpa.domain.Specification<ENTITY> equalsSpecification(@org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Expression<X>> metaclassFunction, @org.jetbrains.annotations.Nullable()
    X value) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected <X extends java.lang.Object>org.springframework.data.jpa.domain.Specification<ENTITY> notEqualsSpecification(@org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Expression<X>> metaclassFunction, X value) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected org.springframework.data.jpa.domain.Specification<ENTITY> likeUpperSpecification(@org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Expression<java.lang.String>> metaclassFunction, @org.jetbrains.annotations.Nullable()
    java.lang.String value) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected <X extends java.lang.Object>org.springframework.data.jpa.domain.Specification<ENTITY> byFieldSpecified(@org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Expression<X>> metaclassFunction, boolean specified) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected <X extends java.lang.Object>org.springframework.data.jpa.domain.Specification<ENTITY> byFieldEmptiness(@org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Expression<java.util.Set<X>>> metaclassFunction, boolean specified) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected <X extends java.lang.Object>org.springframework.data.jpa.domain.Specification<ENTITY> valueIn(@org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Expression<X>> metaclassFunction, @org.jetbrains.annotations.Nullable()
    java.util.Collection<? extends X> values) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected <X extends java.lang.Comparable<? super X>>org.springframework.data.jpa.domain.Specification<ENTITY> greaterThanOrEqualTo(@org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Expression<X>> metaclassFunction, @org.jetbrains.annotations.Nullable()
    X value) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected <X extends java.lang.Comparable<? super X>>org.springframework.data.jpa.domain.Specification<ENTITY> greaterThan(@org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Expression<X>> metaclassFunction, @org.jetbrains.annotations.Nullable()
    X value) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected <X extends java.lang.Comparable<? super X>>org.springframework.data.jpa.domain.Specification<ENTITY> lessThanOrEqualTo(@org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Expression<X>> metaclassFunction, @org.jetbrains.annotations.Nullable()
    X value) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected <X extends java.lang.Comparable<? super X>>org.springframework.data.jpa.domain.Specification<ENTITY> lessThan(@org.jetbrains.annotations.NotNull()
    java.util.function.Function<javax.persistence.criteria.Root<ENTITY>, javax.persistence.criteria.Expression<X>> metaclassFunction, @org.jetbrains.annotations.Nullable()
    X value) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    protected java.lang.String wrapLikeQuery(@org.jetbrains.annotations.Nullable()
    java.lang.String txt) {
        return null;
    }
    
    public QueryService() {
        super();
    }
}