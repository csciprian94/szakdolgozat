package com.example.demo.service;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0017\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00020\b2\u0006\u0010\t\u001a\u00020\u0003H\u0014J\u001e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00020\u000b2\u0006\u0010\t\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\rH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0092\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/example/demo/service/DolgozoQueryService;", "Lcom/example/demo/service/QueryService;", "Lcom/example/demo/domain/Dolgozo;", "Lcom/example/demo/criteria/DolgozoCriteria;", "repository", "Lcom/example/demo/repository/DolgozoRepository;", "(Lcom/example/demo/repository/DolgozoRepository;)V", "createSpecification", "Lorg/springframework/data/jpa/domain/Specification;", "criteria", "findByCriteria", "Lorg/springframework/data/domain/Page;", "page", "Lorg/springframework/data/domain/Pageable;", "demo"})
@org.springframework.transaction.annotation.Transactional(readOnly = true)
@org.springframework.stereotype.Service()
public class DolgozoQueryService extends com.example.demo.service.QueryService<com.example.demo.domain.Dolgozo, com.example.demo.criteria.DolgozoCriteria> {
    private final com.example.demo.repository.DolgozoRepository repository = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.springframework.data.domain.Page<com.example.demo.domain.Dolgozo> findByCriteria(@org.jetbrains.annotations.NotNull()
    com.example.demo.criteria.DolgozoCriteria criteria, @org.jetbrains.annotations.NotNull()
    org.springframework.data.domain.Pageable page) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    protected org.springframework.data.jpa.domain.Specification<com.example.demo.domain.Dolgozo> createSpecification(@org.jetbrains.annotations.NotNull()
    com.example.demo.criteria.DolgozoCriteria criteria) {
        return null;
    }
    
    public DolgozoQueryService(@org.jetbrains.annotations.NotNull()
    com.example.demo.repository.DolgozoRepository repository) {
        super();
    }
}