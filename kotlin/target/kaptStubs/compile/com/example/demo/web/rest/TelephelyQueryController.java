package com.example.demo.web.rest;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0017\u0018\u00002\u00020\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J$\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u000e0\r2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0017R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u001a\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u0013"}, d2 = {"Lcom/example/demo/web/rest/TelephelyQueryController;", "", "resourcesAssembler", "Lorg/springframework/data/web/PagedResourcesAssembler;", "Lcom/example/demo/domain/Telephely;", "queryService", "Lcom/example/demo/service/TelephelyQueryService;", "(Lorg/springframework/data/web/PagedResourcesAssembler;Lcom/example/demo/service/TelephelyQueryService;)V", "getQueryService", "()Lcom/example/demo/service/TelephelyQueryService;", "getResourcesAssembler", "()Lorg/springframework/data/web/PagedResourcesAssembler;", "query", "Lorg/springframework/hateoas/PagedResources;", "Lorg/springframework/hateoas/Resource;", "criteria", "Lcom/example/demo/criteria/TelephelyCriteria;", "page", "Lorg/springframework/data/domain/Pageable;", "demo"})
@org.springframework.web.bind.annotation.RequestMapping(value = {"/api/telephely"})
@org.springframework.web.bind.annotation.RestController()
public class TelephelyQueryController {
    @org.jetbrains.annotations.NotNull()
    private final org.springframework.data.web.PagedResourcesAssembler<com.example.demo.domain.Telephely> resourcesAssembler = null;
    @org.jetbrains.annotations.NotNull()
    private final com.example.demo.service.TelephelyQueryService queryService = null;
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.web.bind.annotation.GetMapping(value = {"/query"})
    public org.springframework.hateoas.PagedResources<org.springframework.hateoas.Resource<com.example.demo.domain.Telephely>> query(@org.jetbrains.annotations.NotNull()
    com.example.demo.criteria.TelephelyCriteria criteria, @org.jetbrains.annotations.NotNull()
    org.springframework.data.domain.Pageable page) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public org.springframework.data.web.PagedResourcesAssembler<com.example.demo.domain.Telephely> getResourcesAssembler() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.demo.service.TelephelyQueryService getQueryService() {
        return null;
    }
    
    public TelephelyQueryController(@org.jetbrains.annotations.NotNull()
    org.springframework.data.web.PagedResourcesAssembler<com.example.demo.domain.Telephely> resourcesAssembler, @org.jetbrains.annotations.NotNull()
    com.example.demo.service.TelephelyQueryService queryService) {
        super();
    }
}