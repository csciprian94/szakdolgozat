package com.example.demo.config;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0017\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"}, d2 = {"Lcom/example/demo/config/DateTimeFormatterConfiguration;", "Lorg/springframework/web/servlet/config/annotation/WebMvcConfigurer;", "()V", "addFormatters", "", "registry", "Lorg/springframework/format/FormatterRegistry;", "demo"})
@org.springframework.context.annotation.Configuration()
public class DateTimeFormatterConfiguration implements org.springframework.web.servlet.config.annotation.WebMvcConfigurer {
    
    @java.lang.Override()
    public void addFormatters(@org.jetbrains.annotations.NotNull()
    org.springframework.format.FormatterRegistry registry) {
    }
    
    public DateTimeFormatterConfiguration() {
        super();
    }
}