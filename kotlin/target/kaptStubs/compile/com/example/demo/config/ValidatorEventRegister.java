package com.example.demo.config;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0017\u0018\u00002\u00020\u0001B#\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0002\u0010\bJ\b\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0092\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005X\u0092\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/example/demo/config/ValidatorEventRegister;", "Lorg/springframework/beans/factory/InitializingBean;", "validatingRepositoryEventListener", "Lorg/springframework/data/rest/core/event/ValidatingRepositoryEventListener;", "validators", "", "", "Lorg/springframework/validation/Validator;", "(Lorg/springframework/data/rest/core/event/ValidatingRepositoryEventListener;Ljava/util/Map;)V", "afterPropertiesSet", "", "demo"})
@org.springframework.context.annotation.Configuration()
public class ValidatorEventRegister implements org.springframework.beans.factory.InitializingBean {
    private final org.springframework.data.rest.core.event.ValidatingRepositoryEventListener validatingRepositoryEventListener = null;
    private final java.util.Map<java.lang.String, org.springframework.validation.Validator> validators = null;
    
    @java.lang.Override()
    public void afterPropertiesSet() {
    }
    
    @org.springframework.beans.factory.annotation.Autowired()
    public ValidatorEventRegister(@org.jetbrains.annotations.NotNull()
    org.springframework.data.rest.core.event.ValidatingRepositoryEventListener validatingRepositoryEventListener, @org.jetbrains.annotations.NotNull()
    java.util.Map<java.lang.String, ? extends org.springframework.validation.Validator> validators) {
        super();
    }
}