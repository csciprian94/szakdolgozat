package com.example.demo.config;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0017\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016J\u0012\u0010\t\u001a\u00020\u00062\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016J\b\u0010\f\u001a\u00020\rH\u0017R\u000e\u0010\u0002\u001a\u00020\u0003X\u0092\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/example/demo/config/RepositoryRestResourceConfiguration;", "Lorg/springframework/data/rest/webmvc/config/RepositoryRestConfigurer;", "validator", "Lorg/springframework/validation/beanvalidation/LocalValidatorFactoryBean;", "(Lorg/springframework/validation/beanvalidation/LocalValidatorFactoryBean;)V", "configureRepositoryRestConfiguration", "", "config", "Lorg/springframework/data/rest/core/config/RepositoryRestConfiguration;", "configureValidatingRepositoryEventListener", "validatingListener", "Lorg/springframework/data/rest/core/event/ValidatingRepositoryEventListener;", "projectionFactory", "Lorg/springframework/data/projection/SpelAwareProxyProjectionFactory;", "demo"})
@org.springframework.context.annotation.Configuration()
public class RepositoryRestResourceConfiguration implements org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer {
    private final org.springframework.validation.beanvalidation.LocalValidatorFactoryBean validator = null;
    
    @java.lang.Override()
    public void configureRepositoryRestConfiguration(@org.jetbrains.annotations.Nullable()
    org.springframework.data.rest.core.config.RepositoryRestConfiguration config) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @org.springframework.context.annotation.Bean()
    public org.springframework.data.projection.SpelAwareProxyProjectionFactory projectionFactory() {
        return null;
    }
    
    @java.lang.Override()
    public void configureValidatingRepositoryEventListener(@org.jetbrains.annotations.Nullable()
    org.springframework.data.rest.core.event.ValidatingRepositoryEventListener validatingListener) {
    }
    
    public RepositoryRestResourceConfiguration(@org.jetbrains.annotations.NotNull()
    org.springframework.validation.beanvalidation.LocalValidatorFactoryBean validator) {
        super();
    }
}