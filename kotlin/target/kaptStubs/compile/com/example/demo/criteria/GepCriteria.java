package com.example.demo.criteria;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001c\u0010\f\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\bR\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001c\u0010\u0015\u001a\u0004\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0012\"\u0004\b\u0017\u0010\u0014R\u001c\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u001c\u0010\u001e\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u0006\"\u0004\b \u0010\b\u00a8\u0006!"}, d2 = {"Lcom/example/demo/criteria/GepCriteria;", "", "()V", "geptipId", "Lcom/example/demo/filter/LongFilter;", "getGeptipId", "()Lcom/example/demo/filter/LongFilter;", "setGeptipId", "(Lcom/example/demo/filter/LongFilter;)V", "id", "getId", "setId", "markaId", "getMarkaId", "setMarkaId", "mukodesKezdeteK", "Lcom/example/demo/filter/LocalDateFilter;", "getMukodesKezdeteK", "()Lcom/example/demo/filter/LocalDateFilter;", "setMukodesKezdeteK", "(Lcom/example/demo/filter/LocalDateFilter;)V", "mukodesKezdeteV", "getMukodesKezdeteV", "setMukodesKezdeteV", "nev", "Lcom/example/demo/filter/StringFilter;", "getNev", "()Lcom/example/demo/filter/StringFilter;", "setNev", "(Lcom/example/demo/filter/StringFilter;)V", "telephelyId", "getTelephelyId", "setTelephelyId", "demo"})
public final class GepCriteria {
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.LongFilter id;
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.StringFilter nev;
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.LongFilter telephelyId;
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.LongFilter geptipId;
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.LongFilter markaId;
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.LocalDateFilter mukodesKezdeteK;
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.LocalDateFilter mukodesKezdeteV;
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.LongFilter getId() {
        return null;
    }
    
    public final void setId(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.LongFilter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.StringFilter getNev() {
        return null;
    }
    
    public final void setNev(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.StringFilter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.LongFilter getTelephelyId() {
        return null;
    }
    
    public final void setTelephelyId(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.LongFilter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.LongFilter getGeptipId() {
        return null;
    }
    
    public final void setGeptipId(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.LongFilter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.LongFilter getMarkaId() {
        return null;
    }
    
    public final void setMarkaId(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.LongFilter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.LocalDateFilter getMukodesKezdeteK() {
        return null;
    }
    
    public final void setMukodesKezdeteK(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.LocalDateFilter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.LocalDateFilter getMukodesKezdeteV() {
        return null;
    }
    
    public final void setMukodesKezdeteV(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.LocalDateFilter p0) {
    }
    
    public GepCriteria() {
        super();
    }
}