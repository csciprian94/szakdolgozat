package com.example.demo.criteria;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001c\u0010\f\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001c\u0010\u0018\u001a\u0004\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0015\"\u0004\b\u001a\u0010\u0017R\u001c\u0010\u001b\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0006\"\u0004\b\u001d\u0010\bR\u001c\u0010\u001e\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u000f\"\u0004\b \u0010\u0011\u00a8\u0006!"}, d2 = {"Lcom/example/demo/criteria/DolgozoCriteria;", "", "()V", "beosztasId", "Lcom/example/demo/filter/LongFilter;", "getBeosztasId", "()Lcom/example/demo/filter/LongFilter;", "setBeosztasId", "(Lcom/example/demo/filter/LongFilter;)V", "id", "getId", "setId", "keresztNev", "Lcom/example/demo/filter/StringFilter;", "getKeresztNev", "()Lcom/example/demo/filter/StringFilter;", "setKeresztNev", "(Lcom/example/demo/filter/StringFilter;)V", "munkaViszonyKezdeteK", "Lcom/example/demo/filter/LocalDateFilter;", "getMunkaViszonyKezdeteK", "()Lcom/example/demo/filter/LocalDateFilter;", "setMunkaViszonyKezdeteK", "(Lcom/example/demo/filter/LocalDateFilter;)V", "munkaViszonyKezdeteV", "getMunkaViszonyKezdeteV", "setMunkaViszonyKezdeteV", "telephelyId", "getTelephelyId", "setTelephelyId", "vezetekNev", "getVezetekNev", "setVezetekNev", "demo"})
public class DolgozoCriteria {
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.LongFilter id;
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.StringFilter vezetekNev;
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.StringFilter keresztNev;
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.LongFilter beosztasId;
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.LongFilter telephelyId;
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.LocalDateFilter munkaViszonyKezdeteK;
    @org.jetbrains.annotations.Nullable()
    private com.example.demo.filter.LocalDateFilter munkaViszonyKezdeteV;
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.LongFilter getId() {
        return null;
    }
    
    public final void setId(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.LongFilter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.StringFilter getVezetekNev() {
        return null;
    }
    
    public final void setVezetekNev(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.StringFilter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.StringFilter getKeresztNev() {
        return null;
    }
    
    public final void setKeresztNev(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.StringFilter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.LongFilter getBeosztasId() {
        return null;
    }
    
    public final void setBeosztasId(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.LongFilter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.LongFilter getTelephelyId() {
        return null;
    }
    
    public final void setTelephelyId(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.LongFilter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.LocalDateFilter getMunkaViszonyKezdeteK() {
        return null;
    }
    
    public final void setMunkaViszonyKezdeteK(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.LocalDateFilter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.filter.LocalDateFilter getMunkaViszonyKezdeteV() {
        return null;
    }
    
    public final void setMunkaViszonyKezdeteV(@org.jetbrains.annotations.Nullable()
    com.example.demo.filter.LocalDateFilter p0) {
    }
    
    public DolgozoCriteria() {
        super();
    }
}