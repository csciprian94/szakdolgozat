package com.example.demo.filter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000f\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0016\u0018\u0000 \u001c*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003:\u0001\u001cB\u0005\u00a2\u0006\u0002\u0010\u0004J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0096\u0002J\u000f\u0010\u000e\u001a\u0004\u0018\u00018\u0000H\u0016\u00a2\u0006\u0002\u0010\u000fJ\u000f\u0010\u0010\u001a\u0004\u0018\u00018\u0000H\u0016\u00a2\u0006\u0002\u0010\u000fJ\u000f\u0010\u0011\u001a\u0004\u0018\u00018\u0000H\u0016\u00a2\u0006\u0002\u0010\u000fJ\u000f\u0010\u0012\u001a\u0004\u0018\u00018\u0000H\u0016\u00a2\u0006\u0002\u0010\u000fJ\b\u0010\u0013\u001a\u00020\u0014H\u0016J\u001b\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u0005\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0016J\u001b\u0010\u0017\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u0007\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0016J\u001b\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\b\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0016J\u001b\u0010\u0019\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\t\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0016J\b\u0010\u001a\u001a\u00020\u001bH\u0016R\u0012\u0010\u0005\u001a\u0004\u0018\u00018\u0000X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0012\u0010\u0007\u001a\u0004\u0018\u00018\u0000X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0012\u0010\b\u001a\u0004\u0018\u00018\u0000X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0012\u0010\t\u001a\u0004\u0018\u00018\u0000X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006\u00a8\u0006\u001d"}, d2 = {"Lcom/example/demo/filter/RangeFilter;", "FIELD_TYPE", "", "Lcom/example/demo/filter/Filter;", "()V", "greaterOrEqualThan", "Ljava/lang/Comparable;", "greaterThan", "lessOrEqualThan", "lessThan", "equals", "", "o", "", "getGreaterOrEqualThan", "()Ljava/lang/Comparable;", "getGreaterThan", "getLessOrEqualThan", "getLessThan", "hashCode", "", "setGreaterOrEqualThan", "(Ljava/lang/Comparable;)Lcom/example/demo/filter/RangeFilter;", "setGreaterThan", "setLessOrEqualThan", "setLessThan", "toString", "", "Companion", "demo"})
public class RangeFilter<FIELD_TYPE extends java.lang.Comparable<? super FIELD_TYPE>> extends com.example.demo.filter.Filter<FIELD_TYPE> {
    private FIELD_TYPE greaterThan;
    private FIELD_TYPE lessThan;
    private FIELD_TYPE greaterOrEqualThan;
    private FIELD_TYPE lessOrEqualThan;
    private static final long serialVersionUID = 1L;
    public static final com.example.demo.filter.RangeFilter.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    public FIELD_TYPE getGreaterThan() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.demo.filter.RangeFilter<FIELD_TYPE> setGreaterThan(@org.jetbrains.annotations.NotNull()
    FIELD_TYPE greaterThan) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public FIELD_TYPE getGreaterOrEqualThan() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.demo.filter.RangeFilter<FIELD_TYPE> setGreaterOrEqualThan(@org.jetbrains.annotations.NotNull()
    FIELD_TYPE greaterOrEqualThan) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public FIELD_TYPE getLessThan() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.demo.filter.RangeFilter<FIELD_TYPE> setLessThan(@org.jetbrains.annotations.NotNull()
    FIELD_TYPE lessThan) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public FIELD_TYPE getLessOrEqualThan() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.demo.filter.RangeFilter<FIELD_TYPE> setLessOrEqualThan(@org.jetbrains.annotations.NotNull()
    FIELD_TYPE lessOrEqualThan) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object o) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public RangeFilter() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/example/demo/filter/RangeFilter$Companion;", "", "()V", "serialVersionUID", "", "demo"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}