package com.example.demo.filter;

import java.lang.System;

/**
 * * Filter class for [LocalDate] type attributes.
 * *
 * * @see RangeFilter
 */
@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\b\u0004\u0018\u0000 \r2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\rB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0002H\u0016J\u0010\u0010\u0006\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0002H\u0016J\u0010\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0002H\u0016J\u0016\u0010\b\u001a\u00020\u00002\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0002H\u0016J\u0010\u0010\f\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0002H\u0016\u00a8\u0006\u000e"}, d2 = {"Lcom/example/demo/filter/LocalDateFilter;", "Lcom/example/demo/filter/RangeFilter;", "Ljava/time/LocalDate;", "()V", "setEquals", "equals", "setGreaterOrEqualThan", "setGreaterThan", "setIn", "in", "", "setLessOrEqualThan", "setLessThan", "Companion", "demo"})
public final class LocalDateFilter extends com.example.demo.filter.RangeFilter<java.time.LocalDate> {
    private static final long serialVersionUID = 1L;
    public static final com.example.demo.filter.LocalDateFilter.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.demo.filter.LocalDateFilter setEquals(@org.jetbrains.annotations.NotNull()
    java.time.LocalDate equals) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.demo.filter.LocalDateFilter setGreaterThan(@org.jetbrains.annotations.NotNull()
    java.time.LocalDate equals) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.demo.filter.LocalDateFilter setGreaterOrEqualThan(@org.jetbrains.annotations.NotNull()
    java.time.LocalDate equals) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.demo.filter.LocalDateFilter setLessThan(@org.jetbrains.annotations.NotNull()
    java.time.LocalDate equals) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.demo.filter.LocalDateFilter setLessOrEqualThan(@org.jetbrains.annotations.NotNull()
    java.time.LocalDate equals) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.demo.filter.LocalDateFilter setIn(@org.jetbrains.annotations.NotNull()
    java.util.List<java.time.LocalDate> in) {
        return null;
    }
    
    public LocalDateFilter() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/example/demo/filter/LocalDateFilter$Companion;", "", "()V", "serialVersionUID", "", "demo"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}