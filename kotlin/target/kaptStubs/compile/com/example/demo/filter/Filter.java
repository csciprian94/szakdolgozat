package com.example.demo.filter;

import java.lang.System;

/**
 * * Base class for the various attribute filters. It can be added to a criteria class as a member, to support the
 * * following query parameters:
 * * <pre>
 * * fieldName.equals='something'
 * * fieldName.specified=true
 * * fieldName.specified=false
 * * fieldName.in='something','other'
 * </pre> *
 */
@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\b\b\u0016\u0018\u0000 \u001e*\u0004\b\u0000\u0010\u00012\u00020\u0002:\u0001\u001eB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0013\u0010\u0004\u001a\u00020\r2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0096\u0002J\r\u0010\u0011\u001a\u0004\u0018\u00018\u0000\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010\u0013\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u000bH\u0016J\r\u0010\u0014\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u0016\u001a\u00020\u0017H\u0016J\u001b\u0010\u0018\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u0004\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0019J\u001c\u0010\u001a\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\f\u0010\n\u001a\b\u0012\u0004\u0012\u00028\u00000\u000bH\u0016J\u001b\u0010\u001b\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\b\u0010\f\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0002\u0010\u001cJ\b\u0010\u001d\u001a\u00020\u0007H\u0016R\u0012\u0010\u0004\u001a\u0004\u0018\u00018\u0000X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0006\u001a\u00020\u00078DX\u0084\u0004\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0016\u0010\n\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000e\u00a8\u0006\u001f"}, d2 = {"Lcom/example/demo/filter/Filter;", "FIELD_TYPE", "Ljava/io/Serializable;", "()V", "equals", "Ljava/lang/Object;", "filterName", "", "getFilterName", "()Ljava/lang/String;", "in", "", "specified", "", "Ljava/lang/Boolean;", "o", "", "getEquals", "()Ljava/lang/Object;", "getIn", "getSpecified", "()Ljava/lang/Boolean;", "hashCode", "", "setEquals", "(Ljava/lang/Object;)Lcom/example/demo/filter/Filter;", "setIn", "setSpecified", "(Ljava/lang/Boolean;)Lcom/example/demo/filter/Filter;", "toString", "Companion", "demo"})
public class Filter<FIELD_TYPE extends java.lang.Object> implements java.io.Serializable {
    private FIELD_TYPE equals;
    private java.lang.Boolean specified;
    private java.util.List<? extends FIELD_TYPE> in;
    private static final long serialVersionUID = 1L;
    public static final com.example.demo.filter.Filter.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    protected final java.lang.String getFilterName() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final FIELD_TYPE getEquals() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.demo.filter.Filter<FIELD_TYPE> setEquals(FIELD_TYPE equals) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getSpecified() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.demo.filter.Filter<FIELD_TYPE> setSpecified(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean specified) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public java.util.List<FIELD_TYPE> getIn() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.example.demo.filter.Filter<FIELD_TYPE> setIn(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends FIELD_TYPE> in) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object o) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public Filter() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/example/demo/filter/Filter$Companion;", "", "()V", "serialVersionUID", "", "demo"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}