package com.example.demo.filter;

import java.lang.System;

/**
 * * Filter class for [Long] type attributes.
 * *
 * * @see RangeFilter
 */
@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0003\u0018\u0000 \u00042\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0004B\u0005\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0005"}, d2 = {"Lcom/example/demo/filter/LongFilter;", "Lcom/example/demo/filter/RangeFilter;", "", "()V", "Companion", "demo"})
public final class LongFilter extends com.example.demo.filter.RangeFilter<java.lang.Long> {
    private static final long serialVersionUID = 1L;
    public static final com.example.demo.filter.LongFilter.Companion Companion = null;
    
    public LongFilter() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/example/demo/filter/LongFilter$Companion;", "", "()V", "serialVersionUID", "", "demo"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}