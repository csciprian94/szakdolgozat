package com.example.demo.filter;

import java.lang.System;

/**
 * * Class for filtering attributes with [String] type.
 * * It can be added to a criteria class as a member, to support the following query parameters:
 * * `
 * * fieldName.equals='something'
 * * fieldName.specified=true
 * * fieldName.specified=false
 * * fieldName.in='something','other'
 * * fieldName.contains='thing'
 * ` *
 */
@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u0000 \u000e2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000eB\u0005\u00a2\u0006\u0002\u0010\u0003J\u0013\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0096\u0002J\b\u0010\t\u001a\u0004\u0018\u00010\u0002J\b\u0010\n\u001a\u00020\u000bH\u0016J\u000e\u0010\f\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0002J\b\u0010\r\u001a\u00020\u0002H\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0002X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"}, d2 = {"Lcom/example/demo/filter/StringFilter;", "Lcom/example/demo/filter/Filter;", "", "()V", "contains", "equals", "", "o", "", "getContains", "hashCode", "", "setContains", "toString", "Companion", "demo"})
public final class StringFilter extends com.example.demo.filter.Filter<java.lang.String> {
    private java.lang.String contains;
    private static final long serialVersionUID = 1L;
    public static final com.example.demo.filter.StringFilter.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getContains() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.demo.filter.StringFilter setContains(@org.jetbrains.annotations.NotNull()
    java.lang.String contains) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object o) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public StringFilter() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/example/demo/filter/StringFilter$Companion;", "", "()V", "serialVersionUID", "", "demo"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}