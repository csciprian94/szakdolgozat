package com.example.demo.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0002\bg\u0018\u0000 \u00052\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\b\u0012\u0004\u0012\u00020\u00020\u0004:\u0001\u0005\u00a8\u0006\u0006"}, d2 = {"Lcom/example/demo/repository/DolgozoRepository;", "Lorg/springframework/data/repository/PagingAndSortingRepository;", "Lcom/example/demo/domain/Dolgozo;", "", "Lorg/springframework/data/jpa/repository/JpaSpecificationExecutor;", "Companion", "demo"})
@org.springframework.data.rest.core.annotation.RepositoryRestResource(collectionResourceRel = "dolgozo", path = "dolgozo")
public abstract interface DolgozoRepository extends org.springframework.data.repository.PagingAndSortingRepository<com.example.demo.domain.Dolgozo, java.lang.Long>, org.springframework.data.jpa.repository.JpaSpecificationExecutor<com.example.demo.domain.Dolgozo> {
    public static final com.example.demo.repository.DolgozoRepository.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ENTITY_NAME = "dolgozo";
    
    @kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/example/demo/repository/DolgozoRepository$Companion;", "", "()V", "ENTITY_NAME", "", "demo"})
    public static final class Companion {
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ENTITY_NAME = "dolgozo";
        
        private Companion() {
            super();
        }
    }
}