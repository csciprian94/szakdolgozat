package com.example.demo.domain;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b4\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0087\b\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002Bs\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\f\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u0012\u00a2\u0006\u0002\u0010\u0014J\u000b\u00109\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000b\u0010:\u001a\u0004\u0018\u00010\u0012H\u00c6\u0003J\u000b\u0010;\u001a\u0004\u0018\u00010\u0012H\u00c6\u0003J\u000b\u0010<\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000b\u0010=\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010>\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000b\u0010?\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u0010\u0010@\u001a\u0004\u0018\u00010\fH\u00c6\u0003\u00a2\u0006\u0002\u0010#J\u000b\u0010A\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000b\u0010B\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u0010\u0010C\u001a\u0004\u0018\u00010\u0010H\u00c6\u0003\u00a2\u0006\u0002\u0010\u001eJ\u0092\u0001\u0010D\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00102\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00122\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0012H\u00c6\u0001\u00a2\u0006\u0002\u0010EJ\u0013\u0010F\u001a\u00020G2\b\u0010H\u001a\u0004\u0018\u00010IH\u00d6\u0003J\t\u0010J\u001a\u00020\fH\u00d6\u0001J\t\u0010K\u001a\u00020\u0004H\u00d6\u0001R \u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R \u0010\r\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\"\u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010!\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\"\u0010\u000b\u001a\u0004\u0018\u00010\f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010&\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R \u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\'\u0010\u001a\"\u0004\b(\u0010\u001cR \u0010\n\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010\u001a\"\u0004\b*\u0010\u001cR \u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010,\"\u0004\b-\u0010.R \u0010\u0011\u001a\u0004\u0018\u00010\u00128\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u0010,\"\u0004\b0\u0010.R \u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\u001a\"\u0004\b2\u0010\u001cR \u0010\b\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u00104\"\u0004\b5\u00106R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010\u001a\"\u0004\b8\u0010\u001c\u00a8\u0006L"}, d2 = {"Lcom/example/demo/domain/Dolgozo;", "Lcom/example/demo/domain/AbstractDomain;", "()V", "vezetekNev", "", "keresztkNev", "beosztas", "Lcom/example/demo/domain/Beosztas;", "telephely", "Lcom/example/demo/domain/Telephely;", "lakhely", "iranyitoSzam", "", "cim", "telefonSzam", "fizetes", "", "szulIdo", "Ljava/time/LocalDate;", "munkaViszonyKezdete", "(Ljava/lang/String;Ljava/lang/String;Lcom/example/demo/domain/Beosztas;Lcom/example/demo/domain/Telephely;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/time/LocalDate;Ljava/time/LocalDate;)V", "getBeosztas", "()Lcom/example/demo/domain/Beosztas;", "setBeosztas", "(Lcom/example/demo/domain/Beosztas;)V", "getCim", "()Ljava/lang/String;", "setCim", "(Ljava/lang/String;)V", "getFizetes", "()Ljava/lang/Long;", "setFizetes", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "getIranyitoSzam", "()Ljava/lang/Integer;", "setIranyitoSzam", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getKeresztkNev", "setKeresztkNev", "getLakhely", "setLakhely", "getMunkaViszonyKezdete", "()Ljava/time/LocalDate;", "setMunkaViszonyKezdete", "(Ljava/time/LocalDate;)V", "getSzulIdo", "setSzulIdo", "getTelefonSzam", "setTelefonSzam", "getTelephely", "()Lcom/example/demo/domain/Telephely;", "setTelephely", "(Lcom/example/demo/domain/Telephely;)V", "getVezetekNev", "setVezetekNev", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Ljava/lang/String;Lcom/example/demo/domain/Beosztas;Lcom/example/demo/domain/Telephely;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/time/LocalDate;Ljava/time/LocalDate;)Lcom/example/demo/domain/Dolgozo;", "equals", "", "other", "", "hashCode", "toString", "demo"})
@javax.persistence.Table(name = "dolgozo")
@javax.persistence.Entity()
public final class Dolgozo extends com.example.demo.domain.AbstractDomain {
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "vezeteknev", length = 50)
    private java.lang.String vezetekNev;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "keresztnev", length = 50)
    private java.lang.String keresztkNev;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @javax.persistence.JoinColumn(name = "beosztas_id")
    private com.example.demo.domain.Beosztas beosztas;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @javax.persistence.JoinColumn(name = "telephely_id")
    private com.example.demo.domain.Telephely telephely;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "lakhely", length = 100)
    private java.lang.String lakhely;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "iranyitoszam")
    private java.lang.Integer iranyitoSzam;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "cim", length = 200)
    private java.lang.String cim;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "telefonszam", length = 50)
    private java.lang.String telefonSzam;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "fizetes")
    private java.lang.Long fizetes;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "szul_ido")
    private java.time.LocalDate szulIdo;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "munkaviszony_kezdete")
    private java.time.LocalDate munkaViszonyKezdete;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getVezetekNev() {
        return null;
    }
    
    public final void setVezetekNev(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getKeresztkNev() {
        return null;
    }
    
    public final void setKeresztkNev(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.domain.Beosztas getBeosztas() {
        return null;
    }
    
    public final void setBeosztas(@org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Beosztas p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.domain.Telephely getTelephely() {
        return null;
    }
    
    public final void setTelephely(@org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Telephely p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLakhely() {
        return null;
    }
    
    public final void setLakhely(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getIranyitoSzam() {
        return null;
    }
    
    public final void setIranyitoSzam(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCim() {
        return null;
    }
    
    public final void setCim(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTelefonSzam() {
        return null;
    }
    
    public final void setTelefonSzam(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getFizetes() {
        return null;
    }
    
    public final void setFizetes(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.LocalDate getSzulIdo() {
        return null;
    }
    
    public final void setSzulIdo(@org.jetbrains.annotations.Nullable()
    java.time.LocalDate p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.LocalDate getMunkaViszonyKezdete() {
        return null;
    }
    
    public final void setMunkaViszonyKezdete(@org.jetbrains.annotations.Nullable()
    java.time.LocalDate p0) {
    }
    
    public Dolgozo(@org.jetbrains.annotations.Nullable()
    java.lang.String vezetekNev, @org.jetbrains.annotations.Nullable()
    java.lang.String keresztkNev, @org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Beosztas beosztas, @org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Telephely telephely, @org.jetbrains.annotations.Nullable()
    java.lang.String lakhely, @org.jetbrains.annotations.Nullable()
    java.lang.Integer iranyitoSzam, @org.jetbrains.annotations.Nullable()
    java.lang.String cim, @org.jetbrains.annotations.Nullable()
    java.lang.String telefonSzam, @org.jetbrains.annotations.Nullable()
    java.lang.Long fizetes, @org.jetbrains.annotations.Nullable()
    java.time.LocalDate szulIdo, @org.jetbrains.annotations.Nullable()
    java.time.LocalDate munkaViszonyKezdete) {
        super(null, null, null, null);
    }
    
    public Dolgozo() {
        super(null, null, null, null);
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.domain.Beosztas component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.domain.Telephely component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long component9() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.LocalDate component10() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.LocalDate component11() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.demo.domain.Dolgozo copy(@org.jetbrains.annotations.Nullable()
    java.lang.String vezetekNev, @org.jetbrains.annotations.Nullable()
    java.lang.String keresztkNev, @org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Beosztas beosztas, @org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Telephely telephely, @org.jetbrains.annotations.Nullable()
    java.lang.String lakhely, @org.jetbrains.annotations.Nullable()
    java.lang.Integer iranyitoSzam, @org.jetbrains.annotations.Nullable()
    java.lang.String cim, @org.jetbrains.annotations.Nullable()
    java.lang.String telefonSzam, @org.jetbrains.annotations.Nullable()
    java.lang.Long fizetes, @org.jetbrains.annotations.Nullable()
    java.time.LocalDate szulIdo, @org.jetbrains.annotations.Nullable()
    java.time.LocalDate munkaViszonyKezdete) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}