package com.example.demo.domain;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0014\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0017\u0018\u00002\u00020\u0001B5\u0012\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\tJ\r\u0010\u001b\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u0016J\b\u0010\u001c\u001a\u00020\u001dH\u0007J\u0015\u0010\u001e\u001a\u00020\u001d2\b\u0010\u001f\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u0018R \u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR \u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000b\"\u0004\b\u000f\u0010\rR\"\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0014\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\"\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0019\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001a\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0019\u00a8\u0006 "}, d2 = {"Lcom/example/demo/domain/AbstractDomain;", "", "id", "", "created", "Ljava/time/LocalDateTime;", "edited", "vers", "", "(Ljava/lang/Long;Ljava/time/LocalDateTime;Ljava/time/LocalDateTime;Ljava/lang/Integer;)V", "getCreated", "()Ljava/time/LocalDateTime;", "setCreated", "(Ljava/time/LocalDateTime;)V", "getEdited", "setEdited", "getId", "()Ljava/lang/Long;", "setId", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "getVers", "()Ljava/lang/Integer;", "setVers", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "version", "getVersion", "prePersist", "", "setVersion", "ver", "demo"})
@javax.persistence.MappedSuperclass()
public class AbstractDomain {
    @javax.persistence.Transient()
    private java.lang.Integer version;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.GeneratedValue(generator = "generator")
    @javax.persistence.Column(name = "id", unique = true, nullable = false, updatable = false)
    @javax.persistence.Id()
    private java.lang.Long id;
    @org.jetbrains.annotations.Nullable()
    @org.hibernate.annotations.CreationTimestamp()
    @javax.persistence.Column(name = "created", updatable = false)
    private java.time.LocalDateTime created;
    @org.jetbrains.annotations.Nullable()
    @org.springframework.data.annotation.LastModifiedDate()
    @org.hibernate.annotations.CreationTimestamp()
    @javax.persistence.Column(name = "edited", updatable = false)
    private java.time.LocalDateTime edited;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "version")
    @com.fasterxml.jackson.annotation.JsonIgnore()
    @javax.persistence.Version()
    private java.lang.Integer vers;
    
    @javax.persistence.PrePersist()
    public final void prePersist() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getVersion() {
        return null;
    }
    
    public final void setVersion(@org.jetbrains.annotations.Nullable()
    java.lang.Integer ver) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getId() {
        return null;
    }
    
    public final void setId(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.LocalDateTime getCreated() {
        return null;
    }
    
    public final void setCreated(@org.jetbrains.annotations.Nullable()
    java.time.LocalDateTime p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.LocalDateTime getEdited() {
        return null;
    }
    
    public final void setEdited(@org.jetbrains.annotations.Nullable()
    java.time.LocalDateTime p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getVers() {
        return null;
    }
    
    public final void setVers(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    public AbstractDomain(@org.jetbrains.annotations.Nullable()
    @com.fasterxml.jackson.annotation.JsonProperty(value = "id")
    java.lang.Long id, @org.jetbrains.annotations.Nullable()
    @com.fasterxml.jackson.annotation.JsonProperty(value = "created")
    java.time.LocalDateTime created, @org.jetbrains.annotations.Nullable()
    @com.fasterxml.jackson.annotation.JsonProperty(value = "edited")
    java.time.LocalDateTime edited, @org.jetbrains.annotations.Nullable()
    java.lang.Integer vers) {
        super();
    }
    
    public AbstractDomain() {
        super();
    }
}