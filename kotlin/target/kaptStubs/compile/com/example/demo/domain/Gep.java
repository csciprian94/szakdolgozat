package com.example.demo.domain;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b)\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0087\b\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002BU\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\f\u0012\b\u0010\r\u001a\u0004\u0018\u00010\f\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\f\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010\u00a2\u0006\u0002\u0010\u0011J\u000b\u0010/\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000b\u00100\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000b\u00101\u001a\u0004\u0018\u00010\bH\u00c6\u0003J\u000b\u00102\u001a\u0004\u0018\u00010\nH\u00c6\u0003J\u0010\u00103\u001a\u0004\u0018\u00010\fH\u00c6\u0003\u00a2\u0006\u0002\u0010\u001bJ\u0010\u00104\u001a\u0004\u0018\u00010\fH\u00c6\u0003\u00a2\u0006\u0002\u0010\u001bJ\u0010\u00105\u001a\u0004\u0018\u00010\fH\u00c6\u0003\u00a2\u0006\u0002\u0010\u001bJ\u000b\u00106\u001a\u0004\u0018\u00010\u0010H\u00c6\u0003Jn\u00107\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u00c6\u0001\u00a2\u0006\u0002\u00108J\u0013\u00109\u001a\u00020:2\b\u0010;\u001a\u0004\u0018\u00010<H\u00d6\u0003J\t\u0010=\u001a\u00020\fH\u00d6\u0001J\t\u0010>\u001a\u00020\u0004H\u00d6\u0001R \u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R \u0010\u0007\u001a\u0004\u0018\u00010\b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\"\u0010\r\u001a\u0004\u0018\u00010\f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001e\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR \u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&R\"\u0010\u000b\u001a\u0004\u0018\u00010\f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001e\u001a\u0004\b\'\u0010\u001b\"\u0004\b(\u0010\u001dR \u0010\t\u001a\u0004\u0018\u00010\n8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010*\"\u0004\b+\u0010,R\"\u0010\u000e\u001a\u0004\u0018\u00010\f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001e\u001a\u0004\b-\u0010\u001b\"\u0004\b.\u0010\u001d\u00a8\u0006?"}, d2 = {"Lcom/example/demo/domain/Gep;", "Lcom/example/demo/domain/AbstractDomain;", "()V", "nev", "", "marka", "Lcom/example/demo/domain/Marka;", "gepTipus", "Lcom/example/demo/domain/GepTipus;", "telephely", "Lcom/example/demo/domain/Telephely;", "suly", "", "gyartasEve", "uzemIdo", "cegTulajdona", "Ljava/time/LocalDate;", "(Ljava/lang/String;Lcom/example/demo/domain/Marka;Lcom/example/demo/domain/GepTipus;Lcom/example/demo/domain/Telephely;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/time/LocalDate;)V", "getCegTulajdona", "()Ljava/time/LocalDate;", "setCegTulajdona", "(Ljava/time/LocalDate;)V", "getGepTipus", "()Lcom/example/demo/domain/GepTipus;", "setGepTipus", "(Lcom/example/demo/domain/GepTipus;)V", "getGyartasEve", "()Ljava/lang/Integer;", "setGyartasEve", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getMarka", "()Lcom/example/demo/domain/Marka;", "setMarka", "(Lcom/example/demo/domain/Marka;)V", "getNev", "()Ljava/lang/String;", "setNev", "(Ljava/lang/String;)V", "getSuly", "setSuly", "getTelephely", "()Lcom/example/demo/domain/Telephely;", "setTelephely", "(Lcom/example/demo/domain/Telephely;)V", "getUzemIdo", "setUzemIdo", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "(Ljava/lang/String;Lcom/example/demo/domain/Marka;Lcom/example/demo/domain/GepTipus;Lcom/example/demo/domain/Telephely;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/time/LocalDate;)Lcom/example/demo/domain/Gep;", "equals", "", "other", "", "hashCode", "toString", "demo"})
@javax.persistence.Table(name = "gep")
@javax.persistence.Entity()
public final class Gep extends com.example.demo.domain.AbstractDomain {
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "nev", length = 200)
    private java.lang.String nev;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @javax.persistence.JoinColumn(name = "marka_id")
    private com.example.demo.domain.Marka marka;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @javax.persistence.JoinColumn(name = "geptipus_id")
    private com.example.demo.domain.GepTipus gepTipus;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @javax.persistence.JoinColumn(name = "telephely_id")
    private com.example.demo.domain.Telephely telephely;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "suly")
    private java.lang.Integer suly;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "gyartas_eve")
    private java.lang.Integer gyartasEve;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "uzemido")
    private java.lang.Integer uzemIdo;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "ceg_tulajdona")
    private java.time.LocalDate cegTulajdona;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getNev() {
        return null;
    }
    
    public final void setNev(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.domain.Marka getMarka() {
        return null;
    }
    
    public final void setMarka(@org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Marka p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.domain.GepTipus getGepTipus() {
        return null;
    }
    
    public final void setGepTipus(@org.jetbrains.annotations.Nullable()
    com.example.demo.domain.GepTipus p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.domain.Telephely getTelephely() {
        return null;
    }
    
    public final void setTelephely(@org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Telephely p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getSuly() {
        return null;
    }
    
    public final void setSuly(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getGyartasEve() {
        return null;
    }
    
    public final void setGyartasEve(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getUzemIdo() {
        return null;
    }
    
    public final void setUzemIdo(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.LocalDate getCegTulajdona() {
        return null;
    }
    
    public final void setCegTulajdona(@org.jetbrains.annotations.Nullable()
    java.time.LocalDate p0) {
    }
    
    public Gep(@org.jetbrains.annotations.Nullable()
    java.lang.String nev, @org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Marka marka, @org.jetbrains.annotations.Nullable()
    com.example.demo.domain.GepTipus gepTipus, @org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Telephely telephely, @org.jetbrains.annotations.Nullable()
    java.lang.Integer suly, @org.jetbrains.annotations.Nullable()
    java.lang.Integer gyartasEve, @org.jetbrains.annotations.Nullable()
    java.lang.Integer uzemIdo, @org.jetbrains.annotations.Nullable()
    java.time.LocalDate cegTulajdona) {
        super(null, null, null, null);
    }
    
    public Gep() {
        super(null, null, null, null);
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.domain.Marka component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.domain.GepTipus component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.domain.Telephely component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.LocalDate component8() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.demo.domain.Gep copy(@org.jetbrains.annotations.Nullable()
    java.lang.String nev, @org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Marka marka, @org.jetbrains.annotations.Nullable()
    com.example.demo.domain.GepTipus gepTipus, @org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Telephely telephely, @org.jetbrains.annotations.Nullable()
    java.lang.Integer suly, @org.jetbrains.annotations.Nullable()
    java.lang.Integer gyartasEve, @org.jetbrains.annotations.Nullable()
    java.lang.Integer uzemIdo, @org.jetbrains.annotations.Nullable()
    java.time.LocalDate cegTulajdona) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}