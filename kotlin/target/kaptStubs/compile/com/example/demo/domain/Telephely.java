package com.example.demo.domain;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 11}, bv = {1, 0, 2}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b(\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0087\b\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002Bq\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0002\u0010\u0010J\u000b\u0010,\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000b\u0010-\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000b\u0010.\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u0010\u0010/\u001a\u0004\u0018\u00010\tH\u00c6\u0003\u00a2\u0006\u0002\u0010\u001aJ\u000b\u00100\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000b\u00101\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000b\u00102\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000b\u00103\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000b\u00104\u001a\u0004\u0018\u00010\u000fH\u00c6\u0003Jz\u00105\u001a\u00020\u00002\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00c6\u0001\u00a2\u0006\u0002\u00106J\u0013\u00107\u001a\u0002082\b\u00109\u001a\u0004\u0018\u00010:H\u00d6\u0003J\t\u0010;\u001a\u00020\tH\u00d6\u0001J\t\u0010<\u001a\u00020\u0004H\u00d6\u0001R \u0010\n\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R \u0010\f\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0012\"\u0004\b\u0016\u0010\u0014R \u0010\r\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0012\"\u0004\b\u0018\u0010\u0014R\"\u0010\b\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001d\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR \u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R \u0010\u000e\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u0012\"\u0004\b\'\u0010\u0014R \u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0012\"\u0004\b)\u0010\u0014R \u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u0012\"\u0004\b+\u0010\u0014\u00a8\u0006="}, d2 = {"Lcom/example/demo/domain/Telephely;", "Lcom/example/demo/domain/AbstractDomain;", "()V", "nev", "", "megye", "Lcom/example/demo/domain/Megye;", "telepules", "iranyitoSzam", "", "cim", "telefonSzam", "email", "fax", "mukodesKezdete", "Ljava/time/LocalDate;", "(Ljava/lang/String;Lcom/example/demo/domain/Megye;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/time/LocalDate;)V", "getCim", "()Ljava/lang/String;", "setCim", "(Ljava/lang/String;)V", "getEmail", "setEmail", "getFax", "setFax", "getIranyitoSzam", "()Ljava/lang/Integer;", "setIranyitoSzam", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getMegye", "()Lcom/example/demo/domain/Megye;", "setMegye", "(Lcom/example/demo/domain/Megye;)V", "getMukodesKezdete", "()Ljava/time/LocalDate;", "setMukodesKezdete", "(Ljava/time/LocalDate;)V", "getNev", "setNev", "getTelefonSzam", "setTelefonSzam", "getTelepules", "setTelepules", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Lcom/example/demo/domain/Megye;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/time/LocalDate;)Lcom/example/demo/domain/Telephely;", "equals", "", "other", "", "hashCode", "toString", "demo"})
@javax.persistence.Table(name = "telephely")
@javax.persistence.Entity()
public final class Telephely extends com.example.demo.domain.AbstractDomain {
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "nev", length = 100)
    private java.lang.String nev;
    @org.jetbrains.annotations.Nullable()
    @org.hibernate.annotations.LazyToOne(value = org.hibernate.annotations.LazyToOneOption.NO_PROXY)
    @javax.persistence.ManyToOne(fetch = javax.persistence.FetchType.LAZY)
    @javax.persistence.JoinColumn(name = "megye_id")
    @org.springframework.data.rest.core.annotation.RestResource(exported = false)
    private com.example.demo.domain.Megye megye;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "telepules", length = 100)
    private java.lang.String telepules;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "iranyitoszam")
    private java.lang.Integer iranyitoSzam;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "cim", length = 200)
    private java.lang.String cim;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "telefonszam", length = 50)
    private java.lang.String telefonSzam;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "email", length = 100)
    private java.lang.String email;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "fax", length = 100)
    private java.lang.String fax;
    @org.jetbrains.annotations.Nullable()
    @javax.persistence.Column(name = "mukodes_kezdete")
    private java.time.LocalDate mukodesKezdete;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getNev() {
        return null;
    }
    
    public final void setNev(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.domain.Megye getMegye() {
        return null;
    }
    
    public final void setMegye(@org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Megye p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTelepules() {
        return null;
    }
    
    public final void setTelepules(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getIranyitoSzam() {
        return null;
    }
    
    public final void setIranyitoSzam(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCim() {
        return null;
    }
    
    public final void setCim(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTelefonSzam() {
        return null;
    }
    
    public final void setTelefonSzam(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEmail() {
        return null;
    }
    
    public final void setEmail(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFax() {
        return null;
    }
    
    public final void setFax(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.LocalDate getMukodesKezdete() {
        return null;
    }
    
    public final void setMukodesKezdete(@org.jetbrains.annotations.Nullable()
    java.time.LocalDate p0) {
    }
    
    public Telephely(@org.jetbrains.annotations.Nullable()
    java.lang.String nev, @org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Megye megye, @org.jetbrains.annotations.Nullable()
    java.lang.String telepules, @org.jetbrains.annotations.Nullable()
    java.lang.Integer iranyitoSzam, @org.jetbrains.annotations.Nullable()
    java.lang.String cim, @org.jetbrains.annotations.Nullable()
    java.lang.String telefonSzam, @org.jetbrains.annotations.Nullable()
    java.lang.String email, @org.jetbrains.annotations.Nullable()
    java.lang.String fax, @org.jetbrains.annotations.Nullable()
    java.time.LocalDate mukodesKezdete) {
        super(null, null, null, null);
    }
    
    public Telephely() {
        super(null, null, null, null);
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.demo.domain.Megye component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.LocalDate component9() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.demo.domain.Telephely copy(@org.jetbrains.annotations.Nullable()
    java.lang.String nev, @org.jetbrains.annotations.Nullable()
    com.example.demo.domain.Megye megye, @org.jetbrains.annotations.Nullable()
    java.lang.String telepules, @org.jetbrains.annotations.Nullable()
    java.lang.Integer iranyitoSzam, @org.jetbrains.annotations.Nullable()
    java.lang.String cim, @org.jetbrains.annotations.Nullable()
    java.lang.String telefonSzam, @org.jetbrains.annotations.Nullable()
    java.lang.String email, @org.jetbrains.annotations.Nullable()
    java.lang.String fax, @org.jetbrains.annotations.Nullable()
    java.time.LocalDate mukodesKezdete) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}