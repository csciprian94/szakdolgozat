package com.example.demo.filter

import java.time.LocalDate

/**
 * Filter class for [LocalDate] type attributes.
 *
 * @see RangeFilter
 */
class LocalDateFilter : RangeFilter<LocalDate>() {

    override fun setEquals(equals: LocalDate): LocalDateFilter {
        super.setEquals(equals)
        return this
    }

    override fun setGreaterThan(equals: LocalDate): LocalDateFilter {
        super.setGreaterThan(equals)
        return this
    }

    override fun setGreaterOrEqualThan(equals: LocalDate): LocalDateFilter {
        super.setGreaterOrEqualThan(equals)
        return this
    }

    override fun setLessThan(equals: LocalDate): LocalDateFilter {
        super.setLessThan(equals)
        return this
    }

    override fun setLessOrEqualThan(equals: LocalDate): LocalDateFilter {
        super.setLessOrEqualThan(equals)
        return this
    }

    override fun setIn(`in`: List<LocalDate>): LocalDateFilter {
        super.setIn(`in`)
        return this
    }

    companion object {

        private val serialVersionUID = 1L
    }

}
