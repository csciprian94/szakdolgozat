package com.example.demo.filter

import java.util.Objects

/**
 * Class for filtering attributes with [String] type.
 * It can be added to a criteria class as a member, to support the following query parameters:
 * `
 * fieldName.equals='something'
 * fieldName.specified=true
 * fieldName.specified=false
 * fieldName.in='something','other'
 * fieldName.contains='thing'
` *
 */
open class StringFilter : Filter<String>() {

    private var contains: String? = null

    open fun getContains(): String? {
        return contains
    }

    open fun setContains(contains: String): StringFilter {
        this.contains = contains
        return this
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) {
            return true
        }
        if (o == null || javaClass != o.javaClass) {
            return false
        }
        if (!super.equals(o)) {
            return false
        }
        val that = o as StringFilter?
        return contains == that!!.contains
    }

    override fun hashCode(): Int {
        return Objects.hash(super.hashCode(), contains)
    }

    override fun toString(): String {
        return (filterName + " ["
                + (if (getContains() != null) "contains=" + getContains() + ", " else "")
                + (if (getEquals() != null) "equals=" + getEquals() + ", " else "")
                + (if (getSpecified() != null) "specified=" + getSpecified()!! else "")
                + "]")
    }

    companion object {

        private val serialVersionUID = 1L
    }

}
