package com.example.demo.filter

import java.util.Objects

open class RangeFilter<FIELD_TYPE : Comparable<FIELD_TYPE>> : Filter<FIELD_TYPE>() {

    private var greaterThan: FIELD_TYPE? = null
    private var lessThan: FIELD_TYPE? = null
    private var greaterOrEqualThan: FIELD_TYPE? = null
    private var lessOrEqualThan: FIELD_TYPE? = null

    open fun getGreaterThan(): FIELD_TYPE? {
        return greaterThan
    }

    open fun setGreaterThan(greaterThan: FIELD_TYPE): RangeFilter<FIELD_TYPE> {
        this.greaterThan = greaterThan
        return this
    }

    open fun getGreaterOrEqualThan(): FIELD_TYPE? {
        return greaterOrEqualThan
    }

    open fun setGreaterOrEqualThan(greaterOrEqualThan: FIELD_TYPE): RangeFilter<FIELD_TYPE> {
        this.greaterOrEqualThan = greaterOrEqualThan
        return this
    }

    open fun getLessThan(): FIELD_TYPE? {
        return lessThan
    }

    open fun setLessThan(lessThan: FIELD_TYPE): RangeFilter<FIELD_TYPE> {
        this.lessThan = lessThan
        return this
    }

    open fun getLessOrEqualThan(): FIELD_TYPE? {
        return lessOrEqualThan
    }

    open fun setLessOrEqualThan(lessOrEqualThan: FIELD_TYPE): RangeFilter<FIELD_TYPE> {
        this.lessOrEqualThan = lessOrEqualThan
        return this
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) {
            return true
        }
        if (o == null || javaClass != o.javaClass) {
            return false
        }
        if (!super.equals(o)) {
            return false
        }
        val that = o as RangeFilter<*>?
        return greaterThan == that!!.greaterThan &&
                lessThan == that.lessThan &&
                greaterOrEqualThan == that.greaterOrEqualThan &&
                lessOrEqualThan == that.lessOrEqualThan
    }

    override fun hashCode(): Int {
        return Objects.hash(super.hashCode(), greaterThan, lessThan, greaterOrEqualThan, lessOrEqualThan)
    }

    override fun toString(): String {
        return (filterName + " ["
                + (if (getGreaterThan() != null) "greaterThan=" + getGreaterThan() + ", " else "")
                + (if (getGreaterOrEqualThan() != null) "greaterOrEqualThan=" + getGreaterOrEqualThan() + ", " else "")
                + (if (getLessThan() != null) "lessThan=" + getLessThan() + ", " else "")
                + (if (getLessOrEqualThan() != null) "lessOrEqualThan=" + getLessOrEqualThan() + ", " else "")
                + (if (getEquals() != null) "equals=" + getEquals() + ", " else "")
                + (if (getSpecified() != null) "specified=" + getSpecified() + ", " else "")
                + (if (getIn() != null) "in=" + getIn()!! else "")
                + "]")
    }

    companion object {

        private val serialVersionUID = 1L
    }

}
