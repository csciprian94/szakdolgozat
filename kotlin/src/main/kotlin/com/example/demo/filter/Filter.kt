package com.example.demo.filter

import java.io.Serializable
import java.util.Objects

/**
 * Base class for the various attribute filters. It can be added to a criteria class as a member, to support the
 * following query parameters:
 * <pre>
 * fieldName.equals='something'
 * fieldName.specified=true
 * fieldName.specified=false
 * fieldName.in='something','other'
</pre> *
 */
// 119 - generic typenames, 1948 FIELD_TYPE is not serializable
open class Filter<FIELD_TYPE> : Serializable {
    private var equals: FIELD_TYPE? = null
    private var specified: Boolean? = null
    private var `in`: List<FIELD_TYPE>? = null

    protected val filterName: String
        get() = this.javaClass.simpleName

    fun getEquals(): FIELD_TYPE? {
        return equals
    }

    open fun setEquals(equals: FIELD_TYPE): Filter<FIELD_TYPE> {
        this.equals = equals
        return this
    }

    fun getSpecified(): Boolean? {
        return specified
    }

    fun setSpecified(specified: Boolean?): Filter<FIELD_TYPE> {
        this.specified = specified
        return this
    }

    open fun getIn(): List<FIELD_TYPE>? {
        return `in`
    }

    open fun setIn(`in`: List<FIELD_TYPE>): Filter<FIELD_TYPE> {
        this.`in` = `in`
        return this
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) {
            return true
        }
        if (o == null || javaClass != o.javaClass) {
            return false
        }
        val filter = o as Filter<*>?
        return equals == filter!!.equals &&
                specified == filter.specified &&
                `in` == filter.`in`
    }

    override fun hashCode(): Int {
        return Objects.hash(equals, specified, `in`)
    }

    override fun toString(): String {
        return (filterName + " ["
                + (if (getEquals() != null) "equals=" + getEquals() + ", " else "")
                + (if (getIn() != null) "in=" + getIn() + ", " else "")
                + (if (getSpecified() != null) "specified=" + getSpecified()!! else "")
                + "]")
    }

    companion object {

        private const val serialVersionUID = 1L
    }
}
