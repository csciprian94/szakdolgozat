package com.example.demo.filter

/**
 * Class for filtering attributes with [Boolean] type. It can be added to a criteria class as a member, to support
 * the following query parameters:
 * <pre>
 * fieldName.equals=true
 * fieldName.specified=true
 * fieldName.specified=false
 * fieldName.in=true,false
</pre> *
 */
class BooleanFilter : Filter<Boolean>() {
    companion object {
        private val serialVersionUID = 1L
    }
}
