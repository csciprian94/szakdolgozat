package com.example.demo.filter

/**
 * Filter class for [Long] type attributes.
 *
 * @see RangeFilter
 */
class LongFilter : RangeFilter<Long>() {
    companion object {
        private val serialVersionUID = 1L
    }
}
