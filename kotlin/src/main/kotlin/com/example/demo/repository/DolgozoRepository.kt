package com.example.demo.repository

import com.example.demo.domain.Dolgozo
import com.example.demo.repository.DolgozoRepository.Companion.ENTITY_NAME
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = ENTITY_NAME, path = ENTITY_NAME)
interface DolgozoRepository : PagingAndSortingRepository<Dolgozo, Long>, JpaSpecificationExecutor<Dolgozo> {
    companion object {
        const val ENTITY_NAME = "dolgozo"
    }
}