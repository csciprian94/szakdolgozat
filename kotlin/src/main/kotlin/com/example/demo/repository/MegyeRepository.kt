package com.example.demo.repository

import com.example.demo.domain.Megye
import com.example.demo.repository.MegyeRepository.Companion.ENTITY_NAME
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = ENTITY_NAME, path = ENTITY_NAME)
interface MegyeRepository : PagingAndSortingRepository<Megye, Long>, JpaSpecificationExecutor<Megye> {
    companion object {
        const val ENTITY_NAME = "megye"
    }
}
