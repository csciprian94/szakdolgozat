package com.example.demo.repository

import com.example.demo.domain.Marka
import com.example.demo.repository.MarkaRepository.Companion.ENTITY_NAME
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = ENTITY_NAME, path = ENTITY_NAME)
interface MarkaRepository: PagingAndSortingRepository<Marka, Long>, JpaSpecificationExecutor<Marka> {
    companion object {
        const val ENTITY_NAME = "marka"
    }
}