package com.example.demo.repository

import com.example.demo.domain.Telephely
import com.example.demo.repository.TelephelyRepository.Companion.ENTITY_NAME
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = ENTITY_NAME, path = ENTITY_NAME)
interface TelephelyRepository : PagingAndSortingRepository<Telephely, Long>, JpaSpecificationExecutor<Telephely> {
    companion object {
        const val ENTITY_NAME = "telephely"
    }
}