package com.example.demo.repository

import com.example.demo.domain.GepTipus
import com.example.demo.repository.GepTipusRepository.Companion.ENTITY_NAME
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = ENTITY_NAME, path = ENTITY_NAME)
interface GepTipusRepository: PagingAndSortingRepository<GepTipus, Long>, JpaSpecificationExecutor<GepTipus> {
    companion object {
        const val ENTITY_NAME = "geptipus"
    }
}