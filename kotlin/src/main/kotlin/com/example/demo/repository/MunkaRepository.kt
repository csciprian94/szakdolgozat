package com.example.demo.repository

import com.example.demo.domain.Munka
import com.example.demo.repository.MunkaRepository.Companion.ENTITY_NAME
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = ENTITY_NAME, path = ENTITY_NAME)
interface MunkaRepository : PagingAndSortingRepository<Munka, Long>, JpaSpecificationExecutor<Munka> {
    companion object {
        const val ENTITY_NAME = "munka"
    }
}
