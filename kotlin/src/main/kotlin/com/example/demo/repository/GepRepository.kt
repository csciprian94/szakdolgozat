package com.example.demo.repository

import com.example.demo.domain.Gep
import com.example.demo.repository.GepRepository.Companion.ENTITY_NAME
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = ENTITY_NAME, path = ENTITY_NAME)
interface GepRepository : PagingAndSortingRepository<Gep, Long>, JpaSpecificationExecutor<Gep> {
    companion object {
        const val ENTITY_NAME = "gep"
    }
}
