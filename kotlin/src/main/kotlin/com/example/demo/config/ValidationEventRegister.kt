package com.example.demo.config

import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener
import org.springframework.validation.Validator

@Configuration
class ValidatorEventRegister @Autowired
constructor(private val validatingRepositoryEventListener: ValidatingRepositoryEventListener, private val validators: Map<String, Validator>) : InitializingBean {

    override fun afterPropertiesSet() {
        val events = listOf("beforeCreate")
        for ((key, value) in validators) {
            events.stream()
                    .filter { p -> key.startsWith(p) }
                    .findFirst()
                    .ifPresent { p ->
                        validatingRepositoryEventListener
                                .addValidator("beforeCreate", value)
                                .addValidator("beforeSave", value)

                    }
        }
    }
}
