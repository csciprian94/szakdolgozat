package com.example.demo.config

import org.springframework.context.annotation.Configuration
import org.springframework.format.FormatterRegistry
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.time.format.DateTimeFormatter

@Configuration
class DateTimeFormatterConfiguration : WebMvcConfigurer {

    override fun addFormatters(registry: FormatterRegistry) {
        val registrar = DateTimeFormatterRegistrar()

        val dateTimeFormatter = DateTimeFormatter.ISO_ZONED_DATE_TIME
        registrar.setDateFormatter(dateTimeFormatter)

        registrar.setUseIsoFormat(true)
        registrar.registerFormatters(registry)
    }
}
