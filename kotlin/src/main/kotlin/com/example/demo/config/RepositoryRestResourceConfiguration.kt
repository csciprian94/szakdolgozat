package com.example.demo.config

import com.example.demo.domain.*
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.projection.SpelAwareProxyProjectionFactory
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean


@Configuration
class RepositoryRestResourceConfiguration(private val validator: LocalValidatorFactoryBean) : RepositoryRestConfigurer {

    override fun configureRepositoryRestConfiguration(config: RepositoryRestConfiguration) {
        config.exposeIdsFor(Megye::class.java)
        config.exposeIdsFor(Telephely::class.java)
        config.exposeIdsFor(Beosztas::class.java)
        config.exposeIdsFor(Marka::class.java)
        config.exposeIdsFor(GepTipus::class.java)
        config.exposeIdsFor(Dolgozo::class.java)
        config.exposeIdsFor(Gep::class.java)
        config.exposeIdsFor(Munka::class.java)

    }

    @Bean
    fun projectionFactory(): SpelAwareProxyProjectionFactory {
        return SpelAwareProxyProjectionFactory()
    }

    override fun configureValidatingRepositoryEventListener(validatingListener: ValidatingRepositoryEventListener) {
        validatingListener.addValidator("beforeCreate", validator)
        validatingListener.addValidator("beforeSave", validator)
    }
}
