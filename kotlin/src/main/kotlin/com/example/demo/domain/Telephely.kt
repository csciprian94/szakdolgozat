package com.example.demo.domain


import org.hibernate.annotations.LazyToOne
import org.hibernate.annotations.LazyToOneOption
import org.springframework.data.rest.core.annotation.RestResource
import java.time.LocalDate
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "telephely")
data class Telephely(
        @NotNull
        @Column(name = "nev", length = 100)
        var nev: String?,

        @NotNull
        @RestResource(exported = false)
        @JoinColumn(name = "megye_id")
        @ManyToOne(fetch = FetchType.LAZY)
        @LazyToOne(LazyToOneOption.NO_PROXY)
        var megye: Megye? = null,

        @NotNull
        @Column(name = "telepules", length = 100)
        var telepules: String?,

        @Column(name = "iranyitoszam")
        var iranyitoSzam: Int?,

        @NotNull
        @Column(name = "cim", length = 200)
        var cim: String?,

        @NotNull
        @Column(name = "telefonszam", length = 50)
        var telefonSzam: String?,

        @Column(name = "email", length = 100)
        var email: String?,

        @Column(name = "fax", length = 100)
        var fax: String?,

        @Column(name = "mukodes_kezdete")
        var mukodesKezdete: LocalDate?) : AbstractDomain()
