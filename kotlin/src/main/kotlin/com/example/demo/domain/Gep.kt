package com.example.demo.domain

import com.example.demo.enum.GepStatuszEnum
import org.hibernate.annotations.LazyToOne
import org.hibernate.annotations.LazyToOneOption
import org.hibernate.annotations.LazyToOneOption.NO_PROXY
import org.springframework.data.rest.core.annotation.RestResource
import java.time.LocalDate
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "gep")
data class Gep(

        @NotNull
        @Column(name = "nev", length = 200)
        var nev: String,

        @NotNull
        @RestResource(exported = false)
        @JoinColumn(name = "marka_id")
        @ManyToOne(fetch = FetchType.LAZY)
        @LazyToOne(NO_PROXY)
        var marka: Marka,

        @NotNull
        @RestResource(exported = false)
        @JoinColumn(name = "geptipus_id")
        @ManyToOne(fetch = FetchType.LAZY)
        @LazyToOne(NO_PROXY)
        var gepTipus: GepTipus,

        @NotNull
        @RestResource(exported = false)
        @JoinColumn(name = "telephely_id")
        @ManyToOne(fetch = FetchType.LAZY)
        @LazyToOne(NO_PROXY)
        var telephely: Telephely,

        @RestResource(exported = false)
        @JoinColumn(name = "dolgozo_id")
        @ManyToOne(fetch = FetchType.LAZY)
        @LazyToOne(NO_PROXY)
        var dolgozo: Dolgozo?,

        @Column(name = "suly")
        var suly: Int?,

        @Column(name = "gyartas_eve")
        var gyartasEve: Int?,

        //órában mérjük
        @NotNull
        @Column(name = "uzemido")
        var uzemIdo: Int? = 0,

        @Column(name = "ceg_tulajdona")
        var cegTulajdona: LocalDate?,

        @NotNull
        @Enumerated(EnumType.STRING)
        @Column(name = "statusz", nullable = false, length = 20)
        var statusz: GepStatuszEnum) : AbstractDomain()
