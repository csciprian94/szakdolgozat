package com.example.demo.domain

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = "megye")
data class Megye(@Id
                 @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
                 @SequenceGenerator(name = "sequenceGenerator")
                 var id: Long? = null,
                 @Column(name = "megye_nev", length = 50)
                 var megyenev: String? = null)
