package com.example.demo.domain

import com.example.demo.enum.MunkaStatuszEnum
import org.hibernate.annotations.LazyToOne
import org.hibernate.annotations.LazyToOneOption
import org.springframework.data.rest.core.annotation.RestResource
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "munka")
data class Munka(

        @RestResource(exported = false)
        @JoinColumn(name = "gep_id")
        @ManyToOne(fetch = FetchType.LAZY)
        @LazyToOne(LazyToOneOption.NO_PROXY)
        var gep: Gep?,

        @RestResource(exported = false)
        @JoinColumn(name = "dolgozo_id")
        @ManyToOne(fetch = FetchType.LAZY)
        @LazyToOne(LazyToOneOption.NO_PROXY)
        var dolgozo: Dolgozo?,

        @Column(name = "munkaido", length = 100)
        var munkaido: Int? = 0,

        @NotNull
        @Enumerated(EnumType.STRING)
        @Column(name = "statusz", length = 20)
        var statusz: MunkaStatuszEnum,

        @NotNull
        @Column(name = "leiras")
        var leiras: String?) : AbstractDomain()
