package com.example.demo.domain


import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.annotations.CreationTimestamp
import org.springframework.data.annotation.LastModifiedDate
import java.time.LocalDateTime
import javax.persistence.*
import javax.persistence.metamodel.SingularAttribute

@MappedSuperclass
open class AbstractDomain(
        @Id
        @Column(name = "id", unique = true, nullable = false, updatable = false)
        @GeneratedValue(generator = "generator")
        @JsonProperty("id")
        var id: Long? = null,

        @Column(name = "created", updatable = false)
        @CreationTimestamp
        @JsonProperty("created")
        var created: LocalDateTime = LocalDateTime.now(),

        @Column(name = "edited", updatable = false)
        @CreationTimestamp
        @LastModifiedDate
        @JsonProperty("edited")
        var edited: LocalDateTime? = null,

        @Version
        @JsonIgnore
        @Column(name = "version")
        var vers: Int? = null) {

    @Transient
    private var version: Int? = null

    @PrePersist
    fun prePersist() {
        this.created = LocalDateTime.now()
        this.edited = this.created
    }

    fun getVersion(): Int? {
        return this.vers
    }

    fun setVersion(ver: Int?) {
        this.vers = ver
        this.version = ver
    }

}
