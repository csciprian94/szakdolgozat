package com.example.demo.domain

import org.hibernate.annotations.Formula
import org.hibernate.annotations.LazyToOne
import org.hibernate.annotations.LazyToOneOption
import org.springframework.data.rest.core.annotation.RestResource
import java.time.LocalDate
import javax.persistence.*
import javax.validation.constraints.NotNull


@Entity
@Table(name = "dolgozo")
data class Dolgozo(

        @NotNull
        @Column(name = "vezeteknev", length = 50)
        var vezetekNev: String?,

        @NotNull
        @Column(name = "keresztnev", length = 50)
        var keresztNev: String?,

        @Formula("( select CONCAT(vezeteknev, ' ', keresztnev) )")
        val dolgozoTeljesNeve: String?,

        @NotNull
        @RestResource(exported = false)
        @JoinColumn(name = "beosztas_id")
        @ManyToOne(fetch = FetchType.LAZY)
        @LazyToOne(LazyToOneOption.NO_PROXY)
        var beosztas: Beosztas?,

        @NotNull
        @RestResource(exported = false)
        @JoinColumn(name = "telephely_id")
        @ManyToOne(fetch = FetchType.LAZY)
        @LazyToOne(LazyToOneOption.NO_PROXY)
        var telephely: Telephely?,

        @NotNull
        @Column(name = "lakhely", length = 100)
        var lakhely: String?,

        @Column(name = "iranyitoszam")
        var iranyitoSzam: Int?,

        @NotNull
        @Column(name = "cim", length = 200)
        var cim: String?,

        @NotNull
        @Column(name = "telefonszam", length = 50)
        var telefonSzam: String?,

        @Column(name = "fizetes")
        var fizetes: Long?,

        @NotNull
        @Column(name = "szul_ido")
        var szulIdo: LocalDate?,

        @Column(name = "munkaviszony_kezdete")
        var munkaViszonyKezdete: LocalDate?,

        @NotNull
        @Column(name = "dolgozik")
        var dolgozik: Boolean) : AbstractDomain() {

}
