package com.example.demo.criteria

import com.example.demo.filter.LocalDateFilter
import com.example.demo.filter.LongFilter
import com.example.demo.filter.StringFilter

class TelephelyCriteria(
        var mukodesKezdeteV: LocalDateFilter?,
        var id: LongFilter?,
        var nev: StringFilter?,
        var telepules: StringFilter?,
        var megyeId: LongFilter?,
        var cim: StringFilter?,
        var mukodesKezdeteK: LocalDateFilter?) {

}
