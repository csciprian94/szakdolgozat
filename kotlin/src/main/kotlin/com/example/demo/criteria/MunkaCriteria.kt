package com.example.demo.criteria

import com.example.demo.filter.IntegerFilter
import com.example.demo.filter.LongFilter
import com.example.demo.filter.StringFilter

open class MunkaCriteria {
    var gepId: LongFilter? = null
    var dolgozoId: LongFilter? = null
    var munkaido: IntegerFilter? = null
    var statusz: StringFilter? = null
}
