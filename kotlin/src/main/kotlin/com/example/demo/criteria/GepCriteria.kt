package com.example.demo.criteria

import com.example.demo.filter.IntegerFilter
import com.example.demo.filter.LocalDateFilter
import com.example.demo.filter.LongFilter
import com.example.demo.filter.StringFilter


class GepCriteria {
    var id: LongFilter? = null
    var nev: StringFilter? = null
    var telephelyId: LongFilter? = null
    var geptipId: LongFilter? = null
    var markaId: LongFilter? = null
    var mukodesKezdeteK: LocalDateFilter? = null
    var mukodesKezdeteV: LocalDateFilter? = null
    var statusz: StringFilter? = null
    var uzemIdo: IntegerFilter? = null
}
