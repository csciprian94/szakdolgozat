package com.example.demo.criteria

import com.example.demo.filter.BooleanFilter
import com.example.demo.filter.LocalDateFilter
import com.example.demo.filter.LongFilter
import com.example.demo.filter.StringFilter



class DolgozoCriteria {

    var id: LongFilter? = null
    var vezetekNev: StringFilter? = null
    var keresztNev: StringFilter? = null
    var teljesNev: StringFilter? = null
    var lakhely: StringFilter? = null
    var beosztasId: LongFilter? = null
    var telephelyId: LongFilter? = null
    var munkaViszonyKezdeteK: LocalDateFilter? = null
    var munkaViszonyKezdeteV: LocalDateFilter? = null
    var dolgozik: BooleanFilter? = null
}
