package com.example.demo.service

import com.example.demo.domain.Dolgozo
import com.example.demo.domain.Gep
import com.example.demo.repository.DolgozoRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class DolgozoService(private val repository: DolgozoRepository) {

    fun dolgozniKezd(dolgozo: Dolgozo) {

        dolgozo.dolgozik = true

        repository.save(dolgozo!!)
    }

    fun munkaVege(dolgozo: Dolgozo) {
        dolgozo.dolgozik = false
        repository.save(dolgozo)
    }
}
