package com.example.demo.service

import com.example.demo.domain.Munka
import com.example.demo.enum.MunkaStatuszEnum
import com.example.demo.repository.MunkaRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.lang.Math.abs
import java.time.LocalDateTime

@Service
@Transactional
class MunkaService(private val repository: MunkaRepository, private val gepService: GepService) {
    
    fun munkaVege(munka: Munka) {
        munka.statusz = MunkaStatuszEnum.LEZARULT
        munka.munkaido = abs(LocalDateTime.now().hour - munka.created.getHour())

        gepService.munkaVege(munka)
        repository.save(munka)
    }
}
