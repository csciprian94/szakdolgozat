package com.example.demo.service

import com.example.demo.domain.Gep
import com.example.demo.domain.Munka
import com.example.demo.enum.GepStatuszEnum
import com.example.demo.repository.GepRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class GepService(private val repository: GepRepository, private val dolgozoService: DolgozoService) {

    /** ***/
    fun munkatKezd(gep: Gep) {
        gep.statusz = GepStatuszEnum.HASZNALATBAN
        if (gep != null) {
            dolgozoService.dolgozniKezd(gep.dolgozo!!)
        }
        repository.save(gep)
    }

    fun munkaVege(munka: Munka) {

        var gep = munka.gep

        if (gep != null) {
            gep.uzemIdo = gep.uzemIdo!! + munka.munkaido!!
            gep.statusz = GepStatuszEnum.VARAKOZIK
            dolgozoService.munkaVege(gep.dolgozo!!)
            gep.dolgozo = null
            repository.save(gep)
        }
    }
}
