package com.example.demo.service

import com.example.demo.criteria.MunkaCriteria
import com.example.demo.domain.Gep
import com.example.demo.domain.Munka
import com.example.demo.enum.MunkaStatuszEnum
import com.example.demo.repository.MunkaRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.function.Function


@Service
@Transactional(readOnly = true)
class MunkaQueryService(private val repository: MunkaRepository) : QueryService<Munka, MunkaCriteria>() {

    override fun findByCriteria(criteria: MunkaCriteria, page: Pageable): Page<Munka> {
        var specification: Specification<Munka>? = createSpecification(criteria)
        return repository.findAll(specification, page)
    }

    override fun createSpecification(criteria: MunkaCriteria): Specification<Munka> {
        var specification: Specification<Munka> = Specification { root, criteriaQuery, criteriaBuilder -> null }

        if (criteria.gepId != null) {
            specification = specification.and(buildSpecification(criteria.gepId!!,
                    (Function { root -> root.join<Munka, Gep>("gep").get("id") })))
        }
        if (criteria.munkaido != null) {
            specification = specification.and(buildSpecification(criteria.munkaido!!, Function { e -> e.get("munkaido") }))
        }
        if (criteria.statusz != null) {
            specification = specification.and(equalsSpecification(Function { e -> e.get("statusz") }, MunkaStatuszEnum.valueOf(criteria.statusz!!.getEquals()!!)))
        }
        return specification
    }
}
