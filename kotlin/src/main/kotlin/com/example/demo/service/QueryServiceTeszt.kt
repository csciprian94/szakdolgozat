package com.example.demo.service

import com.example.demo.filter.Filter
import com.example.demo.filter.RangeFilter
import com.example.demo.filter.StringFilter
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.transaction.annotation.Transactional
import java.util.function.Function
import javax.persistence.criteria.Expression
import javax.persistence.criteria.Join
import javax.persistence.criteria.Root
import javax.persistence.criteria.SetJoin
import javax.persistence.metamodel.SetAttribute
import javax.persistence.metamodel.SingularAttribute

/**
 * Base service for constructing and executing complex queries.
 *
 * @param <ENTITY> the type of the entity which is queried.
</ENTITY> */
// 119 - generic type names
@Transactional(readOnly = true)
abstract class QueryServiceTeszt<ENTITY, C> {

    abstract fun findByCriteria(criteria: C, page: Pageable): Page<ENTITY>

    protected abstract fun createSpecification(criteria: C): Specification<ENTITY>

    /**
     * Helper function to return a specification for filtering on a single field, where equality, and null/non-null
     * conditions are supported.
     *
     * @param filter the individual attribute filter coming from the frontend.
     * @param field  the JPA static metamodel representing the field.
     * @param <X>    The type of the attribute which is filtered.
     * @return a Specification
    </X> */
    protected fun <X> buildSpecification(filter: Filter<X>, field: SingularAttribute<in ENTITY, X>): Specification<ENTITY>? {
        return buildSpecification<X>(filter, Function{ root -> root.get(field) })
    }

    /**
     * Helper function to return a specification for filtering on a single field, where equality, and null/non-null
     * conditions are supported.
     *
     * @param filter            the individual attribute filter coming from the frontend.
     * @param metaclassFunction the function, which navigates from the current entity to a column, for which the filter applies.
     * @param <X>               The type of the attribute which is filtered.
     * @return a Specification
    </X> */
    protected fun <X> buildSpecification(filter: Filter<X>, metaclassFunction: Function<Root<ENTITY>, Expression<X>>): Specification<ENTITY>? {
        if (filter.getEquals() != null) {
            return equalsSpecification<X>(metaclassFunction, filter.getEquals() as X)
        } else if (filter.getIn() != null) {
            return valueIn(metaclassFunction, filter.getIn() as List<X>)
        } else if (filter.getSpecified() != null) {
            return byFieldSpecified(metaclassFunction, filter.getSpecified()!!)
        } else if (filter is StringFilter && (filter as StringFilter).getContains() != null) {
            return likeUpperSpecification(Function{ root -> metaclassFunction.apply(root) as Expression<String> }, (filter as StringFilter).getContains()!!)
        }
        return null
    }

    /**
     * Helper function to return a specification for filtering on a [String] field, where equality, containment,
     * and null/non-null conditions are supported.
     *
     * @param filter the individual attribute filter coming from the frontend.
     * @param field  the JPA static metamodel representing the field.
     * @return a Specification
     */
    protected fun buildStringSpecification(filter: StringFilter, field: SingularAttribute<in ENTITY, String>): Specification<ENTITY>? {
        return buildSpecification(filter, Function{ root -> root.get(field) })
    }

    /**
     * Helper function to return a specification for filtering on a [String] field, where equality, containment,
     * and null/non-null conditions are supported.
     *
     * @param filter            the individual attribute filter coming from the frontend.
     * @param metaclassFunction lambda, which based on a Root&lt;ENTITY&gt; returns Expression - basicaly picks a column
     * @return a Specification
     */
    protected fun buildSpecification(filter: StringFilter, metaclassFunction: Function<Root<ENTITY>, Expression<String>>): Specification<ENTITY>? {
        if (filter.getEquals() != null) {
            return equalsSpecification<String>(metaclassFunction, filter.getEquals() as String)
        } else if (filter.getIn() != null) {
            return valueIn(metaclassFunction, filter.getIn() as List<String>)
        } else if (filter.getContains() != null) {
            return likeUpperSpecification(metaclassFunction, filter.getContains() as String)
        } else if (filter.getSpecified() != null) {
            return byFieldSpecified(metaclassFunction, filter.getSpecified()!!)
        }
        return null
    }

    /**
     * Helper function to return a specification for filtering on a single [Comparable], where equality, less
     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     * supported.
     *
     * @param filter the individual attribute filter coming from the frontend.
     * @param field  the JPA static metamodel representing the field.
     * @param <X>    The type of the attribute which is filtered.
     * @return a Specification
    </X> */
    protected fun <X : Comparable<X>> buildRangeSpecification(filter: RangeFilter<X>,
                                                              field: SingularAttribute<in ENTITY, X>): Specification<ENTITY> {
        return buildSpecification<X>(filter, Function{ root -> root.get(field) })
    }

    /**
     * Helper function to return a specification for filtering on a single [Comparable], where equality, less
     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     * supported.
     *
     * @param filter            the individual attribute filter coming from the frontend.
     * @param metaclassFunction lambda, which based on a Root&lt;ENTITY&gt; returns Expression - basicaly picks a column
     * @param <X>               The type of the attribute which is filtered.
     * @return a Specification
    </X> */
    protected fun <X : Comparable<X>> buildSpecification(filter: RangeFilter<X>,
                                                         metaclassFunction: Function<Root<ENTITY>, Expression<X>>): Specification<ENTITY> {
        if (filter.getEquals() != null) {
            return equalsSpecification<X>(metaclassFunction, filter.getEquals() as X)
        } else if (filter.getIn() != null) {
            return valueIn(metaclassFunction, filter.getIn() as List<X>)
        }

        var result = Specification.where<ENTITY>(Specification { root, criteriaQuery, criteriaBuilder -> null })
        if (filter.getSpecified() != null) {
            result = result.and(byFieldSpecified(metaclassFunction, filter.getSpecified()!!))
        }
        if (filter.getGreaterThan() != null) {
            result = result.and(greaterThan<X>(metaclassFunction, filter.getGreaterThan() as X))
        }
        if (filter.getGreaterOrEqualThan() != null) {
            result = result.and(greaterThanOrEqualTo<X>(metaclassFunction, filter.getGreaterOrEqualThan() as X))
        }
        if (filter.getLessThan() != null) {
            result = result.and(lessThan<X>(metaclassFunction, filter.getLessThan() as X))
        }
        if (filter.getLessOrEqualThan() != null) {
            result = result.and(lessThanOrEqualTo<X>(metaclassFunction, filter.getLessOrEqualThan() as X))
        }
        return result
    }

    /**
     * Helper function to return a specification for filtering on one-to-one or many-to-one reference. Usage:
     * <pre>
     * Specification&lt;Employee&gt; specByProjectId = buildReferringEntitySpecification(criteria.getProjectId(),
     * Employee_.project, Project_.id);
     * Specification&lt;Employee&gt; specByProjectName = buildReferringEntitySpecification(criteria.getProjectName(),
     * Employee_.project, Project_.name);
    </pre> *
     *
     * @param filter     the filter object which contains a value, which needs to match or a flag if nullness is
     * checked.
     * @param reference  the attribute of the static metamodel for the referring entity.
     * @param valueField the attribute of the static metamodel of the referred entity, where the equality should be
     * checked.
     * @param <OTHER>    The type of the referenced entity.
     * @param <X>        The type of the attribute which is filtered.
     * @return a Specification
    </X></OTHER> */
    protected fun <OTHER, X> buildReferringEntitySpecification(filter: Filter<X>,
                                                               reference: SingularAttribute<in ENTITY, OTHER>,
                                                               valueField: SingularAttribute<in OTHER, X>): Specification<ENTITY>? {
        return buildSpecification<X>(filter, Function{ root -> root.get(reference).get(valueField) })
    }

    /**
     * Helper function to return a specification for filtering on one-to-many or many-to-many reference. Usage:
     * <pre>
     * Specification&lt;Employee&gt; specByEmployeeId = buildReferringEntitySpecification(criteria.getEmployeId(),
     * Project_.employees, Employee_.id);
     * Specification&lt;Employee&gt; specByEmployeeName = buildReferringEntitySpecification(criteria.getEmployeName(),
     * Project_.project, Project_.name);
    </pre> *
     *
     * @param filter     the filter object which contains a value, which needs to match or a flag if emptiness is
     * checked.
     * @param reference  the attribute of the static metamodel for the referring entity.
     * @param valueField the attribute of the static metamodel of the referred entity, where the equality should be
     * checked.
     * @param <OTHER>    The type of the referenced entity.
     * @param <X>        The type of the attribute which is filtered.
     * @return a Specification
    </X></OTHER> */
    protected fun <OTHER, X> buildReferringEntitySpecification(filter: Filter<X>,
                                                               reference: SetAttribute<ENTITY, OTHER>,
                                                               valueField: SingularAttribute<OTHER, X>): Specification<ENTITY>? {
        return buildReferringEntitySpecification<OTHER, ENTITY, X>(filter, Function{ root -> root.join(reference) }, Function{ entity -> entity.get(valueField) })
    }

    /**
     * Helper function to return a specification for filtering on one-to-many or many-to-many reference. Usage:
     * <pre>
     * Specification&lt;Employee&gt; specByEmployeeId = buildReferringEntitySpecification(
     * criteria.getEmployeId(),
     * root -&gt; root.get(Project_.company).join(Company_.employees),
     * entity -&gt; entity.get(Employee_.id));
     * Specification&lt;Employee&gt; specByProjectName = buildReferringEntitySpecification(
     * criteria.getProjectName(),
     * root -&gt; root.get(Project_.project)
     * entity -&gt; entity.get(Project_.name));
    </pre> *
     *
     * @param filter           the filter object which contains a value, which needs to match or a flag if emptiness is
     * checked.
     * @param functionToEntity the function, which joins he current entity to the entity set, on which the filtering is applied.
     * @param entityToColumn   the function, which of the static metamodel of the referred entity, where the equality should be
     * checked.
     * @param <OTHER>          The type of the referenced entity.
     * @param <X>              The type of the attribute which is filtered.
     * @return a Specification
    </X></OTHER> */
    protected//replace lambda with method reference -> see comment
    fun <OTHER, MISC, X> buildReferringEntitySpecificationSet(filter: Filter<X>,
                                                              functionToEntity: Function<Root<ENTITY>, SetJoin<MISC, OTHER>>,
                                                              entityToColumn: Function<SetJoin<MISC, OTHER>, Expression<X>>): Specification<ENTITY>? {
        if (filter.getEquals() != null) {
            return equalsSpecification<X>(functionToEntity.andThen(entityToColumn), filter.getEquals() as X)
        } else if (filter.getSpecified() != null) {
            // Interestingly, 'functionToEntity' doesn't work, we need the longer lambda formula
            return byFieldSpecified<OTHER>(Function{ root -> functionToEntity.apply(root) }, filter.getSpecified()!!)
        }
        return null
    }

    /* Fenti metódus SetJoin helyett Join-ra */
    protected//replace lambda with method reference -> see comment
    fun <OTHER, MISC, X> buildReferringEntitySpecification(filter: Filter<X>,
                                                           functionToEntity: Function<Root<ENTITY>, Join<MISC, OTHER>>,
                                                           entityToColumn: Function<Join<MISC, OTHER>, Expression<X>>): Specification<ENTITY>? {
        if (filter.getEquals() != null) {
            return equalsSpecification<X>(functionToEntity.andThen(entityToColumn), filter.getEquals() as X)
        } else if (filter.getSpecified() != null) {
            // Interestingly, 'functionToEntity' doesn't work, we need the longer lambda formula
            return byFieldSpecified<OTHER>(Function{ root -> functionToEntity.apply(root) }, filter.getSpecified()!!)
        } else if (filter is StringFilter && (filter as StringFilter).getContains() != null) {
            return likeUpperSpecification(Function{ root -> functionToEntity.andThen(entityToColumn).apply(root) as Expression<String> }, (filter as StringFilter).getContains() as String)
        }
        return null
    }

    /**
     * Helper function to return a specification for filtering on one-to-many or many-to-many reference. Where equality, less
     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     * supported. Usage:
     * <pre>
     * Specification&lt;Employee&gt; specByEmployeeId = buildReferringEntitySpecification(criteria.getEmployeId(),
     * Project_.employees, Employee_.id);
     * Specification&lt;Employee&gt; specByEmployeeName = buildReferringEntitySpecification(criteria.getEmployeName(),
     * Project_.project, Project_.name);
    </pre> *
     *
     * @param filter     the filter object which contains a value, which needs to match or a flag if emptiness is
     * checked.
     * @param reference  the attribute of the static metamodel for the referring entity.
     * @param valueField the attribute of the static metamodel of the referred entity, where the equality should be
     * checked.
     * @param <OTHER>    The type of the referenced entity.
     * @param <X>        The type of the attribute which is filtered.
     * @return a Specification
    </X></OTHER> */
    protected fun <OTHER, X : Comparable<X>> buildReferringEntitySpecification(filter: RangeFilter<X>,
                                                                               reference: SetAttribute<ENTITY, OTHER>,
                                                                               valueField: SingularAttribute<OTHER, X>): Specification<ENTITY> {
        return buildReferringEntitySpecification<OTHER, ENTITY, X>(filter, Function{ root -> root.join(reference) }, Function{ entity -> entity.get(valueField) })
    }

    /**
     * Helper function to return a specification for filtering on one-to-many or many-to-many reference. Where equality, less
     * than, greater than and less-than-or-equal-to and greater-than-or-equal-to and null/non-null conditions are
     * supported. Usage:
     * <pre>`
     * Specification<Employee> specByEmployeeId = buildReferringEntitySpecification(
     * criteria.getEmployeId(),
     * root -> root.get(Project_.company).join(Company_.employees),
     * entity -> entity.get(Employee_.id));
     * Specification<Employee> specByProjectName = buildReferringEntitySpecification(
     * criteria.getProjectName(),
     * root -> root.get(Project_.project)
     * entity -> entity.get(Project_.name));
    ` *
    </pre> *
     *
     * @param filter           the filter object which contains a value, which needs to match or a flag if emptiness is
     * checked.
     * @param functionToEntity the function, which joins he current entity to the entity set, on which the filtering is applied.
     * @param entityToColumn   the function, which of the static metamodel of the referred entity, where the equality should be
     * checked.
     * @param <OTHER>          The type of the referenced entity.
     * @param <MISC>           The type of the entity which is the last before the OTHER in the chain.
     * @param <X>              The type of the attribute which is filtered.
     * @return a Specification
    </X></MISC></OTHER> */
    protected//replace lambda with method reference -> see comment
    fun <OTHER, MISC, X : Comparable<X>> buildReferringEntitySpecificationSet(filter: RangeFilter<X>,
                                                                              functionToEntity: Function<Root<ENTITY>, SetJoin<MISC, OTHER>>,
                                                                              entityToColumn: Function<SetJoin<MISC, OTHER>, Expression<X>>): Specification<ENTITY> {
        val fused = functionToEntity.andThen(entityToColumn)
        if (filter.getEquals() != null) {
            return equalsSpecification<X>(fused, filter.getEquals() as X)
        } else if (filter.getIn() != null) {
            return valueIn(fused, filter.getIn() as List<X>)
        }
        var result = Specification.where<ENTITY>(Specification { root, criteriaQuery, criteriaBuilder -> null })
        if (filter.getSpecified() != null) {
            // Interestingly, 'functionToEntity' doesn't work, we need the longer lambda formula
            result = result.and(byFieldSpecified<OTHER>(Function{ root -> functionToEntity.apply(root) }, filter.getSpecified()!!))
        }
        if (filter.getGreaterThan() != null) {
            result = result.and(greaterThan<X>(fused, filter.getGreaterThan() as X))
        }
        if (filter.getGreaterOrEqualThan() != null) {
            result = result.and(greaterThanOrEqualTo<X>(fused, filter.getGreaterOrEqualThan() as X))
        }
        if (filter.getLessThan() != null) {
            result = result.and(lessThan<X>(fused, filter.getLessThan() as X))
        }
        if (filter.getLessOrEqualThan() != null) {
            result = result.and(lessThanOrEqualTo<X>(fused, filter.getLessOrEqualThan() as X))
        }
        return result
    }

    /* Fenti metódus SetJoin helyett Join-ra */
    protected//replace lambda with method reference -> see comment
    fun <OTHER, MISC, X : Comparable<X>> buildReferringEntitySpecification(filter: RangeFilter<X>,
                                                                           functionToEntity: Function<Root<ENTITY>, Join<MISC, OTHER>>,
                                                                           entityToColumn: Function<Join<MISC, OTHER>, Expression<X>>): Specification<ENTITY> {
        val fused = functionToEntity.andThen(entityToColumn)
        if (filter.getEquals() != null) {
            return equalsSpecification<X>(fused, filter.getEquals() as X)
        } else if (filter.getIn() != null) {
            return valueIn(fused, filter.getIn() as List<X>)
        }
        var result = Specification.where<ENTITY>(Specification { root, criteriaQuery, criteriaBuilder -> null })
        if (filter.getSpecified() != null) {
            // Interestingly, 'functionToEntity' doesn't work, we need the longer lambda formula
            result = result.and(byFieldSpecified<OTHER>(Function{ root -> functionToEntity.apply(root) }, filter.getSpecified()!!))
        }
        if (filter.getGreaterThan() != null) {
            result = result.and(greaterThan<X>(fused, filter.getGreaterThan() as X))
        }
        if (filter.getGreaterOrEqualThan() != null) {
            result = result.and(greaterThanOrEqualTo<X>(fused, filter.getGreaterOrEqualThan() as X))
        }
        if (filter.getLessThan() != null) {
            result = result.and(lessThan<X>(fused, filter.getLessThan() as X))
        }
        if (filter.getLessOrEqualThan() != null) {
            result = result.and(lessThanOrEqualTo<X>(fused, filter.getLessOrEqualThan() as X))
        }
        return result
    }

    /**
     * Generic method, which based on a Root&lt;ENTITY&gt; returns an Expression which type is the same as the given 'value' type.
     *
     * @param metaclassFunction function which returns the column which is used for filtering.
     * @param value             the actual value to filter for.
     * @param <X>               The type of the attribute which is filtered.
     * @return a Specification.
    </X> */
    protected fun <X> equalsSpecification(metaclassFunction: Function<Root<ENTITY>, Expression<X>>, value: X): Specification<ENTITY> {
        return Specification{ root, query, builder -> builder.equal(metaclassFunction.apply(root), value) }
    }

    protected fun <X> notEqualsSpecification(metaclassFunction: Function<Root<ENTITY>, Expression<X>>, value: X): Specification<ENTITY> {
        return Specification{ root, query, builder -> builder.notEqual(metaclassFunction.apply(root), value) }
    }

    protected fun likeUpperSpecification(metaclassFunction: Function<Root<ENTITY>, Expression<String>>,
                                         value: String): Specification<ENTITY> {
        // todo escape \ % _ ( https://stackoverflow.com/questions/673508/using-hibernate-criteria-is-there-a-way-to-escape-special-characters )
        return Specification{ root, query, builder -> builder.like(builder.upper(metaclassFunction.apply(root)), wrapLikeQuery(value)) }
    }

    protected fun <X> byFieldSpecified(metaclassFunction: Function<Root<ENTITY>, Expression<X>>,
                                       specified: Boolean): Specification<ENTITY> {
        return if (specified)
            Specification{ root, query, builder -> builder.isNotNull(metaclassFunction.apply(root)) }
        else
            Specification{ root, query, builder -> builder.isNull(metaclassFunction.apply(root)) }
    }

    protected fun <X> byFieldEmptiness(metaclassFunction: Function<Root<ENTITY>, Expression<Set<X>>>,
                                       specified: Boolean): Specification<ENTITY> {
        return if (specified)
            Specification{ root, query, builder -> builder.isNotEmpty(metaclassFunction.apply(root)) }
        else
            Specification{ root, query, builder -> builder.isEmpty(metaclassFunction.apply(root)) }
    }

    protected fun <X> valueIn(metaclassFunction: Function<Root<ENTITY>, Expression<X>>,
                              values: Collection<X>): Specification<ENTITY> {
        return Specification{ root, query, builder ->
            var `in` = builder.`in`(metaclassFunction.apply(root))
            for (value in values) {
                `in` = `in`.value(value)
            }
            `in`
        }
    }

    protected fun <X : Comparable<X>> greaterThanOrEqualTo(metaclassFunction: Function<Root<ENTITY>, Expression<X>>,
                                                           value: X): Specification<ENTITY> {
        return Specification{ root, query, builder -> builder.greaterThanOrEqualTo(metaclassFunction.apply(root), value) }
    }

    protected fun <X : Comparable<X>> greaterThan(metaclassFunction: Function<Root<ENTITY>, Expression<X>>,
                                                  value: X): Specification<ENTITY> {
        return Specification{ root, query, builder -> builder.greaterThan(metaclassFunction.apply(root), value) }
    }

    protected fun <X : Comparable<X>> lessThanOrEqualTo(metaclassFunction: Function<Root<ENTITY>, Expression<X>>,
                                                        value: X): Specification<ENTITY> {
        return Specification{ root, query, builder -> builder.lessThanOrEqualTo(metaclassFunction.apply(root), value) }
    }

    protected fun <X : Comparable<X>> lessThan(metaclassFunction: Function<Root<ENTITY>, Expression<X>>,
                                               value: X): Specification<ENTITY> {
        return Specification{ root, query, builder -> builder.lessThan(metaclassFunction.apply(root), value) }
    }

    protected fun wrapLikeQuery(txt: String): String {
        return "%" + txt.toUpperCase() + '%'.toString()
    }

}

