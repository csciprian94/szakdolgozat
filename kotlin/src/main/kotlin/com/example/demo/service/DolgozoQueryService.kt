package com.example.demo.service

import com.example.demo.criteria.DolgozoCriteria
import com.example.demo.domain.*
import com.example.demo.enum.GepStatuszEnum
import com.example.demo.repository.DolgozoRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.function.Function

@Service
@Transactional(readOnly = true)
class DolgozoQueryService(private val repository: DolgozoRepository) : QueryService<Dolgozo, DolgozoCriteria>() {

    override fun findByCriteria(criteria: DolgozoCriteria, page: Pageable): Page<Dolgozo> {
        var specification: Specification<Dolgozo>? = createSpecification(criteria)
        return repository.findAll(specification, page)
    }

    override fun createSpecification(criteria: DolgozoCriteria): Specification<Dolgozo> {
        var specification: Specification<Dolgozo> = Specification { root, criteriaQuery, criteriaBuilder -> null }

        if (criteria.teljesNev != null) {
            specification = specification.and(buildSpecification(criteria.teljesNev!!, Function { e -> e.get("vezetekNev") }))
        }
        if (criteria.beosztasId != null) {
            specification = specification.and(buildSpecification(criteria.beosztasId!!,
                    (Function { root -> root.join<Dolgozo, Beosztas>("beosztas").get("id") })))
        }
        if (criteria.telephelyId != null) {
            specification = specification.and(buildSpecification(criteria.telephelyId!!, Function{ root -> root.join<Dolgozo, Telephely>("telephely").get("id") }))
        }
        if (criteria.lakhely != null) {
            specification = specification.and(buildSpecification(criteria.lakhely!!, Function { e -> e.get("lakhely") }))
        }
        if (criteria.dolgozik != null) {
            specification = specification.and(buildSpecification(criteria.dolgozik!!, Function { e -> e.get("dolgozik") }))
        }
        return specification
    }
}
