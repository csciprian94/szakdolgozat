package com.example.demo.service

import com.example.demo.criteria.GepCriteria
import com.example.demo.domain.Gep
import com.example.demo.domain.GepTipus
import com.example.demo.domain.Marka
import com.example.demo.domain.Telephely
import com.example.demo.enum.GepStatuszEnum
import com.example.demo.repository.GepRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.function.Function

@Service
@Transactional(readOnly = true)
class GepQueryService(private val repository: GepRepository) : QueryService<Gep, GepCriteria>() {

    override fun findByCriteria(criteria: GepCriteria, page: Pageable): Page<Gep> {
        var specification = createSpecification(criteria)
        return repository.findAll(specification, page)
    }

    override fun createSpecification(criteria: GepCriteria): Specification<Gep> {
        var specification: Specification<Gep> = Specification { root, criteriaQuery, criteriaBuilder -> null }

        if (criteria.nev != null) {
            specification = specification.and(buildSpecification(criteria.nev!!, Function { e -> e.get("nev") }))
        }
        if (criteria.markaId != null) {
            specification = specification.and(buildSpecification(criteria.markaId!!,
                    (Function { root -> root.join<Gep, Marka>("marka").get("id") })))
        }
        if (criteria.telephelyId != null) {
            specification = specification.and(buildSpecification(criteria.telephelyId!!,
                    (Function { root -> root.join<Gep, Telephely>("telephely").get("id") })))
        }
        if (criteria.geptipId != null) {
            specification = specification.and(buildSpecification(criteria.geptipId!!,
                    (Function { root -> root.join<Gep, GepTipus>("gepTipus").get("id") })))
        }
        if (criteria.uzemIdo != null) {
            specification = specification.and(buildSpecification(criteria.uzemIdo!!, Function { e -> e.get("uzemIdo") }))
        }
        if (criteria.statusz != null) {
            specification = specification.and(equalsSpecification(Function { e -> e.get("statusz") }, GepStatuszEnum.valueOf(criteria.statusz!!.getEquals()!!)))
        }

        if (criteria.statusz != null && criteria.statusz!!.getIn() != null) {
            specification = specification.and(equalsSpecification(Function{ e -> e.get("statusz") }, GepStatuszEnum.valueOf(criteria.statusz!!.getIn()!!.get(0)))).
                    or(equalsSpecification(Function{ e -> e.get("statusz") }, GepStatuszEnum.valueOf(criteria.statusz!!.getIn()!!.get(1))))
        }

        return specification
    }
}
