package com.example.demo.service


import com.example.demo.criteria.TelephelyCriteria
import com.example.demo.domain.Megye
import com.example.demo.domain.Telephely
import com.example.demo.repository.TelephelyRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.function.Function

@Service
@Transactional(readOnly = true)
class TelephelyQueryService(private val repository: TelephelyRepository) : QueryService<Telephely, TelephelyCriteria>() {

    override fun findByCriteria(criteria: TelephelyCriteria, page: Pageable): Page<Telephely> {
        var specification = createSpecification(criteria)
        return repository.findAll(specification, page)
    }

    override fun createSpecification(criteria: TelephelyCriteria): Specification<Telephely> {
        var specification: Specification<Telephely> = Specification { root, criteriaQuery, criteriaBuilder -> null }

            if (criteria.nev != null) {
                specification = specification.and(buildSpecification(criteria.nev!!, Function{ e -> e.get("nev")}))
            }
            if (criteria.cim != null) {
                specification = specification.and(buildSpecification(criteria.cim!!, Function{ e -> e.get("cim")}))
            }
            if (criteria.telepules != null) {
                specification = specification.and(buildSpecification(criteria.telepules!!, Function{ e -> e.get("telepules")}))
            }
            if(criteria.megyeId != null) {
                specification = specification.and(buildSpecification(criteria.megyeId!!,
                        (Function{ root -> root.join<Telephely,Megye>("megye").get("id") }  )))

            }
            if(criteria.mukodesKezdeteK != null) {
                specification = specification.and(buildSpecification(criteria.mukodesKezdeteK!!, Function{ e -> e.get("mukodesKezdete")}))
            }
            if(criteria.mukodesKezdeteV != null) {
                specification = specification.and(buildSpecification(criteria.mukodesKezdeteV!!, Function{ e -> e.get("mukodesKezdete")}))
            }
        return specification
    }
}
