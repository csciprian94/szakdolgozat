package com.example.demo.web.rest

import com.example.demo.domain.Munka
import com.example.demo.repository.MunkaRepository
import com.example.demo.service.MunkaService
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/" + MunkaRepository.ENTITY_NAME)
class MunkaController(private val service: MunkaService) {

    @PutMapping("/munkavege")
    fun munkaVege(@RequestBody munka: Munka) {
        service.munkaVege(munka)
    }
}
