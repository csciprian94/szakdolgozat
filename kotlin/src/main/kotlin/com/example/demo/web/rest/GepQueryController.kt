package com.example.demo.web.rest

import com.example.demo.criteria.GepCriteria
import com.example.demo.domain.Gep
import com.example.demo.service.GepQueryService
import com.example.demo.repository.GepRepository
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.PagedResources
import org.springframework.hateoas.Resource
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/" + GepRepository.ENTITY_NAME)
class GepQueryController(val resourcesAssembler: PagedResourcesAssembler<Gep>, val queryService: GepQueryService) {

    @GetMapping("/query")
    fun query(criteria: GepCriteria, page: Pageable): PagedResources<Resource<Gep>> {
        val resources = resourcesAssembler.toResource(queryService.findByCriteria(criteria, page))
        resources.content.forEach { r -> r.add(linkTo(this.javaClass).slash(r.content.id).withSelfRel()) }
        return resources
    }
}
