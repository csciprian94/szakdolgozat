package com.example.demo.web.rest

import com.example.demo.criteria.TelephelyCriteria
import com.example.demo.domain.Telephely
import com.example.demo.repository.TelephelyRepository
import com.example.demo.service.TelephelyQueryService
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.PagedResources
import org.springframework.hateoas.Resource
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo

@RestController
@RequestMapping("/api/" + TelephelyRepository.ENTITY_NAME)
class TelephelyQueryController(val resourcesAssembler: PagedResourcesAssembler<Telephely>,val queryService: TelephelyQueryService) {

    @GetMapping("/query")
    fun query(criteria: TelephelyCriteria, page: Pageable): PagedResources<Resource<Telephely>> {
        val resources = resourcesAssembler.toResource(queryService.findByCriteria(criteria, page))
        resources.content.forEach { r -> r.add(linkTo(this.javaClass).slash(r.content.id).withSelfRel()) }
        return resources
    }
}
