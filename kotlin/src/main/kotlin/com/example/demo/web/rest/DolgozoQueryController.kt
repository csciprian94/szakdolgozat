package com.example.demo.web.rest

import com.example.demo.criteria.DolgozoCriteria
import com.example.demo.domain.Dolgozo
import com.example.demo.repository.DolgozoRepository
import com.example.demo.service.DolgozoQueryService
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.PagedResources
import org.springframework.hateoas.Resource
import org.springframework.hateoas.mvc.ControllerLinkBuilder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/" + DolgozoRepository.ENTITY_NAME)
class DolgozoQueryController(val resourcesAssembler: PagedResourcesAssembler<Dolgozo>, val queryService: DolgozoQueryService) {

    @GetMapping("/query")
    fun query(criteria: DolgozoCriteria, page: Pageable): PagedResources<Resource<Dolgozo>> {
        val resources = resourcesAssembler.toResource(queryService.findByCriteria(criteria, page))
        resources.content.forEach { r -> r.add(ControllerLinkBuilder.linkTo(this.javaClass).slash(r.content.id).withSelfRel()) }
        return resources
    }

}