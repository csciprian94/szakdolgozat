package com.example.demo.web.rest

import com.example.demo.criteria.MunkaCriteria
import com.example.demo.domain.Munka
import com.example.demo.repository.MunkaRepository
import com.example.demo.service.MunkaQueryService
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PagedResourcesAssembler
import org.springframework.hateoas.PagedResources
import org.springframework.hateoas.Resource
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo


@RestController
@RequestMapping("/api/" + MunkaRepository.ENTITY_NAME)
class MunkaQueryController(val resourcesAssembler: PagedResourcesAssembler<Munka>, val queryService: MunkaQueryService) {

    @GetMapping("/query")
    fun query(criteria: MunkaCriteria, page: Pageable): PagedResources<Resource<Munka>> {
        val resources = resourcesAssembler.toResource(queryService.findByCriteria(criteria, page))
        resources.content.forEach { r -> r.add(linkTo(this.javaClass).slash(r.content.id).withSelfRel()) }
        return resources
    }
}
