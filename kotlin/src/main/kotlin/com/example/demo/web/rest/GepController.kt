package com.example.demo.web.rest

import com.example.demo.domain.Gep
import com.example.demo.repository.GepRepository
import com.example.demo.service.GepService
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/" + GepRepository.ENTITY_NAME)
class GepController(private val service: GepService) {

    @PutMapping("munkakezd")
    fun munkatKezd(@RequestBody gep: Gep) {
        service.munkatKezd(gep)
    }
}
