package com.example.demo.enum

enum class GepStatuszEnum {
    UJ, //Még 0 üzemidővel rendelkezik
    HASZNALATBAN, // Használatban levő munkagép
    KARBANTARTAS, //Karbantartás alatt álló gép
    UZEMEN_KIVUL, //Üzemidővel rendelkező használaton kívüli gépek
    VARAKOZIK
}
