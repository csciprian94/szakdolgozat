package com.example.demo.enum

enum class MunkaStatuszEnum {
    FOLYAMATBAN,
    LEZARULT,
    MEGSZAKADT
}
