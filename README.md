# Szakdolgozat projekt

Mezőgazdasági gépnyilvántartó rendszer egy Java-s és egy Kotlin-os backend, valamint egy Angular frontend-del.

## Installálás

### Kotlin és Java projekt indítása 

- mvn clean
- mvn compile
- alkalmazás indítása

### Angular Frontend
- npm install 
- npm start


