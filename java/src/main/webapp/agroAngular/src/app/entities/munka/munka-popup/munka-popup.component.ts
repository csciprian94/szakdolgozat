import { Component, OnInit } from '@angular/core';
import { Gep } from '../../gep/gep.model';
import { GepService } from '../../gep/gep.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { DolgozoService } from '../../dolgozo/dolgozo.service';
import { PageResponse } from '../../../shared/model/page-response';
import { Dolgozo } from '../../dolgozo/dolgozo.model';
import { Router } from '@angular/router';
import { MunkaFormComponent } from '../munka-form/munka-form.component';

@Component({
    selector: 'app-munka-popup',
    templateUrl: './munka-popup.component.html',
})
export class MunkaPopupComponent implements OnInit {
    gep: Gep;
    dolgozoList: Dolgozo[];

    statuszList: { value: string, label: string }[] = [
        { value: 'UJ', label: 'Új' },
        { value: 'VARAKOZIK', label: 'Várakozik'},
        { value: 'HASZNALATBAN', label: 'Használatban' },
        { value: 'KARBANTARTAS', label: 'Karbantartás alatt' },
        { value: 'UZEMEN_KIVUL', label: 'Üzemen kívül' }
    ];

    constructor(
        private dolgozoService: DolgozoService,
        private gepService: GepService,
        private router: Router, public bsModalRef: BsModalRef,
        private toastrService: ToastrService,
        private modalService: BsModalService
    ) {
    }

    ngOnInit() {
        this.dolgozoService
            .query({
                page: 0,
                size: 100,
                filter: {
                    'telephelyId.equals': this.gep.telephely.id,
                    'dolgozik.equals': false,
                }
            })
            .subscribe(
                (res: PageResponse<any>) => {
                    this.dolgozoList = res._embedded ? (res._embedded[Object.keys(res._embedded)[0]] as any[]) : [];
                }
            );
    }

    moveToMunka() {
            this.bsModalRef.hide();
            this.modalService.show(MunkaFormComponent, { initialState: { gep: this.gep, readOnly: true } });
    }
}
