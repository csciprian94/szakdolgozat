import { Component, OnInit } from '@angular/core';
import { Gep } from '../../gep/gep.model';
import { GepService } from '../../gep/gep.service';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { PageResponse } from '../../../shared/model/page-response';
import { Munka } from '../munka.model';
import { MunkaService } from '../munka.service';

@Component({
    selector: 'app-munka-form',
    templateUrl: './munka-form.component.html',
    styleUrls: ['./munka-form.component.sass']
})
export class MunkaFormComponent implements OnInit {

    gep: Gep;
    readOnly = false;
    munka = new Munka();
    gepList: Gep[];

    munkaStatuszList = [
        { value: 'FOLYAMATBAN', label: 'Folyamatban' },
        { value: 'LEZARULT', label: 'Lezárult' },
        { value: 'MEGSZAKADT', label: 'Megszakadt' }
        ];

    constructor(
        private gepService: GepService,
        private munkaService: MunkaService,
        private router: Router, public bsModalRef: BsModalRef,
        private toastrService: ToastrService,
    ) {
    }

    ngOnInit() {

        if (this.gep) {
            this.munka.statusz = 'FOLYAMATBAN';
            this.munka.gep = this.gep;
            this.munka.dolgozo = this.gep.dolgozo;
        }
        if (!this.readOnly) {
            this.gepService
                .query({
                    page: 0,
                    size: 100,
                    filter: {
                        'statusz.equals': 'VARAKOZIK',
                    }
                })
                .subscribe(
                    (res: PageResponse<any>) => {
                        this.gepList = res._embedded ? (res._embedded[Object.keys(res._embedded)[0]] as any[]) : [];
                    }
                );
        }
    }

    save() {
        this.munkaService.save(this.munka).subscribe(() => {
            this.gepService.munkatKezd(this.munka.gep).subscribe();
            this.toastrService.success('Sikeres Munka mentés');
            this.router.navigate(['munka']);
            this.bsModalRef.hide();
        });
    }
}
