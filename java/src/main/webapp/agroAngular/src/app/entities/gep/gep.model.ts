import { Marka } from '../marka/marka.model';
import { GepTipus } from '../geptipus/geptipus.model';
import { Telephely } from '../telephely/telephely.model';
import { Dolgozo } from '../dolgozo/dolgozo.model';

export class Gep {
    constructor(
        public id?: number,
        public dolgozo?: Dolgozo,
        public nev?: string,
        public marka?: Marka,
        public gepTipus?: GepTipus,
        public telephely?: Telephely,
        public suly?: number,
        public gyartasEve?: number,
        public uzemIdo?: number,
        public cegTulajdona?: Date,
        public statusz?: string
    ) {}
}
