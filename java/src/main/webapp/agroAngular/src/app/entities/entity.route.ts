import { Routes } from '@angular/router';
import { TelephelyListComponent } from './telephely/telephely-list/telephely-list.component';
import { GepListComponent } from './gep/gep-list/gep-list.component';
import { DolgozoListComponent } from './dolgozo/dolgozo-list/dolgozo-list.component';
import { MunkaListComponent } from './munka/munka-list/munka-list.component';

export const entityRoute: Routes = [
    {
        path: 'telephely',
        component: TelephelyListComponent
    },
    {
        path: 'gep',
        component: GepListComponent
    },
    {
        path: 'dolgozo',
        component: DolgozoListComponent
    },
    {
        path: 'munka',
        component: MunkaListComponent,
    }
];
