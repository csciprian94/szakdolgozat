import { Telephely } from '../telephely/telephely.model';
import { Beosztas } from '../beosztas/beosztas.model';

export class Dolgozo {
    constructor(
        public id?: number,
        public vezetekNev?: string,
        public keresztNev?: string,
        public beosztas?: Beosztas,
        public telephely?: Telephely,
        public lakhely?: string,
        public iranyitoSzam?: number,
        public cim?: string,
        public telefonSzam?: string,
        public fizetes?: number,
        public szulIdo?: Date,
        public munkaViszonyKezdete?: Date,
        public dolgozik?: boolean
    ) {
        this.dolgozik = false;
    }
}
