import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { EntityService } from '../../shared/service/entity.service';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { Gep } from './gep.model';

@Injectable({
    providedIn: 'root'
})
export class GepService extends EntityService<any> {

    constructor(protected http: HttpClient, protected toastrService: ToastrService) {
        super(http, '/api/gep', toastrService);
    }

    munkatKezd(gepList: Gep): Observable<void> {
        const httpParam = new HttpParams();
        return this.http.put<void>(`${this.url}/munkakezd`, gepList);
    }
}
