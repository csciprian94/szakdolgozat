import { Injectable } from '@angular/core';
import { EntityService } from '../../shared/service/entity.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class TelephelyService extends EntityService<any> {

  constructor(protected http: HttpClient, protected toastrService: ToastrService) {
    super(http, 'api/telephely', toastrService);
  }
}
