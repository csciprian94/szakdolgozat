import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { entityRoute } from './entity.route';
import { MegyeService } from './megye/megye.service';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { TelephelyListComponent } from './telephely/telephely-list/telephely-list.component';
import { SharedModule } from '../shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { BsDatepickerModule, BsModalRef, BsModalService, DatepickerModule, ModalModule } from 'ngx-bootstrap';
import { TelephelyFormComponent } from './telephely/telephely-form/telephely-form.component';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { DeletePopupComponent } from './delete-popup.component';
import { GepListComponent } from './gep/gep-list/gep-list.component';
import { GepFormComponent } from './gep/gep-form/gep-form.component';
import { DolgozoListComponent } from './dolgozo/dolgozo-list/dolgozo-list.component';
import { DolgozoFormComponent } from './dolgozo/dolgozo-form/dolgozo-form.component';
import { MunkaListComponent } from './munka/munka-list/munka-list.component';
import { MunkaFormComponent } from './munka/munka-form/munka-form.component';
import { MunkaPopupComponent } from './munka/munka-popup/munka-popup.component';

const ENTITY_STATES = [...entityRoute];

@NgModule({
    imports: [
        RouterModule.forRoot(ENTITY_STATES, { useHash: true, enableTracing: true }),
        ToastrModule.forRoot(),
        SharedModule,
        NgxDatatableModule,
        ModalModule.forRoot(),
        FormsModule,
        NgSelectModule,
        DatepickerModule,
        BsDatepickerModule.forRoot(),
    ],
    declarations: [
        TelephelyListComponent,
        TelephelyFormComponent,
        DeletePopupComponent,
        GepListComponent,
        GepFormComponent,
        DolgozoListComponent,
        DolgozoFormComponent,
        MunkaListComponent,
        MunkaFormComponent,
        MunkaPopupComponent
    ],
    entryComponents: [
        TelephelyFormComponent,
        DeletePopupComponent,
        GepFormComponent,
        DolgozoFormComponent,
        MunkaPopupComponent,
        MunkaFormComponent
    ],
    providers: [MegyeService,
        ToastrService,
        BsModalService,
        BsModalRef
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EntityModule {
}
