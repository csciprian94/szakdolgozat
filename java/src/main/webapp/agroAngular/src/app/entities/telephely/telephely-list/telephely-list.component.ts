import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Telephely } from '../telephely.model';
import { Megye } from '../../megye/megye.model';
import { MegyeService } from '../../megye/megye.service';
import { TelephelyService } from '../telephely.service';
import { ToastrService } from 'ngx-toastr';
import { PageResponse } from '../../../shared/model/page-response';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { TelephelyFormComponent } from '../telephely-form/telephely-form.component';
import { Subscription } from 'rxjs';
import { ENTITY_VALUES } from '../../entity-constanst';

@Component({
    selector: 'app-telephely-list',
    templateUrl: './telephely-list.component.html',
    styles: []
})
export class TelephelyListComponent implements OnInit {

    telephely: Telephely;
    mukodesKezdeteK: any;
    mukodesKezdeteV: any;
    megyeId: number;
    bsRangeValue: Date[];
    megyeList: Megye[];

    columns = [];
    items: any;
    itemsPerPage = ENTITY_VALUES.PAGE_SIZE;
    totalItems = 0;
    pageNumber = 0;
    previousPage = 0;
    sortOptions: string[] = ['id,asc'];
    private subscription: Subscription;
    initialSelection = [];
    protected subscriptions: Subscription[] = [];

    queryParams: any;
    @ViewChild('buttonTemplates') public buttonTemplates: TemplateRef<any>;

    constructor(
        private toastrService: ToastrService,
        private router: Router,
        private megyeService: MegyeService,
        private telephelyService: TelephelyService,
        private modalService: BsModalService,
        private bsModalRef: BsModalRef,
    ) {
        this.subscriptions.push(this.modalService.onHidden.subscribe(() => {
            this.loadAll();
        }));
        this.megyeService.findAll().subscribe(megyek => {
            this.megyeList = megyek._embedded.megye;
        });
        this.telephely = new Telephely();
        this.bsRangeValue = null;
    }

    ngOnInit() {
        this.loadAll();
    }

    loadAll() {

        this.columns = [
            { prop: 'nev', name: 'Név', sort: 'nev' },
            { prop: 'megye.megyenev', name: 'Megye', sort: 'megye.megyenev' },
            { prop: 'telepules', name: 'Település', sort: 'telepules' },
            { prop: 'cim', name: 'Cím' },
            { prop: 'mukodesKezdete', name: 'Működés kezdete' },
            { cellTemplate: this.buttonTemplates }
        ];

        this.telephelyService
            .query({
                page: this.pageNumber - 1,
                size: this.itemsPerPage,
                filter: this.queryParams,
                sort: this.sortOptions
            })
            .subscribe(
                (res: PageResponse<any>) => {
                    this.items = res._embedded ? (res._embedded[Object.keys(res._embedded)[0]] as any[]) : [];
                    this.totalItems = res.page.totalElements;
                    this.pageNumber = res.page.number;
                },
                () => {
                    this.toastrService.error('Nem sikerült betölteni a táblázat adatait');
                }
            );
    }

    public trackByFn(index, item) {
        return index;
    }

    onDelete(id: number) {
        this.router.navigate(['telephely', id, 'delete']);
    }

    mukodesDatumChange(event: Date) {
        if (event) {
            this.mukodesKezdeteK = new Date(event[0]).toISOString();
            this.mukodesKezdeteV = new Date(event[1]).toISOString();
        }
    }

    onSearch() {
        this.queryParams = {
            'nev.contains': this.telephely.nev,
            'telepules.contains': this.telephely.telepules,
            'cim.contains': this.telephely.cim,
            'megyeId.equals': this.megyeId,
            'mukodesKezdeteK.greaterOrEqualThan': this.mukodesKezdeteK,
            'mukodesKezdeteV.lessOrEqualThan': this.mukodesKezdeteV
        };
        this.loadAll();
    }

    clearSearch() {
        this.mukodesKezdeteK = null;
        this.mukodesKezdeteV = null;
        this.queryParams = null;
        this.router.navigate(['/telephely'], { queryParams: {} });
        this.loadAll();
    }

    loadPage(event: any) {
        if (event.offset !== this.previousPage) {
            this.previousPage = event.offset;
            this.pageNumber = event.offset + 1;
            this.loadAll();
        }
    }

    edit(item: any) {
        if (item) {
            this.bsModalRef = this.modalService.show(TelephelyFormComponent, { initialState: { id: item.id } });
        } else {
            this.bsModalRef = this.modalService.show(TelephelyFormComponent);
        }
    }

    delete(item: any) {
        this.telephelyService.delete(item.id).subscribe(() => {
            this.loadAll();
        });
    }
}
