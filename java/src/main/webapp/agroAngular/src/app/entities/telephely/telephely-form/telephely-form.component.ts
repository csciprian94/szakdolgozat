import { Component, EventEmitter, OnInit } from '@angular/core';
import { TelephelyService } from '../telephely.service';
import { BsModalRef } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Telephely } from '../telephely.model';
import { Megye } from '../../megye/megye.model';
import { MegyeService } from '../../megye/megye.service';

@Component({
    selector: 'app-telephely-form',
    templateUrl: './telephely-form.component.html',
})
export class TelephelyFormComponent implements OnInit {
    telephely: Telephely;
    megyeList: Megye[];
    id: number;

    constructor(private service: TelephelyService, private  megyeService: MegyeService, public bsModalRef: BsModalRef, private toastrService: ToastrService
    ) {
        this.telephely = new Telephely();
    }

    ngOnInit() {
        this.megyeService.findAll().subscribe(megyek => {
            this.megyeList = megyek._embedded.megye
        });
        if (this.id) {
            this.service.findOne(this.id).subscribe(entity => {
                this.telephely = entity;
            })
        }
    }

    save() {
        this.service.save(this.telephely).subscribe(() => {
            this.toastrService.success('Sikeres Telephely mentés');
            this.bsModalRef.hide();
        });
    }
}
