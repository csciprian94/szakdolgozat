import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'delete-popup',
    templateUrl: './delete-popup.component.html'
})
export class DeletePopupComponent implements OnInit {

    id: number;
    service: any;

    ngOnInit() {
    }

    confirmDelete() {
        this.service.delete(this.id).subscribe()
    }
}
