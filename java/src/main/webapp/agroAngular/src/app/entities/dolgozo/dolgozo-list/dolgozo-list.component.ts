import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Dolgozo } from '../../dolgozo/dolgozo.model';
import { Telephely } from '../../telephely/telephely.model';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { TelephelyService } from '../../telephely/telephely.service';
import { DolgozoService } from '../../dolgozo/dolgozo.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { PageResponse } from '../../../shared/model/page-response';
import { DolgozoFormComponent } from '../dolgozo-form/dolgozo-form.component';
import { Beosztas } from '../../beosztas/beosztas.model';
import { BeosztasService } from '../../beosztas/beosztas.service';
import { ENTITY_VALUES } from '../../entity-constanst';

@Component({
    selector: 'app-dolgozo-list',
    templateUrl: './dolgozo-list.component.html',
})
export class DolgozoListComponent implements OnInit {

    dolgozo: Dolgozo;
    cegTulajdonaK: any;
    cegTulajdonaV: any;
    bsRangeValue: Date[];
    telephelyList: Telephely[];
    beosztasList: Beosztas[];
    beosztasId: number;
    telephelyId: number;
    columns = [];
    items: any;
    itemsPerPage = ENTITY_VALUES.PAGE_SIZE;
    totalItems = 0;
    pageNumber = 0;
    previousPage = 0;
    sortOptions: string[] = ['id,asc'];
    protected subscriptions: Subscription[] = [];

    queryParams: any;
    @ViewChild('buttonTemplates') public buttonTemplates: TemplateRef<any>;
    @ViewChild('dolgozikTemplate') public dolgozikTemplate: TemplateRef<any>;

    constructor(
        private toastrService: ToastrService,
        private router: Router,
        private beosztasService: BeosztasService,
        private telephelyService: TelephelyService,
        private dolgozoService: DolgozoService,
        private modalService: BsModalService,
        private bsModalRef: BsModalRef,
    ) {
        this.subscriptions.push(this.modalService.onHidden.subscribe(() => {
            this.loadAll();
        }));
        this.telephelyService.findAll().subscribe(telephely => {
            this.telephelyList = telephely._embedded.telephely;
        });
        this.beosztasService.findAll().subscribe(beosztas => {
            this.beosztasList = beosztas._embedded.beosztas;
        });

        this.dolgozo = new Dolgozo();
        this.bsRangeValue = null;
    }

    ngOnInit() {
        this.loadAll();
    }

    loadAll() {

        this.columns = [
            { prop: 'dolgozoTeljesNeve', name: 'Dolgozó neve', sort: 'dolgozoTeljesNeve' },
            { prop: 'beosztas.nev', name: 'Beosztás', sort: 'beosztas.nev' },
            { prop: 'telephely.nev', name: 'Telephely', sort: 'telephely.nev' },
            { prop: 'lakhely', name: 'Lakhely', sort: 'lakhely' },
            { cellTemplate: this.dolgozikTemplate, name: 'Dolgozik' },
            { cellTemplate: this.buttonTemplates }
        ];

        this.dolgozoService
            .query({
                page: this.pageNumber - 1,
                size: this.itemsPerPage,
                filter: this.queryParams,
                sort: this.sortOptions
            })
            .subscribe(
                (res: PageResponse<any>) => {
                    this.items = res._embedded ? (res._embedded[Object.keys(res._embedded)[0]] as any[]) : [];
                    this.totalItems = res.page.totalElements;
                    this.pageNumber = res.page.number;
                },
                () => {
                    this.toastrService.error('Nem sikerült betölteni a táblázat adatait');
                }
            );
    }

    public trackByFn(index, item) {
        return index;
    }

    onDelete(id: number) {
        this.router.navigate(['dolgozo', id, 'delete']);
    }

    onSearch() {
        this.queryParams = {
            'teljesNev.contains': this.dolgozo.vezetekNev,
            'telephelyId.equals': this.telephelyId,
            'beosztasId.equals': this.beosztasId,
            'dolgozik.equals': this.dolgozo.dolgozik,
            'lakhely.contains': this.dolgozo.lakhely,
        };
        this.loadAll();
    }

    clearSearch() {
        this.queryParams = null;
        this.router.navigate(['/dolgozo'], { queryParams: {} });
        this.loadAll();
    }

    loadPage(event: any) {
        if (event.offset !== this.previousPage) {
            this.previousPage = event.offset;
            this.pageNumber = event.offset + 1;
            this.loadAll();
        }
    }

    edit(item: any) {
        if (item) {
            this.bsModalRef = this.modalService.show(DolgozoFormComponent, { initialState: { id: item.id } });
        } else {
            this.bsModalRef = this.modalService.show(DolgozoFormComponent);
        }
    }

    delete(item: any) {
        this.dolgozoService.delete(item.id).subscribe(() => {
            this.loadAll();
        });
    }
}
