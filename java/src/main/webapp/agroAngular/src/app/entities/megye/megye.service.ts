import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EntityService } from '../../shared/service/entity.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class MegyeService extends EntityService<any> {

  constructor(protected http: HttpClient, protected toastrService: ToastrService) {
    super(http, '/api/megye', toastrService);
  }
}
