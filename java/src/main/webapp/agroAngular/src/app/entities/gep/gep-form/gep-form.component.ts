import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Gep } from '../gep.model';
import { GepService } from '../gep.service';
import { TelephelyService } from '../../telephely/telephely.service';
import { GeptipusService } from '../../geptipus/geptipus.service';
import { Marka } from '../../marka/marka.model';
import { Telephely } from '../../telephely/telephely.model';
import { GepTipus } from '../../geptipus/geptipus.model';
import { MarkasService } from '../../marka/markas.service';

@Component({
    selector: 'app-gep-form',
    templateUrl: './gep-form.component.html',
})
export class GepFormComponent implements OnInit {
    gep: Gep;
    readOnly = false;
    id: number;
    markaList: Marka[];
    telephelyList: Telephely[];
    geptipusList: GepTipus[];

    statuszList: { value: string, label: string }[] = [
        { value: 'HASZNALATBAN', label: 'Használatban' },
        { value: 'KARBANTARTAS', label: 'Karbantartás alatt' },
        { value: 'UZEMEN_KIVUL', label: 'Üzemen kívül' },
        { value: 'VARAKOZIK', label: 'Várakozik' }
    ];

    constructor(private service: GepService, private telephelyService: TelephelyService, private geptipusService: GeptipusService,
                private markaService: MarkasService, public bsModalRef: BsModalRef, private toastrService: ToastrService
    ) {
        this.gep = new Gep();
    }

    ngOnInit() {
        this.markaService.findAll().subscribe(marka => {
            this.markaList = marka._embedded.marka;
        });
        this.geptipusService.findAll().subscribe(geptipus => {
            this.geptipusList = geptipus._embedded.geptipus;
        });
        this.telephelyService.findAll().subscribe(telephely => {
            this.telephelyList = telephely._embedded.telephely;
        });
        if (this.id) {
            this.service.findOne(this.id).subscribe(entity => {
                this.gep = entity;
            });
        } else {
            this.gep.statusz = 'UJ';
            this.gep.uzemIdo = 0;
        }
    }

    save() {
        this.service.save(this.gep).subscribe(() => {
            this.toastrService.success('Sikeres Gép mentés');
            this.bsModalRef.hide();
        });
    }
}
