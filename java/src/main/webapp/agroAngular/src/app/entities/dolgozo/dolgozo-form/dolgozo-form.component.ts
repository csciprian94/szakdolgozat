import { Component, OnInit } from '@angular/core';
import { Telephely } from '../../telephely/telephely.model';
import { TelephelyService } from '../../telephely/telephely.service';
import { BsModalRef } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Beosztas } from '../../beosztas/beosztas.model';
import { Dolgozo } from '../dolgozo.model';
import { DolgozoService } from '../dolgozo.service';
import { BeosztasService } from '../../beosztas/beosztas.service';

@Component({
    selector: 'app-dolgozo-form',
    templateUrl: './dolgozo-form.component.html',
})
export class DolgozoFormComponent implements OnInit {

    dolgozo: Dolgozo;
    id: number;
    telephelyList: Telephely[];
    beosztasList: Beosztas[];
    statuszList: { value: string, label: string }[] = [
        { value: 'UJ', label: 'Új' },
        { value: 'HASZNALATBAN', label: 'Használatban' },
        { value: 'KARBANTARTAS', label: 'Karbantartás alatt' },
        { value: 'UZEMEN_KIVUL', label: 'Üzemen kívül' }
    ];

    constructor(
        private service: DolgozoService,
        private telephelyService: TelephelyService,
        private beosztasService: BeosztasService,
        public bsModalRef: BsModalRef,
        private toastrService: ToastrService
    ) {
        this.dolgozo = new Dolgozo();
    }

    ngOnInit() {
        this.telephelyService.findAll().subscribe(telephely => {
            this.telephelyList = telephely._embedded.telephely;
        });
        this.beosztasService.findAll().subscribe(beosztas => {
            this.beosztasList = beosztas._embedded.beosztas;
        });
        if (this.id) {
            this.service.findOne(this.id).subscribe(entity => {
                this.dolgozo = entity;
            });
        }
    }

    save() {
        this.service.save(this.dolgozo).subscribe(() => {
            this.toastrService.success('Sikeres Dolgozó mentés');
            this.bsModalRef.hide();
        });
    }
}
