import { Injectable } from '@angular/core';
import { EntityService } from '../../shared/service/entity.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { Munka } from './munka.model';

@Injectable({
    providedIn: 'root'
})
export class MunkaService extends EntityService<any> {

    constructor(protected http: HttpClient, protected toastrService: ToastrService) {
        super(http, 'api/munka', toastrService);
    }

    munkaVege(munka: Munka): Observable<void> {
        return this.http.put<void>(`${this.url}/munkavege`, munka);
    }
}
