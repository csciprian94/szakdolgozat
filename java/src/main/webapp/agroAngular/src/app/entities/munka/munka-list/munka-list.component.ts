import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { GepService } from '../../gep/gep.service';
import { PageResponse } from '../../../shared/model/page-response';
import { MunkaService } from '../munka.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { MunkaFormComponent } from '../munka-form/munka-form.component';
import { ENTITY_VALUES } from '../../entity-constanst';
import { Munka } from '../munka.model';
import { DolgozoService } from '../../dolgozo/dolgozo.service';
import { Gep } from '../../gep/gep.model';
import { Dolgozo } from '../../dolgozo/dolgozo.model';

@Component({
    selector: 'app-munka-list',
    templateUrl: './munka-list.component.html',
    styleUrls: ['./munka-list.component.sass']
})
export class MunkaListComponent implements OnInit {
    munkaStatuszEnum = {
        FOLYAMATBAN: 'Folyamatban',
        LEZARULT: 'Lezárult',
        MEGSZAKADT: 'Megszakadt'
    };
    munkaStatuszList = [
        { value: 'FOLYAMATBAN', label: 'Folyamatban' },
        { value: 'LEZARULT', label: 'Lezárult' },
        { value: 'MEGSZAKADT', label: 'Megszakadt' }];

    gepList: Gep[];
    dolgozoList: Dolgozo[];
    gepId: number;
    dolgozoId: number;

    munka = new Munka();
    cegTulajdonaK: any;
    cegTulajdonaV: any;

    columns = [];
    items: any;
    itemsPerPage = ENTITY_VALUES.PAGE_SIZE;
    totalItems = 0;
    pageNumber = 0;
    previousPage = 0;
    sortOptions: string[] = ['id,asc'];

    queryParams: any;
    @ViewChild('buttonTemplates') public buttonTemplates: TemplateRef<any>;
    @ViewChild('munkaStatuszTemplate') public munkaStatuszTemplate: TemplateRef<any>;

    constructor(
        private toastrService: ToastrService,
        private router: Router,
        private gepService: GepService,
        private dolgozoService: DolgozoService,
        private munkaService: MunkaService,
        private modalService: BsModalService,
        private bsModalRef: BsModalRef,
    ) {
    }

    ngOnInit() {
        this.loadAll();

        this.gepService.findAll().subscribe(gep => {
            this.gepList = gep._embedded.gep;
        });
        this.dolgozoService.findAll().subscribe(dolgozo => {
            this.dolgozoList = dolgozo._embedded.dolgozo;
        });
    }

    loadAll() {

        this.columns = [
            { prop: 'gep.nev', name: 'Gép', sort: 'gep.nev' },
            { prop: 'dolgozo.dolgozoTeljesNeve', name: 'Dolgozó', sort: 'dolgozo.dolgozoTeljesNeve' },
            { prop: 'munkaido', name: 'Munkaidő', sort: 'munkaido' },
            { cellTemplate: this.munkaStatuszTemplate, name: 'statusz' },
            { cellTemplate: this.buttonTemplates }
        ];

        this.munkaService
            .query({
                page: this.pageNumber - 1,
                size: this.itemsPerPage,
                filter: this.queryParams,
                sort: this.sortOptions
            })
            .subscribe(
                (res: PageResponse<any>) => {
                    this.items = res._embedded ? (res._embedded[Object.keys(res._embedded)[0]] as any[]) : [];
                    this.totalItems = res.page.totalElements;
                },
                () => {
                    this.toastrService.error('Nem sikerült betölteni a táblázat adatait');
                }
            );
    }

    public trackByFn(index, item) {
        return index;
    }

    onDelete(id: number) {
        this.router.navigate(['munka', id, 'delete']);
    }

    onSearch() {
        this.queryParams = {
            'gepId.equals': this.gepId,
            'munkaido.greaterThan': this.munka.munkaido,
            'statusz.equals': this.munka.statusz,
        };
        this.loadAll();
    }

    clearSearch() {
        this.cegTulajdonaK = null;
        this.cegTulajdonaV = null;
        this.queryParams = null;
        this.router.navigate(['/munka'], { queryParams: {} });
        this.loadAll();
    }

    loadPage(event: any) {
        if (event.offset !== this.previousPage) {
            this.previousPage = event.offset;
            this.pageNumber = event.offset + 1;
            this.loadAll();
        }
    }

    edit(item: any) {
        if (item) {
            this.bsModalRef = this.modalService.show(MunkaFormComponent, { initialState: { id: item.id } });
        } else {
            this.bsModalRef = this.modalService.show(MunkaFormComponent);
        }
    }

    delete(item: any) {
        this.munkaService.delete(item.id).subscribe(() => {
            this.loadAll();
        });
    }

    munkaVege(item: any) {
        this.munkaService.munkaVege(item).subscribe(() => this.loadAll());
    }

    munkaban(item: any) {
        return item.statusz !== 'FOLYAMATBAN';
    }

    setStatuszLabel(statusz: string) {
        return this.munkaStatuszEnum[statusz];
    }
}
