import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MegyeService } from '../../megye/megye.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { PageResponse } from '../../../shared/model/page-response';
import { Gep } from '../gep.model';
import { GepFormComponent } from '../gep-form/gep-form.component';
import { GepService } from '../gep.service';
import { MarkasService } from '../../marka/markas.service';
import { TelephelyService } from '../../telephely/telephely.service';
import { Marka } from '../../marka/marka.model';
import { GepTipus } from '../../geptipus/geptipus.model';
import { GeptipusService } from '../../geptipus/geptipus.service';
import { Telephely } from '../../telephely/telephely.model';
import { DolgozoService } from '../../dolgozo/dolgozo.service';
import { MunkaPopupComponent } from '../../munka/munka-popup/munka-popup.component';
import { ENTITY_VALUES } from '../../entity-constanst';

@Component({
    selector: 'app-gep-list',
    templateUrl: './gep-list.component.html',
})
export class GepListComponent implements OnInit {
    gepStatuszEnum = {
        UJ: 'Új',
        HASZNALATBAN: 'Használatban',
        KARBANTARTAS: 'Karbantartás alatt',
        UZEMEN_KIVUL: 'Üzemen kívül',
        VARAKOZIK: 'Várakozik'
    };

    gepStatuszList = [
        { value: 'UJ', label: 'Új' },
        { value: 'HASZNALATBAN', label: 'Használatban' },
        { value: 'KARBANTARTAS', label: 'Karbantartás alatt' },
        { value: 'UZEMEN_KIVUL', label: 'Üzemen kívül' },
        { value: 'VARAKOZIK', label: 'Várakozik' }];

    markaId: number;
    gepTipusId: number;
    telephelyId: number;
    gep: Gep;
    cegTulajdonaK: any;
    cegTulajdonaV: any;
    megyeId: number;
    bsRangeValue: Date[];
    markaList: Marka[];
    telephelyList: Telephely[];
    geptipusList: GepTipus[];

    columns = [];
    items: any;
    itemsPerPage = ENTITY_VALUES.PAGE_SIZE;
    totalItems = 0;
    pageNumber = 0;
    previousPage = 0;
    sortOptions: string[] = ['id,asc'];
    protected subscriptions: Subscription[] = [];

    queryParams: any;
    @ViewChild('buttonTemplates') public buttonTemplates: TemplateRef<any>;
    @ViewChild('gepStatuszTemplate') public gepStatuszTemplate: TemplateRef<any>;

    constructor(
        private toastrService: ToastrService,
        private router: Router,
        private megyeService: MegyeService,
        private geptipusService: GeptipusService,
        private markaService: MarkasService,
        private telephelyService: TelephelyService,
        private gepService: GepService,
        private dolgozoService: DolgozoService,
        private modalService: BsModalService,
        private bsModalRef: BsModalRef,
    ) {
        this.subscriptions.push(this.modalService.onHidden.subscribe(() => {
            this.loadAll();
        }));

        this.gep = new Gep();
        this.bsRangeValue = null;
    }

    ngOnInit() {
        this.loadAll();

        this.markaService.findAll().subscribe(marka => {
            this.markaList = marka._embedded.marka;
        });
        this.geptipusService.findAll().subscribe(geptipus => {
            this.geptipusList = geptipus._embedded.geptipus;
        });
        this.telephelyService.findAll().subscribe(telephely => {
            this.telephelyList = telephely._embedded.telephely;
        });
    }

    loadAll() {

        this.columns = [
            { prop: 'nev', name: 'Név', sort: 'nev' },
            { prop: 'marka.nev', name: 'Márka', sort: 'marka.nev' },
            { prop: 'gepTipus.nev', name: 'Gép típus', sort: 'gepTipus.nev' },
            { prop: 'telephely.nev', name: 'Telephely', sort: 'telephely.nev' },
            { prop: 'uzemIdo', name: 'Üzemidő (óra)', sort: 'uzemIdo' },
            { cellTemplate: this.gepStatuszTemplate, name: 'Gép státusza' },
            { cellTemplate: this.buttonTemplates }
        ];

        this.gepService
            .query({
                page: this.pageNumber - 1,
                size: this.itemsPerPage,
                filter: this.queryParams,
                sort: this.sortOptions
            })
            .subscribe(
                (res: PageResponse<any>) => {
                    this.items = res._embedded ? (res._embedded[Object.keys(res._embedded)[0]] as any[]) : [];
                    this.totalItems = res.page.totalElements;
                    this.pageNumber = res.page.number;
                },
                () => {
                    this.toastrService.error('Nem sikerült betölteni a táblázat adatait');
                }
            );
    }

    public trackByFn(index, item) {
        return index;
    }

    onDelete(id: number) {
        this.router.navigate(['gep', id, 'delete']);
    }

    mukodesDatumChange(event: Date) {
        if (event) {
            this.cegTulajdonaK = new Date(event[0]).toISOString();
            this.cegTulajdonaV = new Date(event[1]).toISOString();
        }
    }

    onSearch() {
        this.queryParams = {
            'nev.contains': this.gep.nev,
            'megyeId.equals': this.megyeId,
            'markaId.equals': this.markaId,
            'telephelyId.equals': this.telephelyId,
            'geptipId.equals': this.gepTipusId,
            'uzemIdo.greaterThan': this.gep.uzemIdo,
            'statusz.equals': this.gep.statusz,
        };
        this.loadAll();
    }

    clearSearch() {
        this.cegTulajdonaK = null;
        this.cegTulajdonaV = null;
        this.queryParams = null;
        this.router.navigate(['/gep'], { queryParams: {} });
        this.loadAll();
    }

    loadPage(event: any) {
        if (event !== this.previousPage) {
            this.previousPage = event.offset;
            this.pageNumber = event.offset + 1;
            this.loadAll();
        }
    }

    edit(item: any) {
        if (item) {
            this.bsModalRef = this.modalService.show(GepFormComponent, {
                initialState: {
                    id: item.id,
                    readOnly: true
                }
            });
        } else {
            this.bsModalRef = this.modalService.show(GepFormComponent);
        }
    }

    delete(item: any) {
        this.gepService.delete(item.id).subscribe(() => {
            this.loadAll();
        });
    }

    startWork(item: any) {
        this.bsModalRef = this.modalService.show(MunkaPopupComponent, { initialState: { gep: item } });
    }

    dolgozhat(item: any) {
        return item.statusz === 'UJ' || item.statusz === 'VARAKOZIK';
    }

    torolhet(item: any) {
        return item.statusz !== 'UZEMEN_KIVUL';
    }

    setStatuszLabel(statusz: string) {
        return this.gepStatuszEnum[statusz];
    }
}
