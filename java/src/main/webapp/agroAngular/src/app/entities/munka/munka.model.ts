import { Gep } from '../gep/gep.model';
import { Dolgozo } from '../dolgozo/dolgozo.model';

export class Munka {
    constructor(
        public id?: number,
        public gep?: Gep,
        public dolgozo?: Dolgozo,
        public munkaido?: number,
        public statusz?: string,
        public leiras?: string
    ) {
    }
}
