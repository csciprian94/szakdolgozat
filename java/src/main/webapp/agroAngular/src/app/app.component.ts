import { Component, OnInit } from '@angular/core';
import { MegyeService } from './entities/megye/megye.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit{
  title = 'agroAngular';


  constructor(public megyeService: MegyeService) {
  }

  ngOnInit(): void {
    this.megyeService.findAll().subscribe();
  }
}
