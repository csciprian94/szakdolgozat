export interface PageRequest {
  page: number;
  size: number;
  sort?: any;
  filter?: any;
}
