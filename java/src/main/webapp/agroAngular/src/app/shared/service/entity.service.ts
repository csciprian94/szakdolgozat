import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { CrudService } from './crud.service';
import { catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { PageResponse } from '../model/page-response';
import { PageRequest } from '../model/page-request';


@Injectable()
export abstract class EntityService<E> implements CrudService<E> {
    protected constructor(protected http: HttpClient, protected url: string, protected toastrService: ToastrService) {
    }

    create(model: E): Observable<E> {
        return this.http.post<E>(this.url, model)
            .pipe(
                catchError((error) => {
                    this.toastrService.error('Nem sikerült a bejegyzést létrehozni.');
                    return throwError(error);
                })
            );
    }

    update(id: number, model: E): Observable<E> {
        return this.http.put<E>(`${this.url}/${id}`, model)
            .pipe(
                catchError((error) => {
                    this.toastrService.error('Nem sikerült a bejegyzést módosítani.');
                    return throwError(error);
                })
            );
    }

    delete(id: number): Observable<void> {
        return this.http.delete<void>(`${this.url}/${id}`)
            .pipe(
                catchError((error) => {
                    this.toastrService.error('Nem sikerült a bejegyzést törölni.');
                    return throwError(error);
                })
            );
    }

    save(model: E): Observable<E> {
        return !model['id'] ? this.create(model) : this.update(model['id'], model);
    }

    findAll(): Observable<PageResponse<E>> {
        return this.http.get<PageResponse<E>>(`${this.url}`);
    }

    query(pageRequest: PageRequest): Observable<PageResponse<E>> {
        const params = createRequestOption(pageRequest);
        return this.http.get<PageResponse<E>>(`${this.url}/query`, { params });
    }

    findOne(id: number): Observable<E> {
        return this.http.get<E>(`${this.url}/${id}`)
            .pipe(
                catchError((error) => {
                    this.toastrService.error('Nem sikerült a bejegyzést lekérdezni.');
                    return throwError(error);
                })
            );
    }

}

export const createRequestOption = (req?: any): HttpParams => {
    let options: HttpParams = new HttpParams();
    if (req) {
        Object.keys(req).forEach(key => {
            if (key !== 'sort' && key !== 'filter') {
                options = options.set(key, req[key]);
            }
        });
        if (req.sort) {
            req.sort.forEach(val => {
                options = options.append('sort', val);
            });
        }

        if (req.filter) {
            const keys = Object.keys(req.filter);

            keys.forEach(key => {
                if (req.filter[key] !== undefined && req.filter[key] !== null) {
                    options = options.append(key, req.filter[key]);
                }
            });
        }
    }
    return options;
};
