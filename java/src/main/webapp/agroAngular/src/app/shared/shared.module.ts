import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { TableBuilderComponent } from './components/table-builder.component';
import { BsDropdownModule } from 'ngx-bootstrap';


@NgModule({
    // prettier-ignore
    imports: [
        ToastrModule,
        BsDropdownModule.forRoot()
    ],
    declarations: [
        TableBuilderComponent,
    ],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    exports: [TableBuilderComponent]
})
export class SharedModule {
}
