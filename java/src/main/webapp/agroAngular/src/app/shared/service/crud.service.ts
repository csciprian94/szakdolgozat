import { Observable } from 'rxjs';

export interface CrudService<E> {
  create(model: E): Observable<E>;

  update(id: number, model: E): Observable<E>;

  findOne(id: number): Observable<E>;

  delete(id: number): Observable<void>;

  save(model: E): Observable<E>;
}
