import { Component, Input, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Megye } from '../../entities/megye/megye.model';
import { Telephely } from '../../entities/telephely/telephely.model';
import { EntityService } from '../service/entity.service';
import { EventManager } from '@angular/platform-browser';

@Component({
    selector: 'table-builder',
    templateUrl: './table-builder.component.html'
})
export class TableBuilderComponent implements OnInit, OnDestroy {
    @Input() columns: any;
    @Input() eventName: string;
    @Input() items: any;
    queryParams: {};

    selected = [];
    megyeList: Megye[];
    megye: Megye;
    row = new Array<Telephely>();
    map = new Map();

    eventSubscriber: Subscription;
    @ViewChild('buttonTemplates') public buttonTemplates: TemplateRef<any>;

    itemsPerPage = 20;
    totalItems = 0;
    pageNumber = 0;
    previousPage = 0;
    sortOptions: string[];

    constructor(
        private router: Router,
        protected toasterService: ToastrService,
        private activatedRoute: ActivatedRoute,
        protected eventManager: EventManager
    ) {
        this.megye = new Megye();
    }

    ngOnDestroy(): void {
    }

    ngOnInit(): void {
        this.loadAll();
        console.log(this.items);
    }

    loadAll() {
        this.columns.push({ cellTemplate: this.buttonTemplates });
    }

    loadPage(event: any, forceLoad = false) {
        if (event.offset !== this.previousPage || forceLoad) {
            this.previousPage = event.offset;
            this.pageNumber = event.offset + 1;
            this.loadAll();
        }
    }

    onSort(event) {
        this.sortOptions = event.sorts.map(s => `${this.map.get(s.prop)},${s.dir}`);
    }
}
