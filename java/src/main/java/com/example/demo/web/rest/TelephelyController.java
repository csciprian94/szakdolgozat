package com.example.demo.web.rest;

import com.example.demo.repository.TelephelyRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/" + TelephelyRepository.ENTITY_NAME)
public class TelephelyController {
    private final TelephelyRepository repository;

    public TelephelyController(TelephelyRepository repository) {
        this.repository = repository;
    }
}
