package com.example.demo.web.rest;

import com.example.demo.criteria.DolgozoCriteria;
import com.example.demo.domain.Dolgozo;
import com.example.demo.repository.DolgozoRepository;
import com.example.demo.service.DolgozoQueryService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/api/" + DolgozoRepository.ENTITY_NAME)
public class DolgozoQueryController {

    protected final PagedResourcesAssembler resourcesAssembler;
    protected final DolgozoQueryService queryService;

    public DolgozoQueryController(PagedResourcesAssembler resourcesAssembler, DolgozoQueryService queryService) {
        this.resourcesAssembler = resourcesAssembler;
        this.queryService = queryService;
    }

    @GetMapping("/query")
    public PagedResources<Resource<Dolgozo>> query(DolgozoCriteria criteria, Pageable page) {
        PagedResources<Resource<Dolgozo>> resources = resourcesAssembler.toResource(queryService.findByCriteria(criteria, page));
        resources.getContent().forEach(r -> r.add(linkTo(this.getClass()).slash(r.getContent().getId()).withSelfRel()));
        return resources;
    }
}