package com.example.demo.web.rest;

import com.example.demo.domain.Gep;
import com.example.demo.repository.GepRepository;
import com.example.demo.service.GepService;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/" + GepRepository.ENTITY_NAME)
public class GepController {

    private final GepService service;

    public GepController(GepService service) {
        this.service = service;
    }

    @PutMapping("munkakezd")
    public void munkatKezd(@RequestBody Gep gep) {
        service.munkatKezd(gep);
    }
}
