package com.example.demo.web.rest;

import com.example.demo.criteria.MunkaCriteria;
import com.example.demo.domain.Munka;
import com.example.demo.repository.MunkaRepository;
import com.example.demo.service.MunkaQueryService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/api/" + MunkaRepository.ENTITY_NAME)
public class MunkaQueryController {

    protected final PagedResourcesAssembler resourcesAssembler;
    protected final MunkaQueryService queryService;

    public MunkaQueryController(PagedResourcesAssembler resourcesAssembler, MunkaQueryService queryService) {
        this.resourcesAssembler = resourcesAssembler;
        this.queryService = queryService;
    }

    @GetMapping("/query")
    public PagedResources<Resource<Munka>> query(MunkaCriteria criteria, Pageable page) {
        PagedResources<Resource<Munka>> resources = resourcesAssembler.toResource(queryService.findByCriteria(criteria, page));
        resources.getContent().forEach(r -> r.add(linkTo(this.getClass()).slash(r.getContent().getId()).withSelfRel()));
        return resources;
    }
}
