package com.example.demo.web.rest;

import com.example.demo.repository.MegyeRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/" + MegyeRepository.ENTITY_NAME)
public class MegyeController {
    private final MegyeRepository megyeRepository;

    public MegyeController(MegyeRepository megyeRepository) {
        this.megyeRepository = megyeRepository;
    }
}
