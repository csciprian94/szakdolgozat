package com.example.demo.web.rest;

import com.example.demo.criteria.TelephelyCriteria;
import com.example.demo.domain.Telephely;
import com.example.demo.repository.TelephelyRepository;
import com.example.demo.service.TelephelyQueryService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/api/" + TelephelyRepository.ENTITY_NAME)
public class TelephelyQueryController {

    protected final PagedResourcesAssembler resourcesAssembler;
    protected final TelephelyQueryService queryService;

    public TelephelyQueryController(PagedResourcesAssembler resourcesAssembler, TelephelyQueryService queryService) {
        this.resourcesAssembler = resourcesAssembler;
        this.queryService = queryService;
    }

    @GetMapping("/query")
    public PagedResources<Resource<Telephely>> query(TelephelyCriteria criteria, Pageable page) {
        PagedResources<Resource<Telephely>> resources = resourcesAssembler.toResource(queryService.findByCriteria(criteria, page));
        resources.getContent().forEach(r -> r.add(linkTo(this.getClass()).slash(r.getContent().getId()).withSelfRel()));
        return resources;
    }
}
