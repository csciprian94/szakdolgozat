package com.example.demo.web.rest;

import com.example.demo.criteria.GepCriteria;
import com.example.demo.domain.Gep;
import com.example.demo.repository.GepRepository;
import com.example.demo.service.GepQueryService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/api/" + GepRepository.ENTITY_NAME)
public class GepQueryController {

    protected final PagedResourcesAssembler resourcesAssembler;
    protected final GepQueryService queryService;

    public GepQueryController(PagedResourcesAssembler resourcesAssembler, GepQueryService queryService) {
        this.resourcesAssembler = resourcesAssembler;
        this.queryService = queryService;
    }

    @GetMapping("/query")
    public PagedResources<Resource<Gep>> query(GepCriteria criteria, Pageable page) {
        PagedResources<Resource<Gep>> resources = resourcesAssembler.toResource(queryService.findByCriteria(criteria, page));
        resources.getContent().forEach(r -> r.add(linkTo(this.getClass()).slash(r.getContent().getId()).withSelfRel()));
        return resources;
    }
}
