package com.example.demo.config;

import com.example.demo.domain.Beosztas;
import com.example.demo.domain.Dolgozo;
import com.example.demo.domain.Gep;
import com.example.demo.domain.GepTipus;
import com.example.demo.domain.Marka;
import com.example.demo.domain.Megye;
import com.example.demo.domain.Munka;
import com.example.demo.domain.Telephely;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;


@Configuration
public class RepositoryRestResourceConfiguration implements RepositoryRestConfigurer {

	private final LocalValidatorFactoryBean validator;

	public RepositoryRestResourceConfiguration(LocalValidatorFactoryBean validator) {
		this.validator = validator;
	}

	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		config.exposeIdsFor(Megye.class);
		config.exposeIdsFor(Telephely.class);
		config.exposeIdsFor(Beosztas.class);
		config.exposeIdsFor(Marka.class);
		config.exposeIdsFor(GepTipus.class);
		config.exposeIdsFor(Gep.class);
		config.exposeIdsFor(Dolgozo.class);
		config.exposeIdsFor(Munka.class);
	}

	@Bean
	public SpelAwareProxyProjectionFactory projectionFactory() {
		return new SpelAwareProxyProjectionFactory();
	}

	@Override
	public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
		validatingListener.addValidator("beforeCreate", validator);
		validatingListener.addValidator("beforeSave", validator);
	}
}
