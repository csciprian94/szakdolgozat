package com.example.demo.service;

import com.example.demo.criteria.TelephelyCriteria;
import com.example.demo.domain.Megye_;
import com.example.demo.domain.Telephely;
import com.example.demo.domain.Telephely_;
import com.example.demo.repository.TelephelyRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class TelephelyQueryService extends QueryService<Telephely,TelephelyCriteria> {

    private final TelephelyRepository repository;

    public TelephelyQueryService(TelephelyRepository repository) {
        this.repository = repository;
    }

    public Page<Telephely> findByCriteria(TelephelyCriteria criteria, Pageable page) {
        Specification<Telephely> specification = createSpecification(criteria);
        return repository.findAll(specification, page);
    }

    protected Specification<Telephely> createSpecification(TelephelyCriteria criteria) {
        Specification<Telephely> specification = Specification.where(null);

        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Telephely_.id));
            }
            if (criteria.getNev() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNev(), Telephely_.nev));
            }
            if (criteria.getTelepules() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTelepules(), Telephely_.telepules));
            }
            if (criteria.getCim() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCim(), Telephely_.cim));
            }
            if (criteria.getMukodesKezdeteK() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMukodesKezdeteK(), Telephely_.mukodesKezdete));
            }
            if (criteria.getMukodesKezdeteV() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMukodesKezdeteV(), Telephely_.mukodesKezdete));
            }
            if(criteria.getMegyeId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getMegyeId(), Telephely_.megye, Megye_.id));
            }
        }

        return specification;
    }
}
