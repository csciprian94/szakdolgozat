package com.example.demo.service;

import com.example.demo.criteria.DolgozoCriteria;
import com.example.demo.criteria.GepCriteria;
import com.example.demo.domain.Beosztas_;
import com.example.demo.domain.Dolgozo;
import com.example.demo.domain.Dolgozo_;
import com.example.demo.domain.Gep;
import com.example.demo.domain.Telephely;
import com.example.demo.domain.Telephely_;
import com.example.demo.repository.DolgozoRepository;
import com.example.demo.repository.GepRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(readOnly = true)
public class DolgozoQueryService extends QueryService<Dolgozo, DolgozoCriteria> {
    private final DolgozoRepository repository;

    public DolgozoQueryService(DolgozoRepository repository) {
        this.repository = repository;
    }

    public Page<Dolgozo> findByCriteria(DolgozoCriteria criteria, Pageable page) {
        Specification<Dolgozo> specification = createSpecification(criteria);
        return repository.findAll(specification, page);
    }

    protected Specification<Dolgozo> createSpecification(DolgozoCriteria criteria) {
        Specification<Dolgozo> specification = Specification.where(null);

        if (criteria != null) {
            if (criteria.getNev() != null) {
                specification = specification.and(buildSpecification(criteria.getNev(), Dolgozo_.keresztNev));
            }
            if (criteria.getTelephelyId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getTelephelyId(), Dolgozo_.telephely, Telephely_.id));
            }
            if (criteria.getBeosztasId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getBeosztasId(), Dolgozo_.beosztas, Beosztas_.id));
            }
            if (criteria.getDolgozik() != null) {
                specification = specification.and(buildSpecification(criteria.getDolgozik(), Dolgozo_.dolgozik));
            }
            if (criteria.getLakhely() != null) {
                specification = specification.and(buildSpecification(criteria.getLakhely(), Dolgozo_.lakhely));
            }

        }

        return specification;
    }
}
