package com.example.demo.service;

import com.example.demo.domain.Munka;
import com.example.demo.enums.GepStatuszEnum;
import com.example.demo.enums.MunkaStatuszEnum;
import com.example.demo.repository.MunkaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class MunkaService {

    private final MunkaRepository repository;

    private final GepService gepService;

    public MunkaService(MunkaRepository repository, GepService gepService) {
        this.repository = repository;
        this.gepService = gepService;
    }

    public void munkaVege(Munka munka) {
        munka.setStatusz(MunkaStatuszEnum.LEZARULT);
        munka.setMunkaido(Math.abs(LocalDateTime.now().getHour() - munka.getCreated().getHour()));

        gepService.munkaVege(munka);

        repository.save(munka);
    }
}
