package com.example.demo.service;

import com.example.demo.criteria.MunkaCriteria;
import com.example.demo.domain.Dolgozo;
import com.example.demo.domain.Dolgozo_;
import com.example.demo.domain.Gep_;
import com.example.demo.domain.Munka;
import com.example.demo.domain.Munka_;
import com.example.demo.enums.MunkaStatuszEnum;
import com.example.demo.repository.MunkaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class MunkaQueryService extends QueryService<Munka, MunkaCriteria> {

    private final MunkaRepository repository;

    public MunkaQueryService(MunkaRepository repository) {
        this.repository = repository;
    }

    public Page<Munka> findByCriteria(MunkaCriteria criteria, Pageable page) {
        Specification<Munka> specification = createSpecification(criteria);
        return repository.findAll(specification, page);
    }

    protected Specification<Munka> createSpecification(MunkaCriteria criteria) {
        Specification<Munka> specification = Specification.where(null);

        if (criteria != null) {
            if (criteria.getGepId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getGepId(), Munka_.gep, Gep_.id));
            }
            if (criteria.getMunkaido() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMunkaido(), Munka_.munkaido));
            }
            if (criteria.getStatusz() != null) {
                specification = specification.and(equalsSpecification(e -> e.get("statusz"), MunkaStatuszEnum.valueOf(criteria.getStatusz().getEquals())));
            }

        }

        return specification;
    }
}
