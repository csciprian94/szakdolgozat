package com.example.demo.service;

import com.example.demo.domain.Gep;
import com.example.demo.domain.Munka;
import com.example.demo.enums.GepStatuszEnum;
import com.example.demo.repository.GepRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GepService {
    private final GepRepository repository;
    private final DolgozoService dolgozoService;


    public GepService(GepRepository repository, DolgozoService dolgozoService) {
        this.repository = repository;
        this.dolgozoService = dolgozoService;
    }

    public void munkatKezd(Gep gep) {

        gep.setStatusz(GepStatuszEnum.HASZNALATBAN);
        if (gep != null) {
            dolgozoService.dolgozniKezd(gep.getDolgozo());
        }
        repository.save(gep);
    }

    public void munkaVege(Munka munka) {

        Gep gep = munka.getGep();
        gep.setUzemIdo(gep.getUzemIdo() + munka.getMunkaido());
        gep.setStatusz(GepStatuszEnum.VARAKOZIK);
        dolgozoService.munkaVege(gep.getDolgozo());
        gep.setDolgozo(null);
        repository.save(gep);
    }
}
