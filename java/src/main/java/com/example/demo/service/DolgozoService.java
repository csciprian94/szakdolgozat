package com.example.demo.service;

import com.example.demo.domain.Dolgozo;
import com.example.demo.domain.Gep;
import com.example.demo.domain.Munka;
import com.example.demo.repository.DolgozoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DolgozoService {
    private final DolgozoRepository repository;

    public DolgozoService(DolgozoRepository repository) {
        this.repository = repository;
    }

    public void dolgozniKezd(Dolgozo dolgozo) {
        dolgozo.setDolgozik(true);

        repository.save(dolgozo);
    }

    public void munkaVege(Dolgozo dolgozo) {
        dolgozo.setDolgozik(false);

        repository.save(dolgozo);
    }
}
