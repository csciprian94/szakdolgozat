package com.example.demo.service;

import com.example.demo.criteria.GepCriteria;
import com.example.demo.domain.Gep;
import com.example.demo.domain.GepTipus_;
import com.example.demo.domain.Gep_;
import com.example.demo.domain.Marka_;
import com.example.demo.domain.Telephely_;
import com.example.demo.enums.GepStatuszEnum;
import com.example.demo.repository.GepRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class GepQueryService extends QueryService<Gep, GepCriteria> {
    private final GepRepository repository;

    public GepQueryService(GepRepository repository) {
        this.repository = repository;
    }

    public Page<Gep> findByCriteria(GepCriteria criteria, Pageable page) {
        Specification<Gep> specification = createSpecification(criteria);
        return repository.findAll(specification, page);
    }

    protected Specification<Gep> createSpecification(GepCriteria criteria) {
        Specification<Gep> specification = Specification.where(null);


        if (criteria.getNev() != null) {
            specification = specification.and(buildSpecification(criteria.getNev(), Gep_.nev));
        }
        if (criteria.getTelephelyId() != null) {
            specification = specification.and(buildReferringEntitySpecification(criteria.getTelephelyId(), Gep_.telephely, Telephely_.id));
        }
        if (criteria.getMarkaId() != null) {
            specification = specification.and(buildReferringEntitySpecification(criteria.getMarkaId(), Gep_.marka, Marka_.id));
        }
        if (criteria.getGeptipId() != null) {
            specification = specification.and(buildReferringEntitySpecification(criteria.getGeptipId(), Gep_.gepTipus, GepTipus_.id));
        }
        if (criteria.getUzemIdo() != null) {
            specification = specification.and(buildRangeSpecification(criteria.getUzemIdo(), Gep_.uzemIdo));
        }
        if (criteria.getStatusz() != null && criteria.getStatusz().getEquals() != null) {
            specification = specification.and(equalsSpecification(e -> e.get("statusz"), GepStatuszEnum.valueOf(criteria.getStatusz().getEquals())));
        }
        if (criteria.getStatusz() != null && criteria.getStatusz().getIn() != null) {
            specification = specification.and(equalsSpecification(e -> e.get("statusz"), GepStatuszEnum.valueOf(criteria.getStatusz().getIn().get(0)))).
                    or(equalsSpecification(e -> e.get("statusz"), GepStatuszEnum.valueOf(criteria.getStatusz().getIn().get(1))));
        }

        return specification;
    }
}
