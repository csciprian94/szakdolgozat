package com.example.demo.domain;

import com.example.demo.enums.GepStatuszEnum;
import lombok.Data;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "gep")
public class Gep extends AbstractDomain{

    @NotNull
    @Column(name = "nev", length = 200)
    private String nev;

    @NotNull
    @RestResource(exported = false)
    @JoinColumn(name = "marka_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    private Marka marka;

    @NotNull

    @RestResource(exported = false)
    @JoinColumn(name = "geptipus_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    private GepTipus gepTipus;

    @NotNull
    @RestResource(exported = false)
    @JoinColumn(name = "telephely_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    private Telephely telephely;

    @RestResource(exported = false)
    @JoinColumn(name = "dolgozo_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    private Dolgozo dolgozo;

    @Column(name = "suly")
    private Integer suly;

    @Column(name = "gyartas_eve")
    private Integer gyartasEve;

    //órában mérjük
    @NotNull
    @Column(name = "uzemido")
    private Integer uzemIdo = 0;

    @Column(name = "ceg_tulajdona")
    private LocalDate cegTulajdona;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "statusz", nullable = false, length = 20)
    private GepStatuszEnum statusz;
}

