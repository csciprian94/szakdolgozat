package com.example.demo.domain;

import lombok.Data;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "telephely")
public class Telephely extends AbstractDomain {

    @NotNull
    @Column(name = "nev", length = 100)
    private String nev;

    @NotNull
    @RestResource(exported = false)
    @JoinColumn(name = "megye_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Megye megye;

    @NotNull
    @Column(name = "telepules", length = 100)
    private String telepules;

    @Column(name = "iranyitoszam")
    private Integer iranyitoSzam;

    @NotNull
    @Column(name = "cim", length = 200)
    private String cim;

    @NotNull
    @Column(name = "telefonszam", length = 50)
    private String telefonSzam;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "fax", length = 100)
    private String fax;

    @Column(name = "mukodes_kezdete")
    private LocalDate mukodesKezdete;
}
