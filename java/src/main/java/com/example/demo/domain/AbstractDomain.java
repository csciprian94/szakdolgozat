package com.example.demo.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;
import org.springframework.hateoas.Identifiable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.Transient;
import javax.persistence.Version;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@MappedSuperclass
public abstract class AbstractDomain implements Persistable<Long>, Identifiable<Long> {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(generator="generator")
    private Long id;

    @Column(name = "created",updatable = false)
    @CreationTimestamp
    private LocalDateTime created=LocalDateTime.now();

    @Column(name = "edited",updatable = false)
    @CreationTimestamp
    @LastModifiedDate
    private LocalDateTime edited;

    @Version
    @JsonIgnore
    @Column(name = "version")
    private Integer vers;

    @Transient
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private Integer version;

    @PrePersist
    public void prePersist() {
        this.created = LocalDateTime.now();
        this.edited = this.created;
    }

//    @PreUpdate
//    public void preUpdate() {
//        this.edited = LocalDateTime.now();
//    }

    @Override
    public boolean isNew() {
        return this.id==null;
    }

    @Tolerate
    public Integer getVersion() {
        return this.vers;
    }

    @Tolerate
    public void setVersion(Integer ver) {
        this.vers = ver;
        this.version = ver;
    }

}
