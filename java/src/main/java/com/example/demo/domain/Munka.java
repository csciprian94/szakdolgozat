package com.example.demo.domain;

import com.example.demo.enums.MunkaStatuszEnum;
import lombok.Data;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "munka")
public class Munka extends AbstractDomain {

    @RestResource(exported = false)
    @JoinColumn(name = "gep_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    private Gep gep;

    @RestResource(exported = false)
    @JoinColumn(name = "dolgozo_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    private Dolgozo dolgozo;

    @Column(name = "munkaido", length = 100)
    private Integer munkaido = 0;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "statusz", length = 20)
    private MunkaStatuszEnum statusz;

    @NotNull
    @Column(name="leiras")
    private String leiras;
}
