package com.example.demo.domain;

import lombok.Data;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "dolgozo")
public class Dolgozo extends AbstractDomain {

    @NotNull
    @Column(name = "vezeteknev", length = 50)
    private String vezetekNev;

    @NotNull
    @Column(name = "keresztnev", length = 50)
    private String keresztNev;

    @Formula("( select CONCAT(vezeteknev, ' ', keresztnev) )")
    private String dolgozoTeljesNeve;

    @NotNull
    @RestResource(exported = false)
    @JoinColumn(name = "beosztas_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    private Beosztas beosztas;

    @NotNull
    @RestResource(exported = false)
    @JoinColumn(name = "telephely_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    private Telephely telephely;

    @NotNull
    @Column(name = "lakhely", length = 100)
    private String lakhely;

    @Column(name = "iranyitoszam")
    private Integer iranyitoSzam;

    @NotNull
    @Column(name = "cim", length = 200)
    private String cim;

    @NotNull
    @Column(name = "telefonszam", length = 50)
    private String telefonSzam;

    @Column(name = "fizetes")
    private Long fizetes;

    @NotNull
    @Column(name = "szul_ido")
    private LocalDate szulIdo;

    @Column(name = "munkaviszony_kezdete")
    private LocalDate munkaViszonyKezdete;

    @NotNull
    @Column(name = "dolgozik")
    private Boolean dolgozik = false;
}
