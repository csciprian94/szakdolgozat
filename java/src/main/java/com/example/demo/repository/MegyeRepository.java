package com.example.demo.repository;

import com.example.demo.domain.Megye;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = MegyeRepository.ENTITY_NAME, path = MegyeRepository.ENTITY_NAME)
public interface MegyeRepository extends PagingAndSortingRepository<Megye, Long>, JpaSpecificationExecutor<Megye> {
    String ENTITY_NAME = "megye";
}
