package com.example.demo.repository;

import com.example.demo.domain.Beosztas;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = BeosztasRepository.ENTITY_NAME, path = BeosztasRepository.ENTITY_NAME)
public interface BeosztasRepository extends PagingAndSortingRepository<Beosztas, Long>, JpaSpecificationExecutor<Beosztas> {
    String ENTITY_NAME = "beosztas";
}
