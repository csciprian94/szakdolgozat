package com.example.demo.repository;

import com.example.demo.domain.Marka;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = MarkaRepository.ENTITY_NAME, path = MarkaRepository.ENTITY_NAME)
public interface MarkaRepository extends PagingAndSortingRepository<Marka, Long>, JpaSpecificationExecutor<Marka> {
    String ENTITY_NAME = "marka";
}
