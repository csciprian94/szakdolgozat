package com.example.demo.repository;

import com.example.demo.domain.Dolgozo;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = DolgozoRepository.ENTITY_NAME, path = DolgozoRepository.ENTITY_NAME)
public interface DolgozoRepository extends PagingAndSortingRepository<Dolgozo, Long>, JpaSpecificationExecutor<Dolgozo> {
    String ENTITY_NAME = "dolgozo";
}