package com.example.demo.repository;

import com.example.demo.domain.Gep;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = GepRepository.ENTITY_NAME, path = GepRepository.ENTITY_NAME)
public interface GepRepository extends PagingAndSortingRepository<Gep, Long>, JpaSpecificationExecutor<Gep> {
    String ENTITY_NAME = "gep";
}