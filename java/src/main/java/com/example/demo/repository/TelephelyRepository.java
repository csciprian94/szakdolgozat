package com.example.demo.repository;

import com.example.demo.domain.Telephely;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = TelephelyRepository.ENTITY_NAME, path = TelephelyRepository.ENTITY_NAME)
public interface TelephelyRepository extends PagingAndSortingRepository<Telephely, Long>, JpaSpecificationExecutor<Telephely> {
    String ENTITY_NAME = "telephely";
}
