package com.example.demo.repository;

import com.example.demo.domain.Munka;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = MunkaRepository.ENTITY_NAME, path = MunkaRepository.ENTITY_NAME)
public interface MunkaRepository extends PagingAndSortingRepository<Munka, Long>, JpaSpecificationExecutor<Munka> {
    String ENTITY_NAME = "munka";
}
