package com.example.demo.repository;

import com.example.demo.domain.GepTipus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = GepTipusRepository.ENTITY_NAME, path = GepTipusRepository.ENTITY_NAME)
public interface GepTipusRepository extends PagingAndSortingRepository<GepTipus, Long>, JpaSpecificationExecutor<GepTipus> {
    String ENTITY_NAME = "geptipus";
}

