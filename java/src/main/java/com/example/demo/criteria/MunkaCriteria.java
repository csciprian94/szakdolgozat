package com.example.demo.criteria;

import com.example.demo.filter.IntegerFilter;
import com.example.demo.filter.LongFilter;
import com.example.demo.filter.StringFilter;
import lombok.Data;

@Data
public class MunkaCriteria {
    private LongFilter gepId;
    private LongFilter dolgozoId;
    private IntegerFilter munkaido;
    private StringFilter statusz;

}
