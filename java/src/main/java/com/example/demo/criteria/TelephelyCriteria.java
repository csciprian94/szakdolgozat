package com.example.demo.criteria;

import com.example.demo.filter.LocalDateFilter;
import com.example.demo.filter.LongFilter;
import com.example.demo.filter.StringFilter;

public class TelephelyCriteria {

    private LongFilter id;

    private StringFilter nev;

    private StringFilter telepules;

    private LongFilter megyeId;

    private StringFilter cim;

    private StringFilter megyenev;

    private LocalDateFilter mukodesKezdeteK;

    private LocalDateFilter mukodesKezdeteV;

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNev() {
        return nev;
    }

    public void setNev(StringFilter nev) {
        this.nev = nev;
    }

    public StringFilter getTelepules() {
        return telepules;
    }

    public void setTelepules(StringFilter telepules) {
        this.telepules = telepules;
    }

    public LongFilter getMegyeId() {
        return megyeId;
    }

    public void setMegyeId(LongFilter megyeId) {
        this.megyeId = megyeId;
    }

    public StringFilter getCim() {
        return cim;
    }

    public void setCim(StringFilter cim) {
        this.cim = cim;
    }

    public LocalDateFilter getMukodesKezdeteK() {
        return mukodesKezdeteK;
    }

    public void setMukodesKezdeteK(LocalDateFilter mukodesKezdeteK) {
        this.mukodesKezdeteK = mukodesKezdeteK;
    }

    public LocalDateFilter getMukodesKezdeteV() {
        return mukodesKezdeteV;
    }

    public void setMukodesKezdeteV(LocalDateFilter mukodesKezdeteV) {
        this.mukodesKezdeteV = mukodesKezdeteV;
    }

    public StringFilter getMegyenev() {
        return megyenev;
    }

    public void setMegyenev(StringFilter megyenev) {
        this.megyenev = megyenev;
    }
}
