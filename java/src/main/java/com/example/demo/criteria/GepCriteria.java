package com.example.demo.criteria;

import com.example.demo.filter.IntegerFilter;
import com.example.demo.filter.LocalDateFilter;
import com.example.demo.filter.LongFilter;
import com.example.demo.filter.StringFilter;
import lombok.Data;

@Data
public class GepCriteria {
    private LongFilter id;
    private StringFilter nev;
    private LongFilter telephelyId;
    private LongFilter geptipId;
    private LongFilter dolgozoId;
    private LongFilter markaId;
    private LocalDateFilter mukodesKezdeteK;
    private LocalDateFilter mukodesKezdeteV;
    private StringFilter statusz;
    private IntegerFilter uzemIdo;
}
