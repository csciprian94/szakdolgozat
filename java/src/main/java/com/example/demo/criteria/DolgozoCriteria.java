package com.example.demo.criteria;

import com.example.demo.filter.BooleanFilter;
import com.example.demo.filter.LongFilter;
import com.example.demo.filter.StringFilter;
import lombok.Data;

@Data
public class DolgozoCriteria {
    private StringFilter nev;
    private LongFilter telephelyId;
    private BooleanFilter dolgozik;
    private StringFilter vezetekNev;
    private StringFilter keresztNev;
    private StringFilter teljesNev;
    private StringFilter lakhely;
    private LongFilter beosztasId;



}
