package com.example.demo.enums;

public enum  MunkaStatuszEnum {
    FOLYAMATBAN,
    LEZARULT,
    MEGSZAKADT
}
