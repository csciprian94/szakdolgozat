package com.example.demo.enums;

public enum GepStatuszEnum {
    UJ, //Még 0 üzemidővel rendelkezik
    HASZNALATBAN, // Használatban levő munkagép
    VARAKOZIK, // Azonnal használható gép
    KARBANTARTAS, //Karbantartás alatt álló gép
    UZEMEN_KIVUL, //Üzemidővel rendelkező használaton kívüli gépek
}
