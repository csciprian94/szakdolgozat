package com.example.demo.filter;

/**
 * Filter class for {@link Double} type attributes.
 *
 * @see RangeFilter
 */
public class DoubleFilter extends RangeFilter<Double> {
	private static final long serialVersionUID = 1L;
}
