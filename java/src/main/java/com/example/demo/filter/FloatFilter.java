package com.example.demo.filter;

/**
 * Filter class for {@link Float} type attributes.
 *
 * @see RangeFilter
 */
public class FloatFilter extends RangeFilter<Float> {
    private static final long serialVersionUID = 1L;
}
