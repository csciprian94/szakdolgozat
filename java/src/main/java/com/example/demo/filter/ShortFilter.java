package com.example.demo.filter;

/**
 * Filter class for {@link Short} type attributes.
 *
 * @see RangeFilter
 */
public class ShortFilter extends RangeFilter<Short> {
    private static final long serialVersionUID = 1L;
}
